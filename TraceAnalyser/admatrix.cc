#include "admatrix.hh"


Admatrix::Admatrix() {

  int i,j;

  numinst = 0;

  for (i=0; i < MAXNODE; i++) {
    for (j=0; j < MAXNODE; j++) {
      matrix[i][j] = NODEP;
    }
  }
}


Admatrix::~Admatrix() {

}

void
Admatrix::addInst(int num, string name){

  //printf("%s\n",name);
  //strncpy (names[num],name, 80);
  names[num] = name;
}



void
Admatrix::AddLink(int instrfrom, int instrto, dependtype type) {

  matrix[instrfrom][instrto] = type;

  //cout << "Adding link " << " " << instrfrom << "->" << instrto << endl;

  if (instrfrom > numinst) numinst = instrfrom;
  if (instrto > numinst) numinst = instrto;
}


void
Admatrix::Davinci(string filename) {

  int i,j;
  string  edgetype, node, edgecolour;
  ofstream file;

  file.open(adddav(filename));

  file << "[" << endl;

  for (i=0; i < numinst; i++) {

    node = "ellipse";
    
    //Output node header stuff
    file << "l(\"" 
	 << i << ":" << names[i]
	 << "\",n(\"\",[a(\"OBJECT\",\""
	 << i << ":" << names[i]
	 << "\"),a(\"_GO\",\""
	 << node
	 << "\")"
	 << "]," << endl;
    
    file << "[" << endl;
    
  
    for (j=0; j < numinst; j++) {
      //If link exists
      if (matrix[i][j] != NODEP) {
	edgecolour="black";
	switch (matrix[i][j]) {
	case WWDEP:
	  edgetype = "solid";
	  edgecolour = "brown";
	  break;
	case WRDEP:
          edgetype = "solid";
	  edgecolour = "blue";
	  break;
	case RWDEP:
	  edgetype = "solid";
	  edgecolour="red";
	  break;
	case SPAWN:
	  edgetype = "dotted";
	  break;
	case INDEP:
	  edgetype = "solid";
	  break;
	}
	file << "l(\"" << i << "->"
	     << j << ":" << names[j]
	     << "\",e(\"\",[" 
	     << "a(\"EDGEPATTERN\",\"" << edgetype << "\"),"
	     << "a(\"EDGECOLOR\",\"" << edgecolour << "\""
	     << ")],r(\""
	     << j << ":" << names[j]
	     << "\")))," << endl;
      }
    }
    //Output node tail stuff
    file << "]" << endl;
    file << ")),\n\n" << endl;
  }

  file << "]" << endl;
  
  file.close();
}



/*
ostream&
operator << (ostream& str, Admatrix& a) {
  
  int i,j;

  str << "Matrix..." << endl;
  for (i=0; i < a.numpaths; i++) {
    for (j=0; j < a.numpaths; j++) {
      if (a.matrix[i][j] != NODEP) {
	str << *(a.namelist[i]) << "\t" 
	    << *(a.namelist[j]) << "\t"
	    << a.matrix[i][j] << endl;
      }
    }
  }
  str << "...End of Matrix" << endl;
  return str;
}
*/



