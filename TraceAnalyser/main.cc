#include "admatrix.hh"
#include "stdio.h"
#include "string.h"

#define _GNU_SOURCE
#include <getopt.h>


enum insttype {READ, WRITE};
int threads_option;


struct Inst {

  insttype type;
  int memloc;
  int inst;
  int thread;
};


Inst prog[MAXNODE*5];
int inst;
Admatrix matrix;
int atomnum;

void ReadFile(string filename) {

  ifstream file;
  string type;
  string name;
  inst=-1;
  atomnum = 0;
  int memloc;
  int threadnum;

  cout << "Reading file: " << filename << endl; 

  file.open(filename);

  while (!file.eof()){

    file >> type; 

    //if (strcmp(type, "Inst")==0){
    if (type == "Inst"){
      inst++;
      file >> threadnum;
      file >> name;
      matrix.addInst(inst, name);
      //cout << "Got instruction " << inst << ":" << name << endl;
      continue;
    }

    file >> memloc;

    //if (strcmp(type, "Read")==0) {
    if (type=="Read") {

      prog[atomnum].type = READ;
      prog[atomnum].memloc = memloc;
      prog[atomnum].thread = threadnum;
      prog[atomnum++].inst = inst;
    }
    else { 
      //if (strcmp(type, "Write")==0) {
      if (type=="Write") {

	prog[atomnum].type = WRITE;
	prog[atomnum].memloc = memloc;
	prog[atomnum].thread = threadnum;
	prog[atomnum++].inst = inst;
      }
      else {
	
	cout << "Misunderstood: " << type << endl;
      }
    }
  }
  cout << "Read " << inst << " instructions and " << atomnum << " atoms" << endl;
  file.close();
}


void Output() {


  int i;

  for (i=0; i<atomnum; i++) {

    if (prog[i].type==READ){
      cout << "Read";
    }
    if (prog[i].type==WRITE){
      cout << "Write";
    }
    cout << " " << prog[i].memloc << endl;
  }
}
void DoDeps(int i) {

  int j;
  insttype first, second;
  int firstnum, secondnum;

  for (j=i-1; j!=-1; j--){

    // Conflict, accces the same location
    if (prog[i].memloc == prog[j].memloc){

      first = prog[j].type;
      second = prog[i].type;

      firstnum = prog[j].inst;
      secondnum = prog[i].inst;
      if (firstnum != secondnum){
	
	// If we are looking at threads analysis and dependencies are 
	// within thread, ignore
	if (threads_option && (prog[i].thread == prog[j].thread)){

	}
	else {
	
	  if ((first == WRITE)&&(second == WRITE)){
	    //cout << "Got write - write dep" << endl;
	    matrix.AddLink(firstnum,secondnum,WWDEP);
	  } 
	  if ((first == READ)&&(second == WRITE)){
	    matrix.AddLink(firstnum,secondnum,WRDEP);
	    //cout << "Got read - write dep" << endl;
	  } 
	  if ((first == WRITE)&&(second == READ)){
	    matrix.AddLink(firstnum,secondnum,RWDEP);
	} 
	}
      }
    }
    if (threads_option) {
      if (
	  (prog[i].thread == prog[j].thread) //Inst in the same thread
	  &&
	  (prog[i].inst - prog[j].inst == 1) // Inst are consecutive
	  ){
	matrix.AddLink(prog[j].inst, prog[i].inst, SPAWN);
      }
    }
  }
}

void MakeMatrix(){
  
  int i;

  for (i=0;i<atomnum; i++) {

    DoDeps(i);
  }
}


int main (int argc, char * argv[]) {

  int c;
  string filename;
  threads_option = 0;

  while (1)
    {
      //int this_option_optind = optind ? optind : 1;
      int option_index = 0;
      static struct option long_options[] =
      {
	//	{"file", 1, 0, 0},
	{0, 0, 0, 0}
      };

      c = getopt_long (argc, argv, "t",
		       long_options, &option_index);
      if (c == -1)
	break;

      switch (c)
	{
	case 't':
	  cout << "Threads option enabled" << endl;
	  threads_option = 1;
	  break;
	default:
	  cout << "Don't understand commandline option" << endl;
	}
    }

  if (argv[optind]) {
    filename = argv[optind];
  }
  else {

    cout << "Error, please specify filename " << argc << endl;
     return 1;
  }


  ReadFile(filename);
  //Output();
  MakeMatrix();
  cout << "Number of instructions " << inst << endl;
  matrix.Davinci("matrix");
}
