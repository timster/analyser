#ifndef ADMATRIX_HH
#define ADMATRIX_HH


#include <iostream>
#include <fstream>
#include <string>
#include "misc.hh"


#define MAXNODE 6000
//Different type of dependency

enum dependtype{NODEP, INDEP, WWDEP, WRDEP, RWDEP, SPAWN};
enum nodetype{LEGALLEAF, ORPHANLEAF, CALLNODE};

class Admatrix {

private:

  int matrix[MAXNODE][MAXNODE];
  string names[MAXNODE];//[80];
  //  Path *namelist[MAXNODE];
  //nodetype ntype[MAXNODE];

  int numinst;

public:


  Admatrix();
  ~Admatrix();
  
  void AddLink(int instrfrom, int instrto, dependtype type);
  void Davinci(string filename); //Output in daVinci format
  void addInst(int num, string name); 
  //friend ostream& operator << (ostream&, Admatrix&);
  
};

#endif
