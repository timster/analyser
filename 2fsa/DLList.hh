// This may look like C code, but it is really -*- C++ -*-
/* 
   Copyright (C) 1988 Free Software Foundation
   written by Doug Lea (dl@rocky.oswego.edu)
   cout stuff done by Tim Lewis (tl@dcs.ed.ac.uk)
*/

#define DLISTWRAPPER

#ifdef DLISTWRAPPER
#include "dlistwrapper.hh"
#else

#ifndef _DLList_h
#ifdef __GNUG__
//#pragma interface
#endif
#define _DLList_h 1

#undef OK


typedef void* Pix;

#include <iostream>
#include <string.h>

using namespace std;

struct BaseDLNode {
  BaseDLNode *bk;
  BaseDLNode *fd;
  void *item() {return (void*)(this+1);} //Return ((DLNode<T>*)this)->hd
};

template<class T>
class DLNode : public BaseDLNode
{
public:
  T hd;
  DLNode() { }
  DLNode(const T& h, DLNode* p = 0, DLNode* n = 0)
    : hd(h) { bk = p; fd = n; }
  ~DLNode() { }
};

class BaseDLList {
protected:
  BaseDLNode *h;

BaseDLList() { h = 0; sep = ".";}
  void copy(const BaseDLList&);
  BaseDLList& operator= (const BaseDLList& a);
  virtual void delete_node(BaseDLNode*node) = 0;
  virtual BaseDLNode* copy_node(const void* datum) = 0;
  virtual void copy_item(void *dst, void *src) = 0;
  virtual ~BaseDLList() { }

  Pix                   prepend(const void*);
  Pix                   append(const void*);
  Pix ins_after(Pix p, const void *datum);
  Pix ins_before(Pix p, const void *datum);
  void remove_front(void *dst);
  void remove_rear(void *dst);
  void join(BaseDLList&);

public:
  int                   empty() const { return h == 0; }
  int                   length() const;
  void                  clear();
  void                  error(const char* msg) const;
  int                   owns(Pix p) const;
  int                   OK() const;
  void                  del(Pix& p, int dir = 1);
  void                  del_after(Pix& p);
  void                  del_front();
  void                  del_rear();
  char *                  sep;
  void                    Sep(char *p) {sep = p; }
};

template <class T>
class DLList : public BaseDLList {
  //friend class          <T>DLListTrav;

  virtual void delete_node(BaseDLNode *node) { delete (DLNode<T>*)node; }
  virtual BaseDLNode* copy_node(const void *datum)
    { return new DLNode<T>(*(const T*)datum); }
  virtual void copy_item(void *dst, void *src) { *(T*)dst = *(T*)src; }

public:
  DLList() : BaseDLList() { }
  DLList(const DLList<T>& a) : BaseDLList() { copy(a);}

  DLList<T>&            operator = (const DLList<T>& a)
    { BaseDLList::operator=((const BaseDLList&) a); return *this; }

  virtual ~DLList() { clear(); }

  Pix prepend(const T& item) {return BaseDLList::prepend(&item);}
  Pix append(const T& item) {return BaseDLList::append(&item);}

  void join(DLList<T>& a) { BaseDLList::join(a); }

  T& front() {
    if (h == 0) error("front: empty list");
    return ((DLNode<T>*)h)->hd; }
  T& rear() {
    if (h == 0) error("rear: empty list");
    return ((DLNode<T>*)h->bk)->hd;
  }
  const T& front() const {
    if (h == 0) error("front: empty list");
    return ((DLNode<T>*)h)->hd; }
  const T& rear() const {
    if (h == 0) error("rear: empty list");
    return ((DLNode<T>*)h->bk)->hd;
  }
  T remove_front() { T dst; BaseDLList::remove_front(&dst); return dst; }
  T remove_rear() { T dst; BaseDLList::remove_rear(&dst); return dst; }

  T& operator () (Pix p) {
    if (p == 0) error("null Pix");
    return ((DLNode<T>*)p)->hd;
  }
  const T&              operator () (Pix p) const {
    if (p == 0) error("null Pix");
    return ((DLNode<T>*)p)->hd;
  }
  Pix                   first() const { return Pix(h); }
  Pix                   last()  const { return (h == 0) ? 0 : Pix(h->bk); }
  void                  next(Pix& p) const
    { p = (p == 0 || p == h->bk)? 0 : Pix(((DLNode<T>*)p)->fd); }
  void                  prev(Pix& p) const
    { p = (p == 0 || p == h)? 0 : Pix(((DLNode<T>*)p)->bk); }
  Pix ins_after(Pix p, const T& item)
    {return BaseDLList::ins_after(p, &item); }
  Pix ins_before(Pix p, const T& item)
    {return BaseDLList::ins_before(p, &item);}

  //My stream output function
  friend ostream& operator << (ostream& s, DLList<T>& l) {
    Pix d = l.first(); 
    
    while (d!=0) {
      s << l(d);
      l.next(d);	
      if (d!=0) s << l.sep;
    }
    return s;
  }
  
  //My equality operation
  friend int operator == (DLList<T>&l1, DLList<T>& l2){
    Pix p1=l1.first(), p2=l2.first();

    while (p1!=0) {
      if (p2 == 0) return 0;

      if (l1(p1) == l2(p2)) {
	l1.next(p1);
	l2.next(p2);
      } 
      else {return 0;}
    }
    if (p2 !=0) return 0;
    return 1;
  }


};

#endif
#endif


