#include "conflict.hh"

Conflict::Conflict(MapFsa *r, MapFsa *w) {

  read = r; write = w;
  //ContSlice *waiters;

  MapFsa* readorwrite = new MapFsa(read);
  //MapFsa writecopy(write);
  //MapFsa readcopy(read);
  
  causal = new ConflictFsa();
  causal->MakeCausal();

  readorwrite->Or(write);
  functions = new ContSlice();
  functions->ExistsFirst(readorwrite);
  
  functions->Davinci("function-calls");

  RafterW = new ConflictFsa();
  WafterW = new ConflictFsa();
  WafterR = new ConflictFsa();
  RafterR = new ConflictFsa();
  

  RafterW->BackDecomp(read,write);
  AccessConf = new ConflictFsa(RafterW);
  RafterW->And(causal);
  WafterW->BackDecomp(write,write);
  AccessConf->Or(WafterW);
  WafterW->And(causal);
  WafterR->BackDecomp(write,read);
  AccessConf->Or(WafterR);
  WafterR->And(causal);
  //RafterR.Combine(&read,&readcopy);
  //AccessConf.Or(RafterR);
  //RafterR.And(causal);
  
  RafterW->Davinci("RW");
  WafterW->Davinci("WW");
  WafterR->Davinci("WR");
  //RafterR.Davinci("RR");

  ContSlice *a,*b, *a2, *b2;
  a = new ContSlice();
  b = new ContSlice();
  a2 = new ContSlice();
  b2 = new ContSlice();
  
  
  AccessConf->Davinci("AccessConf");  
  //AccessConf->PrintSparse();
  

  //Trim wrt the causal condition
  AccessConf->And(causal);
  AccessConf->Davinci("access-trimmed-causal");

  conflict = new ConflictFsa(AccessConf);

  a->ExistsFirst(AccessConf);
  a->Davinci("a");

  //Do pruning of conflict acceptor
  //Creates waits-for information
  //ConflictFsa *comb,*bottom,*doub,*indep;

  //comb = new ConflictFsa();
  //bottom = new ConflictFsa();
  //doub = new ConflictFsa();
  //indep = new ConflictFsa();
 

  //Remove bottom stuff
  /*
  doub->MakeDouble(functions);

  bottom->MakeBottom();

  doub->Davinci("doub");
  bottom->Davinci("bottom");

  indep->Combine(doub,bottom); //Maps all legal r/w ins to bottom
  indep->Davinci("indep");

  AccessConf->Or(indep);
  AccessConf->Davinci("bottomed");
  */


  //Do infinite pruning of the conflict FSA
  a->ExistsFirst(AccessConf);
  a->Davinci("all");

  a2->ExistsSecond(AccessConf);
  a2->Davinci("all2");

  AccessConf->Davinci("beforeinfprune");

  BareAccessConf = new ConflictFsa(AccessConf);  //Keep the original access information

  AccessConf->PruneInfiniteMaps();  //Remove infinite nests of access conflicts

  AccessConf->Davinci("afterinfprune");

  if(AccessConf->Equal(BareAccessConf)){
    
    cout << "Infinite pruning has made no difference" << endl;
  }
  else {
    cout << "Infinite pruning has changed dependencies" << endl;
  }

  //Do a check to see what dependencies have been removed
  ConflictFsa *lost, *gained;
  lost = new ConflictFsa(BareAccessConf);
  lost->AndNot(AccessConf);
  lost->Davinci("lost-dependencies");
  gained = new ConflictFsa(AccessConf);
  gained->AndNot(BareAccessConf);
  gained->Davinci("gained-dependencies");

  //The First sets of lost and gained should be the same
  ContSlice *lostfirst, *gainedfirst;
  lostfirst = new ContSlice();
  gainedfirst = new ContSlice();
  lostfirst->ExistsFirst(lost);
  gainedfirst->ExistsFirst(gained);
  lostfirst->Davinci("lost-first");
  gainedfirst->Davinci("gained-first");


  b->ExistsFirst(AccessConf);  //Check we have not lost deps
  b->Davinci("remain");
  b2->ExistsSecond(AccessConf);  //Check we have not lost deps
  b2->Davinci("remain2");

  if (!a->Equal(b)){

    cerr << "**** Prune Infinite has lost dependencies ***" << endl;
  }

  //  Builds `waits for' information for read instructions
  // Don't bother with that here
  /*
  WaitsFor = new ConflictFsa();

  WaitsFor->MakeWaitsFor(AccessConf);
  */


  /*  //Do this overpruning check where we know what the threads are
  waiters = new ContSlice();

  waiters->ExistsFirst(WaitsFor);
  waiters->Davinci("waiters");

  a->AndNot(waiters);
  a->Davinci("missing");
  

  //If a and waiters are different we have over pruned
  // and need to put some dependencies back

  if (!waiters->Equal(a)) {
    cout << "Over pruning of waiting-for information has occured" << endl;

    //Add back all conflicts that are missing

    ConflictFsa *temp = new ConflictFsa();

    //Double missing to 2FSA, append AccessConf AND with WaitsFor
    temp->MakeDouble(a);
    //temp->Davinci("doub");
    temp->Combine(temp, AccessConf);
    //temp->Davinci("comb");
    WaitsFor->Or(temp);
    delete temp;
  }


  */
  /*

  if (IsMonotonic(&AccessConf)) {
    cout << "Threads are monotonic" << endl;
  }
  else {
    cout << "Threads are NOT monotonic" << endl;
  }
  


  //temp.Combine(&AccessConf,&AccessConf);
  //AC2.Combine(&temp, &AccessConf);
  //cout <<  "AccessConf squared build" << endl;
  //AC2.Davinci("AC2");


  ContSlice c;
  ConflictFsa next;

  c.ExistsFirst(&AccessConf);

  next.MakeNext(&c);
  next.Davinci("next");
  */


  //BuildSchedule();

  // Oper h;
  //   h.MakeFeautrierh();
  //   AccessConf.And(h);
  //   AccessConf.Davinci("Feautrierh");
}


void
Conflict::Recurse(int depth, ControlWord p, int state) {

  int alphsize= functions->NumSymbols();
  ControlWord next;
  //Add current path 
  if (functions->Accept(p)){ 
    controlwords.append(p);
    allwords.append(p);
  }
  if (depth==0) {
    return;
  }
  //Go through all transitions from this state
  for (int sym = 1; sym<=alphsize; sym++) {

    if (functions->Target(sym,state)!=0) {
      next = p;
      next.append((Dir)sym);
      Recurse(depth-1,  next,functions->Target(sym,state));
    }
  }
}


void Conflict::BuildSchedule() {


  ofstream file;
  file.open("schedule.tex");
  
  file << "\\begin{tabular}[]{|*{" << NUMPROC << "}{|c}||}  \\hline\\hline" << endl;

  file << "\\multicolumn{" << NUMPROC << "}{||c||}{Processor Number} \\\\ \\hline" << endl;

  for (int i=1; i<NUMPROC; i++) {
    file << i << "&"; 
  }

  file << NUMPROC << "\\\\ \\hline" << endl;

  ControlWord empty;
  scheduled.append(empty);
  bool updated = true;
  int extra;

  DLList<ControlWord> holding;  //Instructions waiting to be run

  //Build list of controlwords
  Recurse(RECDEPTH,empty,1);

  while (updated) {
    updated = false;
    Pix current = controlwords.first();
    //controlwords.next(current);
    while (current!=controlwords.end()) {
      if (Executable(current)) {
	file << controlwords(current) ;
	holding.append(controlwords(current));
	current = controlwords.del(current);
	updated = true;
	if (holding.length()==NUMPROC) {current=controlwords.end();} //Max NUMPROC parallelism
	else {file << "&";}
      }
      else {
	controlwords.next(current);
      }
    }
    //Run all holding instructions
    extra = NUMPROC - holding.length() - 1;
    scheduled.splice(scheduled.end(), holding, holding.begin(), holding.end());
    //holding.clear();
    if (updated == true) {
      for (int i=0; i<extra;i++) file << "&"; //Output instruction holes
      file << "\\\\ \\hline" << endl;
    }
  }
  cout << "Unscheduled Instructions" << endl;
  cout << controlwords << endl;
  //Problem is that an instruction might be waiting for another not in the list

  file << "\\hline" << endl;
  file << "\\end{tabular}" << endl;
  file.close();
}

bool 
Conflict::Executable(DLList<ControlWord>::iterator current) {

  Pix sched = scheduled.first();

  while (sched!=scheduled.end()){

    if (AccessConf->Accept(controlwords(current), scheduled(sched))) {
      return true;
    }
    scheduled.next(sched);
  }
  
  //Can the instruction ever be scheduled??
  if (Schedulable(current)) return false;

  cout << "Instruction " << controlwords(current) << " is deadlocked" <<  endl;


  //Look through AccessConf squared
  sched = scheduled.first();

  while (sched!=scheduled.end()){

    cout << "Comparing " << controlwords(current) << " and " << scheduled(sched) << endl;
    if (AC2->Accept(controlwords(current), scheduled(sched))) {
      return true;
    }
    scheduled.next(sched);
  }
  cout << "Instruction " << controlwords(current) << " cannot be scheduled" <<  endl;
  return false;
}

bool 
Conflict::Schedulable(DLList<ControlWord>::iterator current) {

  Pix cont = allwords.first();

  while (cont!=allwords.end()){

    if (AccessConf->Accept(controlwords(current), allwords(cont))) {
      return true;
    }
    allwords.next(cont);
  }
  return false;
}


void
Conflict::BuildDepGraph() {

  //Build list of controlwords
  ControlWord p;
  Recurse(RECDEPTH,p,1);

  //Go through all pairs of controlwords and link if they are accepted
  //Build conflict graph 
  //Output conflict-free pairs
  bool hassource;

  Pix cont1=controlwords.first();
  Pix cont2=controlwords.first();

  while (cont1 !=controlwords.end()) {
    hassource = false;
    while(cont2 != controlwords.end()) {
      if (RafterW->Accept(controlwords(cont1),controlwords(cont2))) {
	dep.AddLink(controlwords(cont1),controlwords(cont2), RWDEP);
	hassource=true;
      }
      if (WafterW->Accept(controlwords(cont1),controlwords(cont2))) {
	dep.AddLink(controlwords(cont1),controlwords(cont2), WWDEP);
      }
      if (WafterR->Accept(controlwords(cont1),controlwords(cont2))) {
	dep.AddLink(controlwords(cont1),controlwords(cont2), WRDEP);
      }
      //Link parents to children
      if (controlwords(cont1).ParentOf(controlwords(cont2))) { 
	dep.AddLink(controlwords(cont1),controlwords(cont2), SPAWN);
      }
      controlwords.next(cont2);
    }
    if (functions->Accept(controlwords(cont1))) {
      dep.SetType(controlwords(cont1),ORPHANLEAF);
    } 
    if (hassource){
      dep.SetType(controlwords(cont1),LEGALLEAF);
    }
    controlwords.next(cont1);
    cont2=controlwords.first();
  }
  dep.Davinci("Dataflow");
}

void
Conflict::BuildCilk() {
    
  //Build list of control words
  ControlWord p;
  Recurse(RECDEPTH,p,1);
  ConflictFsa o;
  ContSlice acc;

  //Build access fsas for each control word
  Pix curr=controlwords.first();
    
  while(curr!=controlwords.end()) {
	
    o=RafterW;
    o.AcceptFirst(controlwords(curr));
    o.Invert();
    acc.ExistsFirst(&o);
    accessed.append(acc);
    controlwords.next(curr);
  }

  //Build conflict graph 
  //Output conflict-free pairs
  Pix cont1=controlwords.first();
  Pix cont2=controlwords.first();
  Pix acc1=accessed.first();
  Pix acc2=accessed.first();

  controlwords.next(cont2);
  accessed.next(acc2);
  ContSlice test(accessed(acc1));
  while (cont1 != controlwords.end()) {
    while(cont2 != controlwords.end()) {
      //If controlwords cont1 and cont2 are 'siblings'
      if (controlwords(cont1).length()==controlwords(cont2).length()) {
	test=accessed(acc1);
	test.And(&accessed(acc2));
	if (test.Size()==0) {
	  //cout << "Found spawn candidates:\t";
	  dep.AddLink(controlwords(cont1),controlwords(cont2), NODEP);
	}
	else {
	  //cout << "Potential clash:\t";
	  dep.AddLink(controlwords(cont1),controlwords(cont2), WWDEP);
	}
	//cout << controlwords(cont1) << "\t" << controlwords(cont2) 
	//     << endl;
      }
      //If one is a parent, link it to its child
      if (controlwords(cont1).ParentOf(controlwords(cont2))) { 
	dep.AddLink(controlwords(cont1),controlwords(cont2), SPAWN);
      }
      else if (controlwords(cont2).ParentOf(controlwords(cont1))) { 
	dep.AddLink(controlwords(cont2),controlwords(cont1), SPAWN);
      }
      controlwords.next(cont2);
      accessed.next(acc2);
    }
    controlwords.next(cont1);
    cont2=cont1;
    controlwords.next(cont2);
    accessed.next(acc1);
    acc2=acc1;
    accessed.next(acc2);
  }

  //Output graph
  dep.Davinci("cilk");
}

ostream & 
operator << (ostream &s, Conflict &c) {

  Pix a = c.controlwords.first();
  Pix b = c.accessed.first();

  while (a!=c.controlwords.end()) {
    
    s << c.controlwords(a) << "\t\t" << (c.accessed(b)).Size() << endl;
    c.controlwords.next(a);
    c.accessed.next(b);
  }
  return s;
}


