#include "twofsa.hh"


TwoFsa::TwoFsa() {

  Dir t;
  numsym = t.alphabet.Size();
  sizea = sizeb = numsym;
  alphsize = (numsym+1)*(numsym+1)-1;
  padding = numsym+1;
}

TwoFsa::TwoFsa(TwoFsa *o) {

  Copy(o);
}


void
TwoFsa::Copy(TwoFsa *o) {
  
  //cout << "TwoFsaation to be copied" << endl << o << endl;

  direction = o->direction;
  numsym = o->numsym;
  sizea = o->sizea;
  sizeb = o->sizeb;
  alphsize = o->alphsize;
  fsa_copy(mult, o->mult);  //Copies fsa across
}

TwoFsa::~TwoFsa() {}

//Obselete
TwoFsa::TwoFsa(string filename){

  FILE *fp;
  char fsaname[100];

  if ((fp = fopen(filename.c_str(),"r")) == 0) {
    cout << "Cannot open file " << filename << endl;
      exit(1);
  }
  cout << "Parsing fsa from file " << filename << endl;
  tmalloc(mult, fsa, 1);
  fsa_init(mult);
  fsa_read(fp,mult,DENSE,0,0,TRUE,fsaname);
  if (mult->alphabet->arity != 2) {
    cerr << "Fsa in \"" << filename << "\" is not two variable" << endl;
    exit (1);
  }
}

void
TwoFsa::AllocateTransTable() {

  fsa_table_init(mult->table, mult->states->size, alphsize);
  //Zero table
  for (int i=1;i<=mult->states->size;i++){
    for(int j=1;j<=alphsize;j++){ 
      mult->table->table_data_ptr[j][i]=0;
    }
  }
  data = mult->table->table_data_ptr;
}


void
TwoFsa::AddTrans(DLList<TwoTransList*>* l){

  Pix currlist = l->first();
  TwoTransList *tl;
  TwoTrans *t;
  int state=1;
  
  while(currlist!=l->end()) {
    tl = (*l)(currlist);
    Pix curr = tl->first();
    while (curr!=tl->end()){
      t = (*tl)(curr);
      AddTrans(state, t->to, t->a, t->b);
      tl->next(curr);
    }
    l->next(currlist);
    state++;
  }
}


int
TwoFsa::Trans(int from, int x, int y){

  int letter;

  letter = CombineSymbol(x,y);
  return mult->table->table_data_ptr[letter][from];
}


void
TwoFsa::AddTrans(int from, int to, int first, int second){

  //Map padding symbols
  int letter;

  if ((first>sizea+1)||(second>sizeb+1)) {
    cerr << "Symbol (" << first << "," << second << ") out of range [" <<
      sizea << "," << sizeb << "]" << endl;
    exit(0);
  } 

  if (first == sizea+1) first = numsym+1;
  if (second == sizeb+1) second = numsym+1;

  letter = CombineSymbol(first,second);
  
  data[letter][from]=to;
}


//Maps from sizea+1, sizeb+1 to final padding symbol
int
TwoFsa::Target(int from, int first, int second){

  //Map padding symbols
  int letter;

  data = mult->table->table_data_ptr;

  if ((first>sizea+1)||(second>sizeb+1)) {
    cerr << "Symbol (" << first << "," << second << ") out of range [" <<
      sizea << "," << sizeb << "]" << endl;
    exit(0);
  } 

  if (first == sizea+1) first = numsym+1;
  if (second == sizeb+1) second = numsym+1;

  letter = CombineSymbol(first,second);
  
  return data[letter][from];
}



bool
TwoFsa::Accept(Path a, Path b) {

  int first,second, combined;

  int currstate;

  if (mult->states->size==0) return false; //No states

  currstate = mult->initial[1]; //Assume one initial state

  Pix pa=a.first(),pb=b.first();

  while ((pa != a.end()) || (pb!= b.end())) {

    first = numsym+1;
    second = numsym+1;

    if (pa!= a.end()) first = a(pa).Int();
    if (pb!= b.end()) second = b(pb).Int();

    combined = CombineSymbol(first,second);

    //Move to new state
    currstate = data[combined][currstate];
    if (currstate == 0) return false;
    if (pa!=a.end()) {a.next(pa);}
    if (pb!=b.end()) {b.next(pb);}
  } 

  //If current state is accepting return true
  for (int i=1; i<=mult->num_accepting;i++) {
    if (mult->accepting[i] == currstate) return true; 
  }

  return false;
}

//Is this.o <= this ??
bool
TwoFsa::IsClosureOf(TwoFsa *o){

  bool isit;
  TwoFsa *comb = new TwoFsa();
  comb->CombineTFSA(this, o);
  comb->Davinci("isclose");
  
  //return Equal(&comb);
  isit = comb->ContainedIn(this);
  delete comb;
  return isit;
}

void
TwoFsa::InvertTFSA() {

  fsa_swap_coords(mult);
}


void
TwoFsa::MakeNone(){

  //fsa_init(f);

  mult->flags[DFA] = true;
  //Make states, initial and accepting
  mult->states->size = 1;
  mult->num_initial = 1;
  tmalloc(mult->initial, int, 2);
  mult->initial[1] = 1;
  mult->num_accepting = 0;

  //Make alphabet
  MakeAlphabet();

  //Make table
  AllocateTransTable();

  //Fix transitions from state 1
  for (int x = 1; x <= sizea+1 ; x++) {
    for (int y = 1; y <= sizeb+1 ; y++) {
      if ((x == sizea+1) && (y==sizeb+1)) continue; 
      AddTrans(1,0,x,y);
      } 
  }
}

void
TwoFsa::MakeDouble(OneFsa *c) {

  int numstates = c->mult->states->size,
    accepting = c->mult->num_accepting;

  //Change alphabet size
  numsym = c->numsym;
  sizea = sizeb = c->sizea;
  alphsize = (numsym+1)*(numsym+1)-1;

  fsa_init(mult);

  mult->flags[DFA] = true;
  //Make states, initial and accepting
  mult->states->size = numstates;
  mult->num_initial = c->mult->num_initial;
  tmalloc(mult->initial, int, mult->num_initial+1);
  for(int i=1;i<=mult->num_initial;i++) {
    mult->initial[i]=c->mult->initial[i];
  }
  
  mult->num_accepting = accepting;
  if (accepting>0 && accepting<numstates) {
    tmalloc(mult->accepting, int, accepting+1);
    for(int i=1;i<=accepting;i++) {
      mult->accepting[i]=c->mult->accepting[i];
    }
  }
  MakeAlphabet();

  //Make table
  AllocateTransTable();

  //Fix transitions
  for (int a = 1; a<=numstates; a++) {
    for (int x = 1; x <= c->sizea ; x++) {
      AddTrans(a, c->Target(a,x), x, x);
    } 
  }
}



void
TwoFsa::MakeAll(){

  //fsa_init(f);

  mult->flags[DFA] = true;
  //Make states, initial and accepting
  mult->states->size = 1;
  mult->num_initial = 1;
  tmalloc(mult->initial, int, 2);
  mult->initial[1] = 1;
  mult->num_accepting = 1;
  tmalloc(mult->accepting, int, 2);
  mult->accepting[1] = 1;

  MakeAlphabet();

  //Make table
  AllocateTransTable();

  //Fix transitions from state 1
  for (int x = 1; x <= sizea+1 ; x++) {
    for (int y = 1; y <= sizeb+1 ; y++) {
      if ((x == sizea+1) && (y==sizeb+1)) continue; 
      AddTrans(1,1,x,y);
    } 
  }
}




void
TwoFsa::MakeLegal(){

  //fsa_init(f);

  mult->flags[DFA] = true;
  //Make states, initial and accepting
  mult->states->size = 3;
  mult->num_initial = 1;
  tmalloc(mult->initial, int, 2);
  mult->initial[1] = 1;
  mult->num_accepting = 3;
  tmalloc(mult->accepting, int, 4);

  mult->accepting[1] = 1; mult->accepting[2] = 2; mult->accepting[3] = 3;

  MakeAlphabet();

  //Make table
  AllocateTransTable();

  //Fix transitions from state 1
  //  (_,y)->2  (x,_)->3   (x,y)->1
  for (int x = 1; x <= sizea+1 ; x++) {
    for (int y = 1; y <= sizeb+1 ; y++) {
      if ((x == sizea+1) && (y==sizeb+1)) continue;
      if ((x == sizea+1) && (y!=sizeb+1)) AddTrans(1,2,x,y);
      if ((x != sizea+1) && (y==sizeb+1)) AddTrans(1,3,x,y);
      if ((x != sizea+1) && (y!=sizeb+1)) AddTrans(1,1,x,y);
      } 
  }
  //Fix transitions from state 2
  //  (_,y)->2  
  for (int x = 1; x <= sizea+1 ; x++) {
    for (int y = 1; y <= sizeb+1 ; y++) {
      if ((x == sizea+1) && (y==sizeb+1)) continue;
      if ((x == sizea+1) && (y!=sizeb+1)) AddTrans(2,2,x,y);
      else AddTrans(2,0,x,y);
      } 
  }
  //Fix transitions from state 3
  //  (x,_)->3
  for (int x = 1; x <= sizea+1 ; x++) {
    for (int y = 1; y <= sizeb+1 ; y++) {
      if ((x == sizea+1) && (y==sizeb+1)) continue;
      if ((x != sizea+1) && (y==sizeb+1)) AddTrans(3,3,x,y);
      else AddTrans(3,0,x,y);
      } 
  }
}

void
TwoFsa::KillSelfTrans(int state) {

  data = mult->table->table_data_ptr;
  for (int x = 1; x < alphsize ; x++) {
    if  (data[x][state]==state) data[x][state]=0;
  }
}



void
TwoFsa::AcceptFirst(Path f) {

  TwoFsa *tmp=new TwoFsa();
  //Create prefix
  tmp->MakePrefix(f);
  //And with this 
  And(tmp);
  delete tmp;
}

//Perhaps clean up a bit???
void
TwoFsa::ExpandAlphabet() {

  Dir t;
  int oldnumsym;

  //cout << "In function" << endl;

  //Get current size of global alphabet
  oldnumsym = numsym;
  sizea = sizeb = numsym = t.alphabet.Size();
  alphsize = (numsym+1)*(numsym+1)-1;
  //Update alphabet
  MakeAlphabet();

  //Update table
  //Rename each transition symbol in the new alphabet
  table_struc  *newptr;
  int numstates;
  numstates = mult->states->size;
  //Allocate memory for new table
  tmalloc(newptr, table_struc, 1);
  fsa_table_init(newptr, numstates, alphsize);
  //Copy information across
  //Zero all of new table
  for (int i=0; i <= numstates; i++) {
    for (int j=0; j <= alphsize; j++) {
      newptr->table_data_ptr[j][i] = 0;
    }
  }
  int newsymbol, oldsymbol, newx, newy;
  //Go through old table and map transitions
  for (int i=1; i <= numstates; i++) {
    for (int x=1; x <= oldnumsym+1; x++) {
      for (int y=1; y <= oldnumsym+1; y++) {
	if ((x==oldnumsym+1)&&(y==oldnumsym+1)) {} //Invalid case
	else {
	  oldsymbol = MergePair(x,y,oldnumsym);
	  newx = x; newy = y;
	  if (x == oldnumsym+1) newx = numsym+1; //Map padding symbols 
	  if (y == oldnumsym+1) newy = numsym+1;
	  newsymbol = CombineSymbol(newx,newy);
	  newptr->table_data_ptr[newsymbol][i]  
	    = mult->table->table_data_ptr[oldsymbol][i];
	}
      }
    }
  }    
  //Clear old table. Still Segfaults sometimes if we clear here, why? 
  //table_clear(mult->table, numstates); 
  //Change pointer
  mult->table = newptr;
}

void
TwoFsa::MakeAlphabet() {
  MakeAlphabet(mult);
}

//Now produces a valid alphabet
//Assumes f is allocated
void
TwoFsa::MakeAlphabet(fsa* f) {

  CreateAlphabet(f,numsym);
}


void
TwoFsa::AddFirst(int first){

  AddSecond(first);
  fsa_swap_coords(mult);
}

void
TwoFsa::AddSecond(int sec) {

  //cout << "Read/Write " << rdir << " " << wdir << endl;

  fsa_init(mult);

  mult->flags[DFA]=true;
  //Make states, initial and accepting
  mult->states->type = SIMPLE;
  mult->states->size = 2;
  mult->num_initial = 1;
  tmalloc(mult->initial, int, 2);
  mult->initial[1] = 1;
  mult->num_accepting = 1;
  tmalloc(mult->accepting, int, 2);
  mult->accepting[1] = 2;
  
  MakeAlphabet();

  //Make table
  AllocateTransTable();

  //Fix transitions from state 1
  //(x,x)->1 (_,sec)->2
  for (int x=1; x <= sizea+1; x++) {
    for (int y=1; y <= sizeb+1; y++) {
      if ((x == sizea+1) && (y==sizeb+1)) continue;
      else if (x == y) {
	AddTrans(1,1,x,y);
      }
      else if ((x == sizea+1) && (y==sec)) {
	AddTrans(1,2,x,y);
      } 
      else {
	AddTrans(1,0,x,y);
      }
    }
  }
  //No transitions from second
  for (int j=1; j < alphsize; j++) {
    data[j][2]=0;
  }
}


void
TwoFsa::MakePrefix(Path l) {

  int length = l.length();

  //fsa_init(mult);

  mult->flags[DFA] = true;
  //Make states, initial and accepting
  mult->states->size = length+1;
  mult->num_initial = 1;
  tmalloc(mult->initial, int, 2);
  mult->initial[1] = 1;
  mult->num_accepting = 1;
  tmalloc(mult->accepting, int, 2);
  mult->accepting[1] = length+1; //Make last accepting

  //Make alphabet
  MakeAlphabet();

  //Make table
  AllocateTransTable();
  Pix curr=l.first();

  //Do first states
  for (int s = 1; s<=length ; s++) {
    for (int x = 1; x <= sizea+1 ; x++) {
      for (int y = 1; y <= sizeb+1 ; y++) {
	if ((x == sizea+1) && (y==sizeb+1)) continue;
	AddTrans(s,0,x,y);
	if (x == l(curr).Int()) AddTrans(s,s+1,x,y);
      }
    }
    l.next(curr); //Move to next symbol
  }
  //Do last state, all transitions back to self
  for (int x = 1; x <= sizea+1 ; x++) {
    for (int y = 1; y <= sizeb+1 ; y++) {
      if ((x == sizea+1) && (y==sizeb+1)) continue;
      AddTrans(length+1,length+1,x,y);
    }
  }
}


void
TwoFsa::MakePrefix(Path a, Path b){

  TwoFsa *first=new TwoFsa(),*second=new TwoFsa();

  first->MakePrefix(a);
  second->MakePrefix(b);
  second->InvertTFSA();

  CombineTFSA(first,second);
  delete first; delete second;
}


void
TwoFsa::MakeNotEqual() {

  fsa_init(mult);

  mult->flags[DFA] = true;
  //Make states, initial and accepting
  mult->states->size = 2;
  mult->num_initial = 1;
  tmalloc(mult->initial, int, 2);
  mult->initial[1] = 1;
  mult->num_accepting = 1;
  tmalloc(mult->accepting, int, 2);
  mult->accepting[1] = 2;

  //Make alphabet
  MakeAlphabet();

  //Make table
  AllocateTransTable();

  //Fix transitions from state 1
  for (int x = 1; x <= sizea+1 ; x++) {
    for (int y = 1; y <= sizeb+1 ; y++) {
      if ((x == sizea+1) && (y==sizeb+1)) continue;
      if (x == y) {
	AddTrans(1,1,x,y);
      }
      else {
	AddTrans(1,2,x,y);
      } 
    }
  }
  //Fix transitions from state2, all back to 2
  for (int x = 1; x <= sizea+1 ; x++) {
    for (int y = 1; y <= sizeb+1 ; y++) {
      if ((x == sizea+1) && (y==sizeb+1)) continue;
      AddTrans(2,2,x,y);
    }
  }
}


void
TwoFsa::MakeIdentity() {

  Dir t;
  //int eps = t.alphabet.Epsilon();

  mult->flags[DFA] = true;
  //Make states, initial and accepting
  mult->states->size = 1;
  mult->num_initial = 1;
  tmalloc(mult->initial, int, 2);
  mult->initial[1] = 1;
  mult->num_accepting = 1;
  tmalloc(mult->accepting, int, 2);
  mult->accepting[1] = 1;

  //Make alphabet
  MakeAlphabet();

  //Make table
  AllocateTransTable();

  //Fix transitions from state 1
  for (int x = 1; x <= sizea ; x++) {
    for (int y = 1; y <= sizeb ; y++) {
      if ((x == sizea+1) && (y==sizeb+1)) continue;
      //if ((x == y) && (x!=eps)) {
      if (x == y) {
	AddTrans(1,1,x,y);
      }
      else {
	AddTrans(1,0,x,y);
      } 
    }
  }
}


void
TwoFsa::MakeAppendFsa(OneFsa *c) {

  MakeAppendFsa(mult, c->mult);
}


void
TwoFsa::MakeAppendFsa(fsa *f, fsa* onevar) {

  int ovsize = numsym;
  int x,y,s, letter;
  fsa_init(f);

  //Create appropriate alphabet for f
  MakeAlphabet(f);
  //Copy accepting and initial sets from onevar
  f->flags[DFA] = true;
  f->num_initial = 1;
  tmalloc(f->initial, int, 2);
  f->initial[1] = 1;

  fsa_set_is_accepting(onevar);
  if (onevar->is_accepting[onevar->initial[1]]) {
    f->num_accepting = onevar->num_accepting+1;
    tmalloc (f->accepting, int, f->num_accepting+1);
    for (int i=2; i<=f->num_accepting; i++) {
      f->accepting[i] = onevar->accepting[i-1]+1;
    }
    f->accepting[1] = 1;
  }
  else {
    f->num_accepting = onevar->num_accepting;
    tmalloc (f->accepting, int, f->num_accepting+1);
    for (int i=1; i<=f->num_accepting; i++) {
      f->accepting[i] = onevar->accepting[i]+1;
    }
  }

  //Make one more state than onevar has
  f->states->type = SIMPLE;
  f->states->size = onevar->states->size+1;
  //Make table
  fsa_table_init(f->table, f->states->size, alphsize);
  
  int **from = onevar->table->table_data_ptr,
    ** to = f->table->table_data_ptr; //Use sensible names

  //Transistions from state 1 to state 1
  //(x,x) takes us to 1
  //if (y) takes us to s then (y,_) does too 
  for ( x=1; x <= numsym+1; x++) {
    for ( y=1; y <= numsym+1; y++) {
      letter = CombineSymbol(x,y);
      if ((x == numsym+1) && (y==numsym+1)) continue;
      else if (x == y) {
	to[letter][1]=1;
      }
      else if ((x<=ovsize) && (from[x][1] != 0) && (y == numsym+1)) {
        to [letter][1] = from[x][1]+1;
      }
      else {
	to[letter][1]=0;
      }
    }
  }
  //Copy all transitions from onevar-
  //-incrementing the state number and mapping the symbol
  for (s=2; s <= f->states->size; s++) {
    for (x=1; x <= numsym+1; x++) {
      for (y=1; y <= numsym+1; y++) {
	letter = CombineSymbol(x,y);
	if ((x == numsym+1) && (y==numsym+1)) continue;
	else if ((x<=ovsize) && (from[x][s-1] != 0) && (y == numsym+1)) {
	  to [letter][s] = from[x][s-1]+1;
	}
	else {
	  to[letter][s]=0;
	}
      }
    }
  }
  tfree(onevar->is_accepting);
  fsa_minimize(f);
}

//Don't need to store names, in fact does not make sense to do so
void
TwoFsa::CreateAlphabet(fsa* f, int numsym){

  int alphsize = (numsym+1)*(numsym+1);

  //srec_clear(f->alphabet);
  //tmalloc(f->alphabet,srec,1);
  tmalloc(f->alphabet->base,srec,1);
  f->alphabet->type = PRODUCT;
  f->alphabet->arity = 2;
  f->alphabet->padding = '_';
  f->alphabet->base->size = numsym;
  f->alphabet->base->type = IDENTIFIERS;
  //tfree(f->alphabet->base->names);
  tmalloc(f->alphabet->base->names, char*, numsym+1);
  for (int i = 1 ; i <= numsym ; i++) {
    tmalloc(f->alphabet->base->names[i],
	    char, 2);
    //cout << "Identifier " << t.dirnames.Name(i) << endl;
    strcpy (f->alphabet->base->names[i],
	    "?");
    //cout << "Copied " << f->alphabet->base->names[i] << endl;
  }
  f->alphabet->size = alphsize - 1;  //Subtract (-,-)
}



//Map two letters to a letter in the paired alphabet
// Maybe move the mapping of the padding symbols here, but not yet
int
TwoFsa::CombineSymbol(int first, int second){

  return  (first-1)*(numsym+1) + (second-1) + 1;
}

//Map two letters to a letter in the paired alphabet
int
TwoFsa::MergePair(int first, int second, int symnum){

  return  (first-1)*(symnum+1) + (second-1) + 1;
}


int
TwoFsa::SplitSymbolFirst(int symbol){

  return (symbol / (numsym+1)) + 1;
}


int
TwoFsa::SplitSymbolSecond(int symbol){

  return symbol % (numsym+1);
}


//Problem if either is an empty fsa
void
TwoFsa::CombineTFSA(TwoFsa *a, TwoFsa *b) {

  fsa *fsacomb;

  numsym = a->numsym;
  sizea = a->sizea;
  sizeb = b->sizeb;

  //fsa_clear(mult);
  if (a->mult->states->size==0 || b->mult->states->size==0) {
    //cout << "Attempt to combine empty FSAs" << endl;
    //exit(1);
    //(Alternatively return a null TwoFsa)
    fsa_init(mult);
    return;
  }
  //cout << a.mult.states->size << " " << b.mult.states->size << endl;
  fsacomb = fsa_composite(a->mult, b->mult, DENSE, FALSE, "temp", TRUE);
  fsa_minimize(fsacomb);
  //fsa_clear(mult);
  fsa_init(mult);
  fsa_copy(mult, fsacomb);
  fsa_clear(fsacomb);
}


void
TwoFsa::SetDir(Dir d) {

  direction.clear();
  direction.append(d);
}


void
TwoFsa::SetDir(Path f) {

  direction = f;
}



void
TwoFsa::Enum(int len) {

  cout << "**Enumerating " << direction << " ..." << endl;
  fsa_enumerate(stdout,mult,len,len,true);
  cout << "\n*************" << endl;
}

void
TwoFsa::PrintSparse() {

  //cout << "TwoFsa " << direction << endl;
  PrintSparse(mult);
}

//Print text version of the description
void
TwoFsa::PrintSparse(fsa *f) {

  int x,y,s,numalph, numsym, numstates, letter, transition;
  
  int **data = f->table->table_data_ptr, index=0;
  bool first = true;

  numalph = f->alphabet->size;
  numsym = f->alphabet->base->size;
  numstates = f->states->size;

  fsa_set_is_accepting(f);
  fsa_set_is_initial(f);

  if (f->flags[NFA]==false){
    cout << "{:" << endl;
    cout << "\tStates " << numstates << ";" << endl;
    cout << "\tStart ";
    for (s=1; s <= numstates; s++) {
      if (f->is_initial[s]) cout << s;
    }
    cout << "; Accept ";
    for (s=1; s <= numstates; s++) { 
      if (f->is_accepting[s]){
	if (first) {cout << s; first = false;}
	else { cout << ", " << s;}
      }
    }
    cout << ";" << endl;
    cout << "\tTransitions[" << endl;
    
    for (s=1; s <= numstates; s++) { 
    
      cout << "\t[";
      for (x=1; x <= numsym+1; x++) {
	for (y=1; y <= numsym+1; y++) {
	  if ((x==numsym+1) && (y==numsym+1)) continue;
	  letter = CombineSymbol(x,y);
	  transition = data[letter][s];
	  if (transition != 0) {
	    cout << PairName(x,y) << "->" 
		 << transition << ";";
	  }
	}
      }
      cout << "\t]" << endl;
    }
    cout << "}" << endl;
  }
  else {
    cout << "NFA found" << endl;
    for (s=1; s <= numstates; s++) {
      cout << "State [" << s << "]:";
      if (f->is_initial[s]) cout << "Initial ";
      if (f->is_accepting[s]) cout << "Accept ";
      cout << endl;
      while(data[s+1]>&data[s][index]){
	letter = data[s][index++];
	transition = data[s][index++];
	x = SplitSymbolFirst(letter);
	y = SplitSymbolSecond(letter);
	cout << "(" << Name1(x) << "," << Name2(y) << ")->[" 
	     << transition << "]\t";
      }
      index = 0;
      cout << endl;
    }
  }
}



void
TwoFsa::Davinci(string filename) {

  ofstream file;
  file.open(adddav(filename));
  file << "[" << endl;
  Davinci(file);
  file << "]" << endl;
  file.close();
}


void
TwoFsa::Davinci(ostream &out) {
  
  fsa* f = mult;
  int numstates = f->states->size;
  bool found;
  data=f->table->table_data_ptr;
  alphsize = (numsym+1)*(numsym+1)-1;
  fsa_set_is_accepting(f);
  fsa_set_is_initial(f);

  //Two type of node, state and transition
  //State nodes are of type 
  //start - Rhombus 
  //accept - double border
  //normal - Circle

  //Transitions are 'edges' that link two nodes
  //Box nodes with state name and list of  transition labels

  //out << "[" << endl;

  //Go through each state
  for (int i=1; i<=numstates; i++) {
    
    //Determine its type and create an appropriate node
     //Output node header stuff
    out << "l(\"" 
	<< direction << ":\\n"
	<< StateName(i)
	<< "\",n(\"\",[a(\"OBJECT\",\"";

    if (f->is_initial[i]) {out << direction << ":\\n";}

    out << StateName(i)
	<< "\"),a(\"_GO\",\"" ;

    if (f->is_initial[i]) out << "rhombus";
    else out << "circle";
    out  << "\")";  
    if (f->is_accepting[i]) {
      out << ",a(\"BORDER\",\"double\")"  << endl;
    }
    out << "],[" << endl;


    //Now go through all possible transition states
    for (int j=1; j<=numstates; j++) {
      found = false;
      //Look for transitions that take us there
      for (int a=1; a<alphsize; a++) {
	if (data[a][i] == j) found = true; 
      }
      if (found) {
	//Create edge
	out << "l(\""
	    << direction << ":"
	    << StateName(i) << "->" << StateName(j) << "-1-\",";
	out << "e(\"\",[a(\"EDGEPATTERN\",\"solid\"),a(\"_DIR\",\"none\")],";
	//Create new transition node
	out << "l(\"";
	//Create node name, 
	//-a list of all letters that take us there
	out << direction << ":"
	  << StateName(i) << "->" << StateName(j) << "\\n";
	for (int x = 1; x <= sizea+1 ; x++) {
	  for (int y = 1; y <= sizeb+1 ; y++) {
	    if (x==sizea+1 && y==sizeb+1) break;
	    if (Target(i, x, y) == j) {
	      out << PairName(x,y) << "\\n";
	    }
	  }
	}
	out << "\"," << endl;
	out  << "n(\"\",[a(\"OBJECT\",\"";
	//Nodename again, Displayed version
	//-a list of all letters that take us there
	//out << i << "->" << j << "\\n";
	for (int x = 1; x <= sizea+1 ; x++) {
	  for (int y = 1; y <= sizeb+1 ; y++) {
	    if (x==sizea+1 && y==sizeb+1) break;
	    if (Target(i, x, y)==j) {
	      out << PairName(x,y) << "\\n";
	    }
	  }
	}
	out << "\"),a(\"_GO\",\"box\")],[" << endl;

	//Create edge to end node
	out << "l(\"" << direction << ":" << StateName(i)
	    << "->" << StateName(j) << "-2-" //dir 
	    << "\",e(\"\",[" 
	    << "a(\"EDGEPATTERN\",\"" << "solid" << "\""
	    << ")],r(\""
	    << direction << ":\\n"
	    << StateName(j)
	    << "\")))," << endl;
	out << "]))";
	out << ")),";
      } //End if found
    }
    out << endl << "])),\n" << endl;
  }
}


string
TwoFsa::PairName(int x, int y) {

  string name = "(" + Name1(x) + "," + Name2(y) + ")";
  //name = (char *)malloc(sizeof(char)*40);
  //sprintf(name, "(%s,%s)", Name1(x), Name2(y));
  return name;
}




int 
operator == (TwoFsa&, TwoFsa&) {

//   if ((o1.direction == o2.direction) && (o1.rules == o2.rules)) {
//     return 1;
//   }
  return 1;
}

ostream&
operator << (ostream& s, TwoFsa &o) {

  s << "Direction:" << o.direction  << "\n"
    <<  "  States: " << o.mult->states->size
    << endl;
  s << "Alphabet:\t" << o.mult->alphabet->size << endl;

  //s << "---Davinci Output---" << endl;
  //o.Davinci(s);
  //s << "---Davinci Output---" << endl;
  return s;
}





















