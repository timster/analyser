#include "oper.hh"

DirFsa::DirFsa() {

  Dir t;
  sizea = sizeb = numdir = t.alphabet.DirNum();
}


DirFsa::~DirFsa(){

}

DirFsa::DirFsa(int gen) {
  
  DirFsa();
  Dir t;
  int eps = t.alphabet.Epsilon();

  //cout << "Epsilon " << eps << endl;

  if (t.alphabet.Type(gen)==LINK) { 
    cerr << "Direction " << t.alphabet.DirName(gen) 
	 << " is a link direction, fsa must be supplied." << endl;
    exit(0);
  }

  tmalloc(mult,fsa,1);
  fsa_init(mult);

  mult->flags[DFA] = true;
  //Make states, initial and accepting
  mult->states->size = 2;
  mult->num_initial = 1;
  //tfree(mult->initial);
  tmalloc(mult->initial, int, 2);
  mult->initial[1] = 1;
  //tfree(mult->accepting);
  mult->num_accepting = 1;
  tmalloc(mult->accepting, int, 2);
  mult->accepting[1] = 2;

  //Make alphabet
  MakeAlphabet();

  //Make table
  AllocateTransTable();
  //Handle double empty symbol seperately
  //Fix transitions from state 1
  //(dir,dir)->1  (-, gen)->2 
  for (int x = 1; x <= sizea+1 ; x++) {
    for (int y = 1; y <= sizeb+1 ; y++) {
      if ((x==sizea+1) && (y==sizeb+1)) continue;
      AddTrans(1,0,x,y);
      if ((x == y) && (t.alphabet.Type(x)==GEN)&&(x!=eps)) {
	AddTrans(1,1,x,y);
      }
      if ((x == sizea+1) && (y == gen)) {
	AddTrans(1,2,x,y);
      }
    }
  }    
  //No transitions from state 2
  for (int x = 1; x <= sizea+1 ; x++) {
    for (int y = 1; y <= sizeb+1 ; y++) {
      if ((x==sizea+1) && (y==sizeb+1)) continue;
      AddTrans(2,0,x,y);
    }
  }
  //Davinci("test-gen");
}



DirFsa::DirFsa(char *file) : TwoFsa(file) {
  
  DirFsa();
}


DirFsa::DirFsa(DirFsa*df){

  DirFsa();
  Copy(df);
}


// ASAP compatability requires rethink
/*
DirFsa::DirFsa(char *f1, char *f2, int axiomtype) {

  FILE *fp1, *fp2;
  fsa *fsa1, *fsa2;
  char fsaname1[100], fsaname2[100];

  DirFsa();

  tmalloc(fsa1, fsa,1);
  tmalloc(fsa2, fsa,1);
  tmalloc(mult,fsa,1);
  fsa_init(mult);

  if ((fp1 = fopen(f1,"r")) == 0) {
    cout << "Cannot open file " << f1 << endl;
      exit(1);
  }

  if ((fp2 = fopen(f2,"r")) == 0) {
    cout << "Cannot open file " << f2 << endl;
      exit(1);
  }
  cout << "Parsing fsa from file " <<  f1 << endl;

  fsa_read(fp1,fsa1,DENSE,0,0,TRUE,fsaname1);
  cout << "Parsing fsa from file " << f2 << endl;
  fsa_read(fp2,fsa2,DENSE,0,0,TRUE,fsaname2);

  SetDir(Dir(0));
  
  if ((axiomtype==1)  || (axiomtype == 3)) Axiom1and3(fsa1,fsa2);
  else if (axiomtype==2) Axiom2(fsa1,fsa2);
}


//Obsolete ??
void
DirFsa::Axiom1and3(fsa *f1, fsa *f2) {

    fsa *first, *second, *comp;
    tmalloc(first, fsa,1);
    tmalloc(second, fsa,1);
    
    //Append f1 to equality fsa
    MakeAppendFsa(first, f1);

    //Append f2 to equality fsa and invert
    MakeAppendFsa(second, f2);
    fsa_swap_coords(second);

    
    //Compose
    comp = fsa_composite(first, second, DENSE, FALSE, "temp",
			TRUE);

    fsa_copy(mult, comp);
    fsa_clear(first);
    fsa_clear(second);
    fsa_clear(comp);
    tfree(first);
    tfree(second);
    tfree(comp);
}


//Obsolete ??

void
DirFsa::Axiom2(fsa *f1, fsa *f2) {

  fsa *notequal, *first, *second, *comp1, *comp2;

  tmalloc(notequal, fsa, 1);
  tmalloc(first, fsa, 1);
  tmalloc(second, fsa, 1);


  MakeNotEqual(notequal);
  //cout << "Not Equal" << endl ; PrintSparse(notequal);
  MakeAppendFsa(first, f1);
  //cout << "First Append fsa" << endl; PrintSparse(first);
  MakeAppendFsa(second, f2);
  //cout << "Second Append fsa" << endl; PrintSparse(second);
  fsa_swap_coords(second);

  comp1 = fsa_composite(first, notequal, DENSE, FALSE, "temp",TRUE);
  fsa_minimize(comp1);

  //cout << "First comp" << endl; PrintSparse(comp1);
  
  comp2 = fsa_composite(comp1, second, DENSE, FALSE, "temp",TRUE);
  fsa_minimize(comp2);
  //cout << "Second comp" << endl; PrintSparse(comp2);
  
  fsa_copy(mult, comp2);
  fsa_clear(notequal);
  fsa_clear(first);
  fsa_clear(second);
  fsa_clear(comp1);
  fsa_clear(comp2);
  tfree(comp1);  tfree(comp2); tfree(first);  tfree(second);
}
*/

void
DirFsa::MakeRemoveEps(){

  Dir t;
  int numgen=t.alphabet.DirNum();   //This allows for non-generator directions
  //cout << "RemoveEps Numgen = " << numgen << endl;
  //cout << "NumDirs = " << t.alphabet.DirNum() << endl;
  int epsilon=t.alphabet.Epsilon();

  //cout << "NumSym = " << numsym << endl;

  //fsa_init(f);

  mult->flags[DFA] = true;
  //Make states, initial and accepting
  mult->states->size = numgen+3;
  mult->num_initial = 1;
  tmalloc(mult->initial, int, 2);
  mult->initial[1] = 1;
  mult->num_accepting = 2;
  tmalloc(mult->accepting, int, 3);

  mult->accepting[1] = 1; 
  mult->accepting[2] = numgen+3;

  //Make alphabet
  MakeAlphabet();

  //Make table
  AllocateTransTable();

  //Fix transitions from state 1
  for (int x = 1; x <= sizea+1 ; x++) {
    for (int y = 1; y <= sizeb+1 ; y++) {
      if ((x == sizea+1) && (y==sizeb+1)) continue;
      AddTrans(1,0,x,y);
      //if (t.alphabet.Type(y)==GEN){
	if (x==y){
	  AddTrans(1,1,x,y);
	} 
	if (x==epsilon) {
	  AddTrans(1,y+1, x,y);
	}
	//}
      if ((x==epsilon)&&(y==sizeb+1)) {
	AddTrans(1,numgen+3,x,y);}
    if ((x==epsilon) && (y==epsilon))AddTrans(1,numgen+2,x,y); 
    }
  }
  //Fix transitions from each generator state
  for (int i=1; i<=numgen; i++) {
    for (int x = 1; x <= sizea+1 ; x++) {
      for (int y = 1; y <= sizeb+1 ; y++) {
	if ((x == sizea+1) && (y==sizeb+1)) continue;
	AddTrans(i+1,0,x,y);
	//if ((t.alphabet.Type(x)==GEN) && (t.alphabet.Type(y)==GEN)){
	  if (x==i){
	    if (x==y) {
	      AddTrans(i+1,i+1,x,y);
	    }
	    else {
	      AddTrans(i+1,y+1,x,y);
	    }
	  }
	  //}
	if ((x==i)&&(y==epsilon)) {
	  AddTrans(i+1,numgen+2,x,y);
	}
	if ((x==i)&&(y==sizeb+1)) {
	  AddTrans(i+1,numgen+3,x,y);
	}
      }
    }
  }
  //Fix transitions from epsilon state (numgen+2)
  for (int x = 1; x <= sizea+1 ; x++) {
    for (int y = 1; y <= sizeb+1 ; y++) {
      if ((x == sizea+1) && (y==sizeb+1)) continue;
      AddTrans(numgen+2,0,x,y);
      if (x==epsilon){
	//if (t.alphabet.Type(y)==GEN){
	  AddTrans(numgen+2,y+1,x,y);
	  //} 
	if (y==epsilon) {
	  AddTrans(numgen+2,numgen+2,x,y);
	}
	if (y==sizeb+1) {
	  AddTrans(numgen+2,numgen+3,x,y);
	}
      }
    }
  }
  //Fix transitions from final state
  for (int x = 1; x <= sizea+1 ; x++) {
    for (int y = 1; y <= sizeb+1 ; y++) {
      if ((x == sizea+1) && (y==sizeb+1)) continue;
      AddTrans(numgen+3,0,x,y);
    }
  }
  //cout << "Remove eps" << endl;
  //PrintSparse();
  //Davinci("remove");
}


bool
DirFsa::IsOne2One(){

  DirFsa *Rinv = new DirFsa(this), *equal = new DirFsa();

  equal->MakeIdentity();

  Rinv->Invert();

  Rinv->Combine(Rinv, this);
  Rinv->AndNot(equal);

  Rinv->Davinci("rinv");

  if (Rinv->Empty()) return true;

  return false;
}
 


void
DirFsa::Reverse() {

  fsa * rev;
  Dir d;
  int epsilon = d.alphabet.Epsilon(), numstates, **curtable;

  tmalloc(rev,fsa,1);
  SetupAccepting();
  fsa_init(rev);
  rev->flags[NFA]=true;
  rev->flags[MIDFA]=true;
  rev->table->denserows=0;

  MakeAlphabet(rev); 
  
  numstates = rev->states->size = mult->states->size;

  //Create initial states, accepting states of f
  rev->num_initial=mult->num_accepting;

  tmalloc(rev->initial,int,rev->num_initial+1);
  for(int i=1;i<=rev->num_initial;i++) {
    if (rev->num_accepting==numstates) {rev->initial[i]=i;}
    else{rev->initial[i]=mult->accepting[i];}
  }


  //Create accepting states, initial states of f
  rev->num_accepting=mult->num_initial;

  if (rev->num_accepting < rev->states->size){
    tmalloc(rev->accepting,int,rev->num_accepting+1);
    for(int i=1;i<=rev->num_accepting;i++) 
      if (mult->num_initial==numstates){rev->accepting[i]=i;}
      else {rev->accepting[i]=mult->initial[i];}
  }

  MakeAlphabet(rev);

  //Placeholder 

  rev->table->maxstates=numstates;
  rev->table->table_type=SPARSE;

  //Allocate table
  tmalloc(curtable, int*, numstates+2); 
  tmalloc(curtable[0], int, 2*numstates*(sizea+1)*(sizeb+1)); //Conservative

  curtable[1]=curtable[0];
  //printf("\n%p",curtable[1]);
    
  int tableindex=0;

  int letter, a, b, x, y, u, v;

  for (a=1; a<=numstates;a++) { 
    for (x=1; x<=sizea+1; x++) {
      for (y=1; y<=sizeb+1; y++) {
	if ((x==sizea+1) && (y==sizeb+1)) continue;
	for (b=1; b<=numstates; b++) {
	  u = x; v = y;
	  if (x==sizea+1) u = epsilon;
	  if (y==sizeb+1) v = epsilon;
	  letter = CombineSymbol(u, v);
	  if (Target(b,x,y)==a){
	    curtable[a][tableindex++] = letter;
	    curtable[a][tableindex++] = b;
	    //cout << "Adding transition " << a << "->" << b 
	    //   << "(" << u << "," << v << ")" << endl;
	  }
	}
      }
    }
    curtable[a+1] = &curtable[a][tableindex];
    //cout << &curtable[a][tableindex] << endl;
    //printf("\n%p",&curtable[a][tableindex]);
    tableindex=0;
  }
  rev->table->table_data_ptr = curtable;

  fsa_clear(mult);
  mult = fsa_determinize(rev,DENSE,FALSE,"temp");
  //PrintSparse();
}


void
DirFsa::MakeIdentity() {

  //int below = numsym;
  Dir t;
  int eps = t.alphabet.Epsilon();
  
  //fsa_init(mult);

  mult->flags[DFA] = true;
  //Make states, initial and accepting
  mult->states->size = 1;
  mult->num_initial = 1;
  tmalloc(mult->initial, int, 2);
  mult->initial[1] = 1;
  mult->num_accepting = 1;
  tmalloc(mult->accepting, int, 2);
  mult->accepting[1] = 1;

  //Make alphabet
  MakeAlphabet();

  //Make table
  AllocateTransTable();

  //Fix transitions from state 1
  for (int x = 1; x <= sizea ; x++) {
    for (int y = 1; y <= sizeb ; y++) {
      if ((x == y) && (t.alphabet.Type(x)==GEN) && (x!=eps)) {
	AddTrans(1,1,x,y);
      }
      else {
	AddTrans(1,0,x,y);
      } 
    }
  }
}

// The leaf==true flag decides if we are to use the old method of treating 
// padded words as leaf nodes.
// This method is broken, so set as false
void
DirFsa::MakeJoinCode(bool leaf, ostream &out) {

  int numstates = mult->states->size, i;
  int x,y, target;

  if(!IsOne2One()) {
    cerr << "****** Warning: " << direction << " is not 1-1 *******" << endl; 
  }

  SetupAccepting();
  
  //Output function declarations

  for(i=1; i<=numstates ; i++) {

    out << "void" << endl;
    out << "join_" << direction << "_" << i << "(Tree* a, Tree *b);" << endl;
  }


  //Output functions
  for(i=1; i<=numstates; i++) {
    //Function prologue
    out << "void" << endl;
    out << "join_" << direction << "_" << i << "(Tree* a, Tree *b){" << endl;

    //Actually link nodes
    if (Accepting(i)){
      //      out << "if ((a->" << Name1(1) << "==NULL)&&(b->"
      //  << Name2(y) << "==NULL)){
      out << "a->" << direction << " = b;" << endl;
    }

    for(x=1; x<= sizea+1; x++) {
      for(y=1; y<=sizeb+1; y++) {
	if ((x==sizea+1)&&(y==sizeb+1)) continue;
	target = Target(i, x,y);
	//Call join functions recursively
	if (target) {
	  if ((x!=sizea+1)&&(y!=sizeb+1)){
	    out << "if((a->" << Name1(x) << "!=NULL)&&(b->" << Name2(y) 
		<< "!=NULL)) ";
	      
	    out << "join_" << direction << "_"  << target << "(a";
	    out << "->" << Name1(x);
	    out << ",b->" << Name2(y);
	    out << ");" << endl;
	  }

	  if (x==sizea+1) {  
	    if (leaf){	  //Only recurse if a is a leaf node (if a->direction1==NULL)
	      out << "if((a->" << Name1(1) << "==NULL)&&(b->" << Name2(y)
		  << "!=NULL)) ";
	      out << "join_" << direction << "_"  << target << "(a";
	      out << ",b->" << Name2(y);
	      out << ");" << endl;
	    }
	    else {
	      out << "if(b->" << Name2(y)
		  << "!=NULL) ";
	      out << "join_" << direction << "_"  << target << "(a";
	      out << ",b->" << Name2(y);
	      out << ");" << endl;

	    }
	  }
	  if (y==sizeb+1) {
	    if (leaf) {  //Only recurse if b is a leaf node 
	      out << "if((a->" << Name1(x) << "!=NULL)&&(b->" << Name2(1)
		  << "==NULL)) " << endl;
	      out << "join_" << direction << "_"  << target << "(a";
	      out << "->" << Name1(x);
	      out << ",b);" << endl;
	    }
	    else {
	      out << "if (a->" << Name1(x) << "!=NULL) ";
	      out << "join_" << direction << "_"  << target << "(a";
	      out << "->" << Name1(x);
	      out << ",b);" << endl;
	    }
	  }
	}
      }
    }
    //Function epilogue
    out << "}\n" << endl;
  }
}




string
DirFsa::Name1(int d) {

  Dir t;
  if (d==sizea+1) d = numsym+1;
  return t.alphabet.DirName(d); 
}

string
DirFsa::Name2(int d) {

  if (d==sizeb+1) d = numsym+1;
  return Name1(d);
}








