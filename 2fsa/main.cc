#include <iostream>
#include "code.hh"

#include <stdio.h>

// Don't think these externs are required anymore
//extern char *optarg;
//extern int optind, opterr, optopt;

#define _GNU_SOURCE
#include <getopt.h>

Opts options;


int
main (int argc, char *argv[]) {

  
  string filename;

  //char *procfilename;


  int c;
  //int digit_optind = 0;

  options.JoinCode = false;

  while (1)
    {
      //int this_option_optind = optind ? optind : 1;
      int option_index = 0;
      static struct option long_options[] =
      {
	//	{"file", 1, 0, 0},
	{0, 0, 0, 0}
      };

      c = getopt_long (argc, argv, "jdawc",
		       long_options, &option_index);
      if (c == -1)
	break;

      switch (c)
	{
	case 'j':
	  printf ("Join code option\n");
	  options.JoinCode = true;
	  break;
	case 'd':
	  printf ("daVinci code option\n");
	  options.DavCode = true;
	  break;
	case 'a':
	  printf ("Annotate option\n");
	  options.Annotate = true;
	case 'w':
	  printf ("Waits for option\n");
	  options.WaitsFor = true;
	  break;
	case 'c':
	  printf ("Global conflict option\n");
	  options.GlobalConflict = true;
	  break;
	default:
	  printf ("?? getopt returned character code 0%o ??\n", c);
	}
    }
  //Take next arg as filename
  filename = "default.frog";
  //procfilename = "default.proc";

  if (argv[optind]) {
    filename = argv[optind];
  }

  //if (argv[2]) {
  //procfilename = argv[2];
  //}

  Code code(filename);  //Parse code from file
  
  //Return (limited) success
  return 0;
}





