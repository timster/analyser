#include "prog.hh"

int operator == (Pred&, Pred&) {return 1;}
int operator == (Must&, Must&) {return 1;}

ostream& operator << (ostream &s, Must &m){
    s << m.statdir << ":\t" << m.executed;
    return s;
}


ostream & operator << (ostream &s, Pred&p) {

  s << "(" << p.elem->ind << "->" << *(p.elem->word) << " ";
  switch (p.type) {
  case ISNULL: {
    s << " == NULL";
    break;
  }
  case NOTNULL: {
    s << " != NULL";
    break;
  }
  case UNKNOWN: {
    s << " == ???";
    break;
  }
  }
  s << ")";
  return s;
}

void
Pred::Not() {
    if (type == ISNULL) {type = NOTNULL; return;} 
    if (type == NOTNULL) {type = ISNULL; return;}
    //Leave unknowns alone
}

//Assumes predicated are within the same function call 
bool
Implies(Predlist &a, Predlist &b) {

  Pix x=a.first(), y=b.first();
  bool satisfied=false;
  //Go through each of the predicates in b  
  while (y!=b.end()) {
    satisfied = false;
    if (b(y).type == UNKNOWN) return false; //Can't predict
    //Go through each of a looking for satisfaction
    while (x!=a.end()) {
      if (Implies( a(x),b(y) )) {satisfied = true; break;}
      a.next(x);
    }
    if (!satisfied) return false;
    b.next(y);
  }
  return true;
}


bool
Implies(Pred &a, Pred &b) {
  
  if (b.elem->ind != a.elem->ind) return false;
  if ((a.type==UNKNOWN) || (b.type == UNKNOWN)) return false;

  //If they are the same
  if ((a.type==b.type) && (*(a.elem->word)==*(b.elem->word))){
    return true;
  }
  //Make more generous assumptions
  if (ISCORRECT){ 
    if ((a.type==ISNULL) && (b.type==ISNULL) && 
	(a.elem->word->PrefixOf(b.elem->word))) {
      return true;
    }
    if ((b.type==NOTNULL) 
	&& (b.elem->word->PrefixOf(a.elem->word))){
      return true;
    }
  }
  return false;
}

Prog::Prog() {

  //words.Sep("\n");
  //rdir.Sep(".");
  //wdir.Sep(".");

  //numwrites = 0;
  numfunc = 0;
}


/*

//Trims Access fsa so that only inside function dependencies are shown
void
Prog::OnlyThisCall(Oper *o) {

  unrolled = *o;  //Copy operation across

  Dir t;
  int numdir=t.dirnames.Size();
  //int alphsize = (numdir+1)*(numdir+1);
  int letter;
  int numstates = unrolled.mult->states->size;
  int a,b;

  for (int s=1; s<=numstates; s++) {
    for (int x=1; x <= numdir+1; x++) {
      for (int y=1; y <= numdir+1; y++) {
	letter = unrolled.CombineSymbol(x,y,numdir);
	if ((x == numdir+1) && (y==numdir+1)) continue;
	//Convert x and y from directions to statement numbers
	a = t.dirnames.StatementNum(x);
	b = t.dirnames.StatementNum(y);
	if ((a != b) && (stmts[a].type==CALL) && (stmts[b].type==CALL)) {
		  unrolled.mult->table->table_data_ptr[letter][s] = 0;
	}
	if ((stmts[a].type != stmts[b].type)) {
	  unrolled.mult->table->table_data_ptr[letter][s] = 0;
	}
      }
    }
  }
  unrolled.Tidy();

  unrolled.Davinci("unrolled");
}


*/



/*
ostream& 
operator << (ostream& s, Prog &p) {

  Dir tmp;
  
  for (int i=0; i < p.numwrites ; i++) {
    s  << p.stmts[i].funcnumber << ":"
       << tmp.dirnames.Name(p.stmts[i].statdir.Int()) << "\t";
     if (p.stmts[i].type == CALL) {
     	s << "Call:\t" 
	  << "F_" << p.stmts[i].call << "\t" << p.stmts[i].write <<endl ;
    }
    if (p.stmts[i].type == RW) {
      s << "R/W\t" 
	<< p.stmts[i].write << "\t = \t" << p.stmts[i].readlist << endl;
    }
  }


  //s << "These are all the required composite operations" << endl;
  //s << p.words; 
  return s;
}


*/
