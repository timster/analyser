#include "closure.hh"

const int debugBuildStates = FALSE;


StateTrans::StateTrans(int numb) {

  numbarred = numb;
  numsym = sizea = sizeb = numbarred * 2;
}

string
StateTrans::StateName(int d){

  string s = "(" + to_string(d) + ")";
  //tmalloc(s,char,5);
  //sprintf(s,"(%d)",d);
  return s;  
}


string 
StateTrans::Name(int d) {

  string s;
  //tmalloc(s,char,10);

  if (d>numbarred) {
    s = "[" + to_string(d) + "]";
    //sprintf(s,"[%d]",d-numbarred);
  }
  if (d<=numbarred) {
    s = to_string(d);
    //sprintf(s,"%d",d);
  }
  if (d==sizea+1) {
    s = "_";
    //sprintf(s,"_");
  }
  //NB no padding symbol
  if ((d<0) || (d>sizea+1)) {
    cerr << "StateTrans: invalid name " << d << endl;
    exit(1);
  }
  return s;
}


//Sizea is twice the number of states, to allow for barred symbols
//Use negative numbers for barred versions
int
StateTrans::CombineSymbol(int first, int second){
  
  if (first < 0) first = numbarred - first;
  if (second < 0) second = numbarred - second;
  
  return  (first-1)*(sizea+1) + (second-1) + 1;
}

int 
StateTrans::SplitSymbolFirst(int symbol){
  
  int split = (symbol / (sizea+1)) + 1;
  if (split>numbarred) return numbarred - split;
  return split;
}


int StateTrans::SplitSymbolSecond(int symbol){

  int split = symbol % (sizea+1);
  if (split>numbarred) return numbarred - split;
  return split;
}


//If o has transitions (x,y) a->b and (x,z) a->b
//Then we want the duel to have (a,b) x->y and (a,b) x->z :- Non deterministic
StateTrans::StateTrans(DirFsa *o) {

  Dir t;
  int osize=o->SizeA();      //Since o is always a DirFsa
  int a,x,y;
  int opadding = osize+1, **curtable; //Transition table

  o->SetupAccepting();
  fsa_init(mult);
  mult->flags[NFA]=true;
  mult->table->denserows=0;

  //Need a new alphabet 
  //One symbol for each state of o, plus one for the fail state
  numbarred = o->Size()+1;
  numsym = sizea = sizeb = numbarred*2;
  int failsym = numbarred, maxtrans=(sizea+1)*(sizea+1);
  CreateAlphabet(mult, sizea); 
  
  //Make states, one for each generator plus padding symbol, plus a fail state
  numstate = t.alphabet.GenNum()+2;
  //cout << "Numstate " << numstate << endl;
  int failstate=numstate;
  int paddingstate = numstate-1;
  //cout << "Fail state " << failstate << endl;

  mult->states->size = numstate;

  //Make first state initial, irrelevant at this stage
  mult->num_initial = 1;
  tmalloc(mult->initial,int,2);
  mult->initial[1] = 1;

  //Make all states accepting
  mult->num_accepting = numstate; 
  tmalloc(mult->accepting,int, numstate+1);
  for (int i=1; i<=numstate;i++) mult->accepting[i]=i;

  mult->table->maxstates=numstate;
  mult->table->table_type=SPARSE;

  //Allocate table
  tmalloc(curtable, int*, numstate+2); 
  tmalloc(curtable[0], int, 2*numstate*(maxtrans)); //Conservative

  curtable[1]=curtable[0];
  //printf("\n%p",curtable[1]);
    
  int tableindex=0;

  int letter;
  bool found;
  //Loop through all states a, not fail state, or padding state
  //Loop through all symbols x, yz
  for (a=1; a<=numstate-2;a++) { 
    for (x=1; x<numbarred; x++) {
      found = false;
      for (y=1; y<numbarred; y++) {
	//Go through all possible b
	for(int b=1; b<=osize+1;b++) {
	  letter=CombineSymbol(x, y);
	  //cout << "(" << x << "," << y << ")" << " maps back to " 
	  // << "(" << SplitSymbolFirst(letter) << "," 
	  // << SplitSymbolSecond(letter) << ")" << endl;
	  //If o has transition state x->y with symbol (a,b)
	  if (o->Target(x, a,b)==y){
	    //Add transition a->b with symbol (x,y)
	    if (b==opadding) {
	      curtable[a][tableindex++] = letter;
	      curtable[a][tableindex++] = paddingstate;
	      //cout << "Adding transition " << "(" << x << "," << y << "):"
	      //<< a << "->" << paddingstate << endl;
	    }
	    else {
	      curtable[a][tableindex++] = letter;
	      curtable[a][tableindex++] = b;
	      //cout << "Adding transition " << "(" << x << "," << y << "):"
	      //<< a << "->" << b << endl;
	    }
	    found = true;
	  }
	}	
      }
      //If y is zero (fail) then use transition a->failstate (x,fail)
      if (found == false) {
	letter=CombineSymbol(x,failsym);
	curtable[a][tableindex++] = letter;
	curtable[a][tableindex++] = failstate;
	//cout << "Adding transition " << "(" << x << "," << failsym << "):"
	//<< a << "->" << failstate << endl;
      }
    }
    curtable[a+1] = &curtable[a][tableindex];
    //cout << &curtable[a][tableindex] << endl;
    //printf("\n%p",&curtable[a][tableindex]);
    tableindex=0;
  }

  //Handle '_' symbol separately
  for (x=1; x<numbarred; x++) {
    found = false;
    for (y=1; y<numbarred; y++) {
      //Go through all possible b
      for(int b=1; b<=osize;b++) {
	letter=CombineSymbol(x, y);
	//If o has transition state x->y with symbol (_,b)
	if (o->Target(x, opadding, b)==y){
	  //Add transition _->b with symbol (x,y)
	  curtable[paddingstate][tableindex++] = letter;
	  curtable[paddingstate][tableindex++] = b;
	  found = true;
	  //cout << "Adding transition " << "(" << x << "," << y << "):"
	  //<< paddingstate << "->" << b << endl;
	}
	//************(Handle implict (_,_) transition)******** 
	//If o has x accepting then add transition paddingstate
	//to paddingstate with symbol (x,x)
	if ((x==y) && o->Accepting(x)) {
	  curtable[paddingstate][tableindex++] = letter;
	  curtable[paddingstate][tableindex++] = paddingstate;
	  //cout << "Adding transition " << "(" << x << "," << y << "):"
	  //<< paddingstate << "->" << paddingstate << endl;
	}
      }
    }
    //If y is zero (fail) then use symbol (x,fail)
    //If there are no transitions
    if (found==false) {
      letter=CombineSymbol(x,failsym);
      curtable[paddingstate][tableindex++] = letter;
      curtable[paddingstate][tableindex++] = failstate;
      //cout << "Adding transition " << "(" << x << "," << y << "):"
      //<< paddingstate << "->" << failstate << endl;
    }
  }
  curtable[paddingstate+1] = &curtable[paddingstate][tableindex];
  //cout << &curtable[paddingstate][tableindex] << endl;
  //printf("\n%p",&curtable[paddingstate][tableindex]);
    
  tableindex = 0;

  //Do the fail state 
  //Add transitions for all symbols (x,fail) back to it
  for (x = 1; x <= numbarred ; x++) {
    for (y = 1; y <= numbarred ; y++) {
      letter = CombineSymbol(x,y);
      if (y==failsym) {
	//data[letter][failstate] = failstate;
	curtable[failstate][tableindex++] = letter;
	curtable[failstate][tableindex++] = failstate;
	//cout << "Adding transition " << "(" << x << "," << y << "):"
	//<< failstate << "->" << failstate << endl;
      } 
    }
  }
  curtable[failstate+1] = &curtable[failstate][tableindex];
  //cout << &curtable[failstate][tableindex] << endl;
  //printf("\n%p",&curtable[failstate][tableindex]);
  
  mult->table->table_data_ptr = curtable;
  o->FreeAccepting();
  //cout << "Duel\n====" << endl;
  //PrintSparse();
}


StateTrans::StateTrans(StateTrans *d, int in, int out){
  
  fsa_init(mult);
  mult->flags[NFA] = true;
  //Create Alphabet, twice the size of d 
  numbarred = d->numbarred;
  int    maxtrans,
    a,x,y,
    **curtable;

  numsym = sizea = sizeb = numbarred * 2;
  maxtrans = (sizea+1)*(sizea+1);

  //cout << "Creating StateTrans for (" << in << "," << out << ")" << endl;

  CreateAlphabet(mult,sizea);
  
  //Same number of states that d has
  int numstate = d->Size();
  mult->states->size = numstate;
  mult->flags[NFA]=true;
  mult->table->denserows=0;

  
  //Make state in initial
  mult->num_initial = 1;
  tmalloc(mult->initial,int,2);
  mult->initial[1] = in;
  
  //Make all states accepting
  mult->num_accepting = numstate; 
  tmalloc(mult->accepting,int, numstate+1);
  for (int i=1; i<=numstate;i++) mult->accepting[i]=i;

  mult->table->maxstates=numstate;
  mult->table->table_type=SPARSE;
  
  tmalloc(curtable, int*, numstate+2); 
  tmalloc(curtable[0], int, 2*numstate*(maxtrans)); //Conservative


  int letter, dletter, target;
  
  curtable[1]=curtable[0];
  int tableindex=0,dtableindex=0,
    **dtable=d->mult->table->table_data_ptr;
  
  //tableindex and dtableindex curtable and dtable
  //while dcurtable[a+1]>dcurtable[a][dtableindex]
  //get transition
  //split up into x and y 
  //Add transitions for barred and unbarred symbols


  for (a=1; a<=numstate;a++) { 
    while(dtable[a+1]>&dtable[a][dtableindex]){

      dletter = dtable[a][dtableindex++];
      target = dtable[a][dtableindex++];
      x = d->SplitSymbolFirst(dletter);
      y = d->SplitSymbolSecond(dletter);
      //cout << "Found transition " << "(" << x << "," << y << "):"
      //   << a << "->" << target << endl;
	
	if (target!=0) {
	  //Add unbarred version
	  letter = CombineSymbol(x,y);
	  curtable[a][tableindex++] = letter;
	  curtable[a][tableindex++] = target;
	  //cout << "Adding transition " << "(" << x << "," << y << "):"
	  //<< a << "->" << target << endl;
	  if (target==out) {
	    //Add (bar,bar) version
	    letter = CombineSymbol(-x,-y);
	    curtable[a][tableindex++] = letter;
	    curtable[a][tableindex++] = target;
	    //cout << "Adding transition " << "(" << -x << "," << -y << "):"
	    //<< a << "->" << target << endl;
	    //cout << "Maps back to " 
	    //<< "(" << SplitSymbolFirst(letter) << "," 
	    // << SplitSymbolSecond(letter) << ")" << endl;
	  }
	  if (target!=out) {//Lose a bar version
	    letter = CombineSymbol(-x,y);
	    curtable[a][tableindex++] = letter;
	    curtable[a][tableindex++] = target;
	    //cout << "Adding transition " << "(" << -x << "," << y << "):"
	    // << a << "->" << target << endl;
	    //cout << "Maps back to " 
	    // << "(" << SplitSymbolFirst(letter) << "," 
	    // << SplitSymbolSecond(letter) << ")" << endl;
	  }
	}
    }
    curtable[a+1] = &curtable[a][tableindex];
    tableindex=0;
    dtableindex=0;
  }
  mult->table->table_data_ptr = curtable;
  //PrintSparse();
  fsa *newfsa = fsa_determinize(mult, DENSE, FALSE, "temp");
  mult = newfsa;
  //PrintSparse();
}

//Not needed
/*
void
StateTrans::MakeInitial(){

  fsa_init(mult);

  mult->flags[DFA] = true;
  //Create Alphabet, twice the size of d 
  int numdir = numbarred * 2,
    alphsize = (numdir+1)*(numdir+1),
    x,y;

  sizea = sizeb = numdir;
    
  CreateAlphabet(mult,numdir);
  
  int numstate = 1;
  mult->states->size = numstate;
  
  //Make first state initial
  mult->num_initial = 1;
  tmalloc(mult->initial,int,2);
  mult->initial[1] = 1;
  
  mult->num_accepting = numstate; 
  tmalloc(mult->accepting,int, numstate+1);
  for (int i=1; i<=numstate;i++) mult->accepting[i]=i;
  
  //Each state has full number of transitions, make dense table
  fsa_table_init(mult->table, numstate, alphsize);
  
  int **data = mult->table->table_data_ptr,
    letter;
  
    for (x=-numbarred; x<=numbarred; x++) {
      if (x==0) continue;
      for (y=-numbarred; y<=numbarred; y++) {
	if (y==0) continue;
	letter = CombineSymbol(x,y);
	if (x==-1 && y==-1)data[letter][1]=1;
	else data[letter][1]=0;
      }
    }
}
*/


void
StateTrans::MakeRemoveInactive(){

  fsa_init(mult);

  mult->flags[DFA] = true;
  //Create Alphabet, twice the size of d 
  int numdir = numbarred * 2,
    alphsize = (numdir+1)*(numdir+1),
    x,y;

  sizea = sizeb = numdir;

  CreateAlphabet(mult,numdir);
  int numstate = 1;
  mult->states->size = numstate;
  
  //Make first state initial
  mult->num_initial = 1;
  tmalloc(mult->initial,int,2);
  mult->initial[1] = 1;
  
  //Make first state accepting
  mult->num_accepting = numstate; 
  tmalloc(mult->accepting,int, numstate+1);
  mult->accepting[1]=1;
  
  //Each state has full number of transitions, make dense table
  fsa_table_init(mult->table, numstate, alphsize);
  
  int **data = mult->table->table_data_ptr,
    letter;
  //cout << "Building RemoveInactive " << numbarred << endl;
  for (x=-numbarred; x<=numbarred; x++) {
    if (x==0) continue;
    for (y=-numbarred; y<=numbarred; y++) {
      if (y==0) continue;
      letter = CombineSymbol(x,y);
      if ( ((x<0) && (y<0))
	  ||((x>0) && (y==numbarred))) {
	//cout << "Got one" << endl;
	data[letter][1]=1;
      }
      else data[letter][1]=0;
    }
  }
}


State::State(int numb) {

  numbarred = numb;
  numsym = sizea = numb*2;
}


string
State::StateName(int d) {

  string s = to_string(d);
  //tmalloc(s, char, 5);
  //sprintf(s,"%d",d);
  return s;
}

string
State::Name(int d) {

  string s;
  //tmalloc(s,char,10);

  if (d>numbarred) {

    s = "[" + to_string(d) + "]";
    
    //sprintf(s,"[%d]",d-numbarred);
  }
  if (d<=numbarred) {
    
    s = to_string(d);
    //sprintf(s,"%d",d);
  }
  if ((d<0) || (d>sizea)) {
    cerr << "State: invalid name " << d << endl;
    exit(1);
  }

  return s;
}



//Map positive and negative to symbol
int
State::MapSym(int x) {

  if (x < 0) x = numbarred - x;
  return x;
}


void
State::MakeInitial() {

  fsa_init(mult);

  mult->flags[DFA] = true;
  //Create Alphabet, twice the size of d 
  int numdir = numbarred * 2, x;

  sizea = numdir;
  
  MakeAlphabet();
  
  int numstate = 1;
  mult->states->size = numstate;
  
  //Make first state initial
  mult->num_initial = 1;
  tmalloc(mult->initial,int,2);
  mult->initial[1] = 1;
  
  mult->num_accepting = numstate; 
  tmalloc(mult->accepting,int, numstate+1);
  for (int i=1; i<=numstate;i++) mult->accepting[i]=i;
  
  //Each state has full number of transitions, make dense table
  fsa_table_init(mult->table, numstate, numdir);
  
  int **data = mult->table->table_data_ptr;
  
  //Add transitions 1 -> 1 (x)
  for (x=-numbarred; x<=numbarred; x++) {
    if (x==0) continue;
    if (x==-1) data[MapSym(x)][1]=1;
  }
  Davinci("initial");
}


//Convert all unbarred symbols to the same fail symbol
void
State::RemoveInactive(State *s) {

  int numdirs = s->mult->alphabet->size,
    numbarred = numdirs/2;

  //cout << "Remove Inactive " << numdirs << endl;
  StateTrans *st=new StateTrans(numbarred);
  st->MakeRemoveInactive();
  //st.Davinci("removeinactive");
  Combine(s,st);
}


//Is it a fail state?
//Look for barred transitions
bool
State::Fail(){

  int numdirs = mult->alphabet->size,
    numstates = mult->states->size,
  **data=mult->table->table_data_ptr;

  for (int a = 1; a<=numstates; a++){ 
    for (int x = 1+numdirs/2; x < numdirs; x++){
      if (data[x][a]!=0) return false;
    }
  }
  return true;
}

//The state has no transitions away from it
// And self transitions to it are all inactive
bool
State::LeafState(int state){

  int numdirs = mult->alphabet->size,
    **data=mult->table->table_data_ptr;

  for (int x = 1; x < numdirs; x++){
    if ((data[x][state]!=state)&&(data[x][state]!=0)) return false;
    if ((data[x][state]!=0) && (x>numdirs/2)) return false;
  }
  return true;
}

bool
State::ActiveTransTo(int state){

  int numdirs = mult->alphabet->size,
    numstates = mult->states->size,
  **data=mult->table->table_data_ptr;

  for (int a = 1; a<=numstates; a++){ 
    for (int x = 1+numdirs/2; x < numdirs; x++){
      if (data[x][a]==state) return true;
    }
  }
  return false;
}


void
State::RemoveState(int state){

  int numdirs = mult->alphabet->size,
    numstates = mult->states->size,
  **data=mult->table->table_data_ptr;
  
  //Davinci("before");

  SetupAccepting();
  bool accept = Accepting(state);
  FreeAccepting();

  for (int a = 1; a<=numstates; a++){ 
    for (int x = 1; x < numdirs; x++){
      if ((data[x][a]==state)&&accept){
	AddAccepting(a);
      }
      if ((a==state)||(data[x][a]==state)) {
	//cout << "Removing trans" << endl;
	data[x][a]=0;
      }
    }
  }
  Tidy();
  //Davinci("after");
}


//Remove an inactive 'leaf' state
// The state is a 'leaf' and there are no active transitions to it
bool
State::RemoveLeaf(){

  int  numstates = mult->states->size;

  for (int a = 1; a<=numstates; a++){ 
    if (LeafState(a)&&!ActiveTransTo(a)) {
      //cout << "Removing state " << a << endl;
      RemoveState(a);
      return true;
    }
  }
  return false;
}


//Remove all 'leaves'
void
State::Cull() {

  bool found;
  
  //MakeAllAccepting();
  do {
    found = RemoveLeaf();
  } while (found);
}


//Change
//Change so that the initial and accepting states get made properly
void
State::Reverse(){

  //Get size of f
  int numdirs=NumSymbols(),
    numstates=mult->states->size;

  fsa *midfa;

  tmalloc(midfa, fsa,1);
  fsa_init(midfa);

  //Set this to be a midfa
  midfa->flags[MIDFA]=true;
  midfa->states->size = numstates;

  //Create initial states, accepting states of f
  midfa->num_initial=mult->num_accepting;

  tmalloc(midfa->initial,int,midfa->num_initial+1);
  for(int i=1;i<=midfa->num_initial;i++) {
    if (mult->num_accepting==numstates) {midfa->initial[i]=i;}
    else{midfa->initial[i]=mult->accepting[i];}
  }


  //Create accepting states, initial states of f
  midfa->num_accepting=mult->num_initial;

  if (midfa->num_accepting < midfa->states->size){
    tmalloc(midfa->accepting,int,midfa->num_accepting+1);
    for(int i=1;i<=midfa->num_accepting;i++) 
      if (mult->num_initial==numstates){midfa->accepting[i]=i;}
      else {midfa->accepting[i]=mult->initial[i];}
  }

  MakeAlphabet(midfa); 
  
  //Create transition table
  midfa->table->maxstates=numstates;
  midfa->table->table_type=SPARSE;

  int target, **curtable,
    **fdata = mult->table->table_data_ptr;

  tmalloc(curtable, int*, numstates+2); 
  tmalloc(curtable[0], int, 2*numstates*(numdirs)); //Conservative

  curtable[1]=curtable[0];
    
  int tableindex=0;

    
  for (int state = 1; state <= numstates; state++) {
    for (int sym = 1; sym <= numdirs; sym++) {
      for (int a = 1; a <= numstates; a++) {
	//Add transition (c): state->a if f has transition (c): a->state 
	target = fdata[sym][a];
	if (target==state) {
	  curtable[state][tableindex++] = sym;
	  curtable[state][tableindex++] = a;
	  //cout << "Adding transition " << state << "->" << a 
	  //     << "(" << sym << ")" << endl;
	}
      }
    }
    curtable[state+1] = &curtable[state][tableindex];
    tableindex=0;
  }
  midfa->table->table_data_ptr = curtable;
  fsa_clear(mult);
  //Convert to dfa
  mult = fsa_determinize(midfa, DENSE, FALSE, "temp");
  fsa_clear(midfa);
}


//Accepts all active states
//Use it to prune inactive sections from states
void
State::Active(int numbarred) {

  fsa_init(mult);
  
  mult->flags[DFA] = true;

  //Create Alphabet
  int numdir = numbarred * 2,
    alphsize = (numdir+1)*(numdir+1),
    x;

  MakeAlphabet(mult);
  int numstate = 2;
  mult->states->size = numstate;
  
  //Make first state initial
  mult->num_initial = 1;
  tmalloc(mult->initial,int,2);
  mult->initial[1] = 1;
  
  //Make second state accepting
  mult->num_accepting = 1; 
  tmalloc(mult->accepting,int, 2);
  mult->accepting[1]=2;
  
  //Each state has full number of transitions, make dense table
  fsa_table_init(mult->table, numstate, alphsize);
  
  int **data = mult->table->table_data_ptr;

  //Do state one
  for (x=1; x<=numdir; x++) {
    if ((x<=numbarred)|| (x==numdir)) data[x][1]=1;
    else data[x][1]=2;
  }
  //Do state two
  for (x=1; x<=numdir; x++) {
    data[x][2]=2;
  }
}


ClosureMaker::ClosureMaker(DirFsa *o) {

  statenum=0;
  numbarred = o->Size()+1;
  oper = o;
  oper->Davinci("for-closing");
  for (int i=0; i<MAXSTATES; i++) st[i]=NULL;
  closure = new DirFsa();
}

DirFsa *
ClosureMaker::Make() {

  Dir t;
  StateTrans *duel;
  StateTrans *s;
  duel = new StateTrans(oper);   //Build duel operation
  numdir=t.alphabet.GenNum();
  char filename[30];

  //cout << "Making closure" << endl;

  for(int x=1;x<=numdir+1;x++) {
    for(int y=1;y<=numdir+1;y++) {
      if ((x==numdir+1)&&(y==numdir+1)) continue;
      s = new StateTrans(duel,x,y);
      trans[x][y]=s;
      if (debugBuildStates){
	sprintf(filename,"statetrans-%d-%d",x,y);
	trans[x][y]->Davinci(filename);
      }
    }
  }

  //for (int k=6; k<7; k++) {

  
  if (BuildStatesViaTrans(35)) {}
  closure->Davinci("closure");
  return closure;
  

  //BuildStates(7);
  //return closure;


  //}
  //}
  
  //d.Determinize();
  //d.Davinci("Duel");
}

int
ClosureMaker::StateNumber(State *s) {

  for (int i=0;i<statenum;i++) {
    if ((st[i])&&(s->ContainedIn(st[i]))) return i;
  }
  return -1;
}


void
ClosureMaker::Compare(State *s) {

  cout << "Contains states" ;
  for (int i=0;i<statenum;i++) {
    if (st[i]->ContainedIn(s)) {
      cout << i << " " ;
    }
  }
  cout << endl;
  cout << "Overlaps with states" ;
  for (int i=0;i<statenum;i++) {
    if (st[i]->Overlaps(s)) {
      cout << i << " " ;
    }
  }
  cout << endl;
}

void
ClosureMaker::OutputOverlaps() {

  char filename[20];
  State *overlap = new State(numbarred);

  for (int i=0; i<statenum;i++){
    for (int j=i+1; j<statenum;j++){
      overlap->And(st[i],st[j]);      
      sprintf(filename,"overlap-%d-%d",i,j);
      overlap->Davinci(filename);
    }
  }
}

void
ClosureMaker::RemoveOverlap(State *s) {

  for (int i=0;i<statenum;i++) {
    s->AndNot(st[i]);   
  }
}


bool
ClosureMaker::BuildStates(int max){

  //StateTrans *init,
  //  *comb, *doub;

  State *curr = new State(numbarred), 
    *start = new State(numbarred), *active = new State(numbarred);

  char filename[30];
  //Dir d;
  int u,v;

  //comb = new StateTrans(numbarred);
  //doub = new StateTrans(numbarred);

  //Initialise the closure operator
  Dir t;
  int cnumdir=t.alphabet.Size();
  int calphsize = (cnumdir+1)*(cnumdir+1);
  fsa *mult=closure->mult;

  fsa_init(mult);

  mult->flags[DFA] = true;
  //Make states, initial and accepting
  mult->states->size = max+1;
  mult->num_initial = 1;
  tmalloc(mult->initial, int, 2);
  mult->initial[1] = 1;

  //Make alphabet
  closure->MakeAlphabet();

  //Make table
  int letter;
  fsa_table_init(mult->table, max+1, calphsize);
  int **data = mult->table->table_data_ptr;
  DLList<int> accepting;
  //End initialise


  cout << "Building states ";

  active->Active(numbarred);
  active->Davinci("active");
 
  //init = new StateTrans(numbarred);
  start = new State(numbarred);
  //init->MakeInitial();  //Create initial state
  //init->Davinci("initial");
  start->MakeInitial();
  //start->ExistsFirst(init);
  start->Davinci("initial");
  st[0]=start;
  statenum++;

    for (int a=0; a<=max;a++) {
      if (st[a]==NULL) break;
      //st[a]->Davinci("curr-state");
      cout << a << "-" << flush;
      //if (true) {
      if (Accept(st[a])) {
	accepting.append(a+1);
	//cout << " - ACCEPTING" << endl; 
      }
      //else cout << endl;
      //Compare(st[a]);
      for(int x=1;x<=numdir+1;x++) {
	//Only look at gen or padding
	if ((x!=numdir+1)&&(t.alphabet.Type(x)!=GEN)) continue;  
	for(int y=1;y<=numdir+1;y++) {
	  if ((y!=numdir+1)&&(t.alphabet.Type(y)!=GEN)) continue;
	  if ((x==numdir+1)&&(y==numdir+1)) continue;
	  u=x;v=y;
	  if (x==numdir+1) u = cnumdir+1;
	  if (y==numdir+1) v = cnumdir+1;
	  letter=closure->CombineSymbol(u,v);
	  //Apply trans[x][y] to st[a]
	  //cout << "Looking at transition " << a << ":" 
  	  //     << "(" << x << "," << y << ")" << endl;
	  curr = new State(numbarred);
	  //trans[x][y]->Davinci("curr-trans");
	  curr->Combine(st[a], trans[x][y]);
	  //combined.Davinci("combined");

	  //Normalise inactive portions, doesn't work
	  //curr->RemoveInactive(&combined);

	  

	  //Remove inactive portions
	  //curr->Davinci("orig");
	  //curr->And(active);
	  //curr->Davinci("anded");
	  //curr->Cull();
  	  //curr->Davinci("culled");

	  //Do reverse removal, doesn't work
 	  //curr->Reverse();
 	  //curr->Davinci("firstreverse");
 	  //curr->Cull();
 	  //curr->Davinci("reverseculled");
 	  //curr->Reverse();
 	  //curr->Davinci("secondreverse");
	      
	  //If new state is a fail, ignore
	  if (curr->Fail()) { 
	    /*
	    cout << "Fail State found " ;
	    cout << a << " for symbol (" 
	         << t.alphabet.DirName(x) << "," 
	         << t.alphabet.DirName(y) << ")" << endl;

	    if ((x==1)&&(y==1)&&(a==2)) curr->Davinci("special");
	    */
	    delete curr;
	    continue;
	  }
	  //Else add to our state array 
	  int found = StateNumber(curr);

	  if (found > max) {  //Try to cull to an existing state
	    found = StateNumber(curr);
	  }
	  
	  //If we don't already have it and have room, add it
	  if (found<0){
	    //sprintf(filename, "state%d-before",statenum);
	    //curr->Davinci(filename);
	    //RemoveOverlap(curr);
	    //sprintf(filename, "state%d-after",statenum);
	    //curr->Davinci(filename);
	    //found = StateNumber(curr);
	    
	    st[statenum]=curr;
	    
	    cout << "Found transistion " << a << "->" 
	         << statenum << " for symbol (" 
	         << t.alphabet.DirName(x) << "," 
	         << t.alphabet.DirName(y) << ")" << endl; 
	    sprintf(filename, "state%d",statenum);
	    curr->Davinci(filename);
	    

	    if (statenum > max) { //Add transition back to self
	      data[letter][a+1]=a+1;
	      accepting.append(a+1);
	      delete (st[statenum]);
	      st[statenum]=NULL;
	    }
	    else {data[letter][a+1]=statenum+1;}
	    statenum++;
	  }
	  else {
	    cout << "Found transistion " << a << "->" 
		   << found << " for symbol (" 
		 << t.alphabet.DirName(x) << "," 
		 << t.alphabet.DirName(y) << ")" << endl; 
	    sprintf(filename, "state%d",statenum);
	    curr->Davinci(filename);
	    
	    //Add transistion
	    if (found > max){  //Map back to self
	      data[letter][a+1]=a+1;
	      accepting.append(a+1);
	      delete (st[statenum]);
	      st[statenum]=NULL;  
	    }
	    else {data[letter][a+1]=found+1;}
	  }
	  if (statenum==MAXSTATES) return false;
	  //exit(1);
	}
      }
    }
    //OutputOverlaps();


    closure->SetAccept(&accepting);
    closure->Davinci("raw-closure");


    //Tidy up the suspected closure by ensuring that it is legal 
    // and includes the identity operation
    
    DirFsa *equal=new DirFsa(),*legal=new DirFsa();
    equal->MakeIdentity();
    legal->MakeLegal();

    closure->Or(equal);
    closure->And(legal);
    closure->Tidy();

    cout << endl;

    //closure->Davinci("tidyclosure");
    if (closure->IsClosureOf(oper)) {

      cout << "Operation is safe approximation to closure" 
	<< endl;
      return true;
    }
    else {
      cout << "Sorry, only unsafe approximation found" << endl;
      return false;
    }
}



//Use set of transition symbols to determine whether states are equivalent
//This method subsumes the other
bool
ClosureMaker::BuildStatesViaTrans(int max){

  //StateTrans *init,
  //  *comb, *doub;

  State *curr = new State(numbarred), 
    *start = new State(numbarred), *active = new State(numbarred);

  string filename;
  int u,v;

  //comb = new StateTrans(numbarred);
  //doub = new StateTrans(numbarred);

  //Initialise the closure operator
  Dir t;
  int cnumdir=t.alphabet.Size();
  int calphsize = (cnumdir+1)*(cnumdir+1);
  fsa *mult=closure->mult;

  fsa_init(mult);

  mult->flags[DFA] = true;
  //Make states, initial and accepting
  mult->states->size = max+1;
  mult->num_initial = 1;
  tmalloc(mult->initial, int, 2);
  mult->initial[1] = 1;

  //Make alphabet
  closure->MakeAlphabet();

  //Make table
  int letter;
  fsa_table_init(mult->table, max+1, calphsize);
  int **data = mult->table->table_data_ptr;
  DLList<int> accepting;
  //End initialise


  cout << "Building states ";

  active->Active(numbarred);
  active->Davinci("active");
 
  //init = new StateTrans(numbarred);
  start = new State(numbarred);
  start->MakeInitial();
  start->Davinci("initial");
  st[0]=start;
  statenum++;

    for (int a=0; a<=max;a++) {
      if (st[a]==NULL) continue;
      cout << a << "-" << flush;
      if (Accept(st[a])) {
	accepting.append(a+1);
	//cout << " - ACCEPTING" << endl; 
      }
      //Compare(st[a]);
      for(int x=1;x<=numdir+1;x++) {
	//Only look at gen or padding
	if ((x!=numdir+1)&&(t.alphabet.Type(x)!=GEN)) continue;  
	for(int y=1;y<=numdir+1;y++) {
	  if ((y!=numdir+1)&&(t.alphabet.Type(y)!=GEN)) continue;
	  if ((x==numdir+1)&&(y==numdir+1)) continue;
	  u=x;v=y;
	  if (x==numdir+1) u = cnumdir+1;
	  if (y==numdir+1) v = cnumdir+1;
	  letter=closure->CombineSymbol(u,v);
	  //Apply trans[x][y] to st[a]
	  //cout << "Looking at transition " << a << ":" 
  	  //     << "(" << x << "," << y << ")" << endl;
	  curr = new State(numbarred);
	  //trans[x][y]->Davinci("curr-trans");
	  curr->Combine(st[a], trans[x][y]);

	  //If new state is a fail, ignore
	  if (curr->Fail()) { 
	    /*
	    cout << "Fail State found " ;
	    cout << a << " for symbol (" 
	         << t.alphabet.DirName(x) << "," 
	         << t.alphabet.DirName(y) << ")" << endl;

	    if ((x==1)&&(y==1)&&(a==2)) curr->Davinci("special");
	    */
	    delete curr;
	    continue;
	  }
	  //Else add to our state array 
	  int found = StateNumber(curr);

	  if (found > max) {  //Try to cull to an existing state
	    found = StateNumber(curr);
	  }
	  
	  //If we don't already have it and have room, add it
	  if (found<0){
	    //sprintf(filename, "state%d-before",statenum);
	    //curr->Davinci(filename);
	    //RemoveOverlap(curr);
	    //sprintf(filename, "state%d-after",statenum);
	    //curr->Davinci(filename);
	    //found = StateNumber(curr);
	    
	    st[statenum]=curr;
	
	    if (debugBuildStates) {
	    cout << "Found transistion " << a << "->" 
	         << statenum << " for symbol (" 
	         << t.alphabet.DirName(x) << "," 
	         << t.alphabet.DirName(y) << ")" << endl; 
	    //sprintf(filename, "state%d",statenum);
	    filename = "state" + to_string(statenum);
	    }
	    //curr->Davinci(filename);
	    

	    if (statenum > max) { //Add transition back to self
	      data[letter][a+1]=a+1;
	      accepting.append(a+1);
	      delete (st[statenum]);
	      st[statenum]=NULL;
	    }
	    else {data[letter][a+1]=statenum+1;}
	    statenum++;
	  }
	  else {
	    if (debugBuildStates) {
	    cout << "Found transistion " << a << "->" 
		   << found << " for symbol (" 
		 << t.alphabet.DirName(x) << "," 
		 << t.alphabet.DirName(y) << ")" << endl; 
	    //sprintf(filename, "state%d",statenum);
	    filename = "state" + to_string(statenum);
	    }
	    //curr->Davinci(filename);
	    
	    //Add transistion
	    if (found > max){  //Map back to self
	      data[letter][a+1]=a+1;
	      accepting.append(a+1);
	      delete (st[statenum]);
	      st[statenum]=NULL;  
	    }
	    else {data[letter][a+1]=found+1;}
	  }
	  if (statenum==MAXSTATES) return false;
	}
      }
      //Compare transitions with all earlier states
      for(int b = 0; b<a; b++) {
	if (SameTrans(a+1,b+1)) {
	  //Remove a and change all transistions to a, to b
	  if (debugBuildStates) {
	    cout << "State " << a << "is really the same as state " << b << endl; 
	  }	  
	  RemoveState(a+1,b+1);
	  delete st[a];
	  st[a] = NULL;
	  break;
	}
      }
    }
    //OutputOverlaps();


    closure->SetAccept(&accepting);
    closure->Davinci("raw-closure");


    //Tidy up the suspected closure by ensuring that it is legal 
    // and includes the identity operation
    
    DirFsa *equal=new DirFsa(),*legal=new DirFsa();
    equal->MakeIdentity();
    legal->MakeLegal();

    closure->Or(equal);
    closure->And(legal);
    closure->Tidy();

    cout << endl;

    //closure->Davinci("tidyclosure");
    if (closure->IsClosureOf(oper)) {

      cout << "Operation is safe approximation to closure" 
	<< endl;
      return true;
    }
    else {
      cout << "Sorry, only unsafe approximation found" << endl;
      return false;
    }
}

bool
ClosureMaker::SameTrans(int a, int b){

  int **data = closure->mult->table->table_data_ptr;
  int letter, u,v;
  Dir t;
  int cnumdir=t.alphabet.Size();

  for(int x=1;x<=numdir+1;x++) {
    //Only look at gen or padding
    if ((x!=numdir+1)&&(t.alphabet.Type(x)!=GEN)) continue;  
    for(int y=1;y<=numdir+1;y++) {
      if ((y!=numdir+1)&&(t.alphabet.Type(y)!=GEN)) continue;
      if ((x==numdir+1)&&(y==numdir+1)) continue;
      u=x;v=y;
      if (x==numdir+1) u = cnumdir+1;
      if (y==numdir+1) v = cnumdir+1;
      letter=closure->CombineSymbol(u,v);
      if (((data[letter][a]==0) && (data[letter][b]!=0)) ||
      ((data[letter][a]!=0) && (data[letter][b]==0))) return false;
    }
  }
  return true;
}


//Remove all transitions to a state
void 
ClosureMaker::RemoveState(int remove, int replace){

  int **data = closure->mult->table->table_data_ptr;
  int letter, u,v;
  Dir t;
  int ignorestate;
  int cnumdir=t.alphabet.Size();

  for (int a = 1 ; a<remove; a++) {
    for(int x=1;x<=numdir+1;x++) {
      //Only look at gen or padding
      if ((x!=numdir+1)&&(t.alphabet.Type(x)!=GEN)) continue;  
      for(int y=1;y<=numdir+1;y++) {
	if ((y!=numdir+1)&&(t.alphabet.Type(y)!=GEN)) continue;
	if ((x==numdir+1)&&(y==numdir+1)) continue;
	u=x;v=y;
	if (x==numdir+1) u = cnumdir+1;
	if (y==numdir+1) v = cnumdir+1;
	letter=closure->CombineSymbol(u,v);
	if (data[letter][a]==remove) data[letter][a] = replace;
      }
    }
  }
  //All states we can get to from remove should be ignored
  //Is there a way of making this work ???
 
  for(int x=1;x<=numdir+1;x++) {
    //Only look at gen or padding
    if ((x!=numdir+1)&&(t.alphabet.Type(x)!=GEN)) continue;  
    for(int y=1;y<=numdir+1;y++) {
      if ((y!=numdir+1)&&(t.alphabet.Type(y)!=GEN)) continue;
      if ((x==numdir+1)&&(y==numdir+1)) continue;
      u=x;v=y;
      if (x==numdir+1) u = cnumdir+1;
      if (y==numdir+1) v = cnumdir+1;
      letter=closure->CombineSymbol(u,v);
      ignorestate = data[letter][remove];
      if ((ignorestate!=0)&&(ignorestate!=data[letter][replace])){
	if (debugBuildStates){
	  cout << "Ignoring state " << ignorestate-1 << endl;
	}
	delete st[ignorestate-1]; 
	st[ignorestate-1] = NULL;
      }
    }
  }
  
}


//Does it contain an accept state?
bool
ClosureMaker::Accept(State *s) {

  if (s->Empty()) return false;
  int numdirs = s->NumSymbols(),
    numbarred = numdirs/2,
    numstates = s->Size();

  oper->SetupAccepting();
  for (int a = 1; a<=numstates; a++){ 
    for (int x = numbarred+1; x < numdirs; x++){
      if ((s->Target(a,x)!=0) && (oper->Accepting(x-numbarred))){
	oper->FreeAccepting();
	return true;
      }
    }
  }
  oper->FreeAccepting();
  return false;
}






