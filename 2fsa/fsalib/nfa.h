fsa *
fsa_determinize(
	fsa *fsaptr,
	storage_type op_table_type,
	boolean destroy,
	const char *tempfilename);
fsa *
fsa_determinize_short(
	fsa *fsaptr,
	storage_type op_table_type,
	boolean destroy,
	const char *tempfilename);
fsa *
fsa_determinize_int(
        fsa *fsaptr,
	storage_type op_table_type,
	boolean destroy,
	const char *tempfilename);
