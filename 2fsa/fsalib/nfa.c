/* file nfa.c  8.9.97.
 *
 * This file contains routines for processing nfa's i.e. non-deterministic
 * finite state automata.
 * Currently, the only routine available is fsa_determinize, which returns
 * an equivalent deterministic automaton, whose states are subsets of the
 * input fsa.
 * Currently, the only input format accepted is sparse, where epsilon
 * transitions are from generator 0.
 */

#include <stdio.h>
#include <unistd.h>
#include "defs.h"
#include "fsa.h"
#include "hash.h"
#include "externals.h"
#include "nfa.h"
#include "fsaio.h"

fsa *
fsa_determinize(
	fsa *fsaptr,
	storage_type op_table_type,
	boolean destroy,
	const char *tempfilename)
/* The fsa *fsaptr must be an fsa with sparse table.
 * The returned fsa accepts the same language but is deterministic.
 */
{
  if (print_level>=3)
    printf("    #Calling fsa_determinize.\n");
  if (fsaptr->states->size < MAXUSHORT)
    return fsa_determinize_short(fsaptr,op_table_type,destroy,tempfilename);
  else
    return fsa_determinize_int(fsaptr,op_table_type,destroy,tempfilename);
}

fsa *
fsa_determinize_short(
	fsa *fsaptr,
	storage_type op_table_type,
	boolean destroy,
	const char *tempfilename)
{
  int **table, *tableptr, *tableptre, ngens, nssi, nsi, ns, *fsarow,
      nt, cstate, csi, im, i, g1, len, ct;
  unsigned short *ht_ptr, *ht_chptr, *ht_ptrb, *ht_ptre,
                 *cs_ptr, *cs_ptre, *ptr;
  boolean dense_op;
  short_hash_table ht;
  fsa *det;
  FILE *tempfile;

  if (print_level>=3)
    printf("    #Calling fsa_determinize_short.\n");

  tmalloc(det,fsa,1);
  fsa_init(det);
  srec_copy(det->alphabet,fsaptr->alphabet);
  det->flags[DFA] = TRUE;
  det->flags[ACCESSIBLE] = TRUE;
  det->flags[BFS] = TRUE;

  det->states->type = SIMPLE;

  det->table->table_type = op_table_type;
  det->table->denserows = 0;
  det->table->printing_format = op_table_type;

  if (fsaptr->num_initial==0) {
    det->num_initial = 0;
    det->num_accepting = 0;
    det->states->size = 0;
    if (destroy)
      fsa_clear(fsaptr);
    return det;
  }
  ngens = det->alphabet->size;
  nsi = fsaptr->states->size;

  fsa_set_is_accepting(fsaptr);

  dense_op = op_table_type==DENSE;
  table = fsaptr->table->table_data_ptr;

  det->num_initial = 1;
  tmalloc(det->initial,int,2);
  det->initial[1] = 1;
  
  short_hash_init(&ht,FALSE,0,0,0);
  ht_ptrb = ht.current_ptr;
  ht_ptre = ht_ptrb-1;
  nssi = fsaptr->num_initial;
  for (i=0;i<nssi;i++)
    *(++ht_ptre) = fsaptr->initial[i+1];
  /* now perform epsilon closure of initial subset */
  ptr=ht_ptrb-1;
  while (++ptr<=ht_ptre) {
    tableptr=table[*ptr]; tableptre=table[*ptr+1];
    while (tableptr < tableptre){
      if (*tableptr==0) {
        csi = *(tableptr+1);
        if (csi==0)
          continue;
        if (csi> *ht_ptre) {
    /* We have a new state for the image subset to be added to the end */
          *(++ht_ptre) = csi;
        }
        else {
          ht_chptr = ht_ptrb;
          while (*ht_chptr < csi)
            ht_chptr++;
          if (csi < *ht_chptr) {
    /* we have a new state for the image subset to be added in the middle */
            ht_ptr = ++ht_ptre;
            while (ht_ptr > ht_chptr) {
              *ht_ptr = *(ht_ptr-1);
              ht_ptr--;
            }
            *ht_ptr = csi;
          }
        }
      }
      tableptr+=2;
    }
  }
  im = short_hash_locate(&ht,ht_ptre-ht_ptrb+1);
/* Each state in 'det' will be represented as a subset of the set of states
 * of *fsaptr. The initial state contains the initial states
 * of *fsaptr.
 * A subset is an accept-state if it contains an accept state of *fsaptr
 * The subsets will be stored as variable-length records in the hash-table,
 * always in increasing order.
 */
  if (im!=1) {
    fprintf(stderr,"Hash-initialisation problem in fsa_determinize.\n");
    exit(1);
  }
  if ((tempfile=fopen(tempfilename,"w"))==0){
    fprintf(stderr,"Error: cannot open file %s\n",tempfilename);
    exit(1);
  }
  if (dense_op)
    tmalloc(fsarow,int,ngens)
  else
    tmalloc(fsarow,int,2*ngens+1)
 
  cstate = 0;
  if (dense_op)
    len = ngens; /* The length of the fsarow output. */
  nt = 0; /* Number of transitions in det */

  while (++cstate <= ht.num_recs) {
    if (print_level>=3) {
      if ((cstate<=1000 && cstate%100==0)||(cstate<=10000 && cstate%1000==0)||
          (cstate<=100000 && cstate%5000==0) || cstate%50000==0)
       printf("    #cstate = %d;  number of states = %d.\n",cstate,ht.num_recs);
    }
    cs_ptr = short_hash_rec(&ht,cstate);
    cs_ptre = short_hash_rec(&ht,cstate) + short_hash_rec_len(&ht,cstate) - 1;
    if (!dense_op)
      len = 0;

    for (g1=1;g1<=ngens;g1++) {
      /* Calculate action of generator g1 on state cstate  - to get the image, we
       * simply apply it to each state in the subset of states representing cstate.
       */
      ht_ptrb = ht.current_ptr;
      ht_ptre = ht_ptrb-1;
      ptr = cs_ptr-1;
      while (++ptr <= cs_ptre) {
        tableptr=table[*ptr]; tableptre=table[*ptr+1]; 
        while (tableptr < tableptre){
          if (*tableptr==g1) {
            csi = *(tableptr+1);
            if (csi==0)
              continue;
            if (ht_ptrb>ht_ptre || csi> *ht_ptre) {
	      /* We have a new state for the image subset to be added to the end */
              *(++ht_ptre) = csi;
            }
            else {
              ht_chptr = ht_ptrb;
              while (*ht_chptr < csi)
                ht_chptr++;
              if (csi < *ht_chptr) {
		/* we have a new state for the image subset to be added in the middle */
                ht_ptr = ++ht_ptre;
                while (ht_ptr > ht_chptr) {
                  *ht_ptr = *(ht_ptr-1);
                  ht_ptr--;
                }
                *ht_ptr = csi;
              }
            }
          }
          tableptr+=2;
        }
      }
      /* now perform epsilon closure of image subset */
      ptr=ht_ptrb-1;
      while (++ptr<=ht_ptre) {
        tableptr=table[*ptr]; tableptre=table[*ptr+1];
        while (tableptr < tableptre){
          if (*tableptr==0) {
            csi = *(tableptr+1);
            if (csi==0)
              continue;
            if (csi> *ht_ptre) {
	      /* We have a new state for the image subset to be added to the end */
              *(++ht_ptre) = csi;
            }
            else {
              ht_chptr = ht_ptrb;
              while (*ht_chptr < csi)
                ht_chptr++;
              if (csi < *ht_chptr) {
		/* we have a new state for the image subset to be added in the middle */
                ht_ptr = ++ht_ptre;
                while (ht_ptr > ht_chptr) {
                  *ht_ptr = *(ht_ptr-1);
                  ht_ptr--;
                }
                *ht_ptr = csi;
              }
            }
          }
          tableptr+=2;
        }
      }
      im = short_hash_locate(&ht,ht_ptre-ht_ptrb+1);
      if (dense_op)
         fsarow[g1-1] = im;
      else if (im>0) {
         fsarow[++len] = g1;
         fsarow[++len] = im;
      }
      if (im>0)
        nt++;
    }
    if (!dense_op)
      fsarow[0] = len++;
    fwrite((void *)fsarow,sizeof(int),(size_t)len,tempfile);
  }
  fclose(tempfile);

  ns = det->states->size = ht.num_recs;
  det->table->numTransitions = nt;

  /* Locate the accept states. A state is an accept state if and only if
   * the subset representing it contains an accept state of *fsaptr.
   */
  tmalloc(det->is_accepting,boolean,ns+1);
  for (i=1;i<=ns;i++)
    det->is_accepting[i] = FALSE;
  ct = 0;
  for (cstate=1;cstate<=ns;cstate++) {
    cs_ptr = short_hash_rec(&ht,cstate);
    cs_ptre = short_hash_rec(&ht,cstate) + short_hash_rec_len(&ht,cstate) - 1;
    /* See if the set contains an accept-state */
    ptr = cs_ptr-1;
    while (++ptr <= cs_ptre) if (fsaptr->is_accepting[*ptr]) {
      det->is_accepting[cstate] = TRUE;
      ct++;
      break;
    }
  }

  det->num_accepting = ct;
  if (ct==1 || ct != ns) {
    tmalloc(det->accepting,int,ct+1);
    ct = 0;
    for (i=1;i<=ns;i++) if (det->is_accepting[i])
      det->accepting[++ct] = i;
  }
  tfree(fsaptr->is_accepting);
  tfree(det->is_accepting);
  short_hash_clear(&ht);
  tfree(fsarow);

  if (destroy)
    fsa_clear(fsaptr);

  /* Now read the transition table back in */
  tempfile = fopen(tempfilename,"r");
  compressed_transitions_read(det,tempfile);
  fclose(tempfile);

  unlink(tempfilename);

  return det;
}

fsa *
fsa_determinize_int(
        fsa *fsaptr,
	storage_type op_table_type,
	boolean destroy,
	const char *tempfilename)
{
  int **table, *tableptr, *tableptre, ngens, nssi, nsi, ns, *fsarow,
      nt, cstate, csi, im, i, g1, len, ct;
  int *ht_ptr, *ht_chptr, *ht_ptrb, *ht_ptre,
                 *cs_ptr, *cs_ptre, *ptr;
  boolean dense_op;
  hash_table ht;
  fsa *det;
  FILE *tempfile;

  if (print_level>=3)
    printf("    #Calling fsa_determinize_short.\n");

  tmalloc(det,fsa,1);
  fsa_init(det);
  srec_copy(det->alphabet,fsaptr->alphabet);
  det->flags[DFA] = TRUE;
  det->flags[ACCESSIBLE] = TRUE;
  det->flags[BFS] = TRUE;

  det->states->type = SIMPLE;

  det->table->table_type = op_table_type;
  det->table->denserows = 0;
  det->table->printing_format = op_table_type;

  if (fsaptr->num_initial==0) {
    det->num_initial = 0;
    det->num_accepting = 0;
    det->states->size = 0;
    if (destroy)
      fsa_clear(fsaptr);
    return det;
  }
  ngens = det->alphabet->size;
  nsi = fsaptr->states->size;

  fsa_set_is_accepting(fsaptr);

  dense_op = op_table_type==DENSE;
  table = fsaptr->table->table_data_ptr;

  det->num_initial = 1;
  tmalloc(det->initial,int,2);
  det->initial[1] = 1;
  
  hash_init(&ht,FALSE,0,0,0);
  ht_ptrb = ht.current_ptr;
  ht_ptre = ht_ptrb-1;
  nssi = fsaptr->num_initial;
  for (i=0;i<nssi;i++)
    *(++ht_ptre) = fsaptr->initial[i+1];
  /* now perform epsilon closure of initial subset */
  ptr=ht_ptrb-1;
  while (++ptr<=ht_ptre) {
    tableptr=table[*ptr]; tableptre=table[*ptr+1];
    while (tableptr < tableptre) if (*tableptr==0) {
      csi = *(tableptr+1);
      if (csi==0)
        continue;
      if (csi> *ht_ptre) {
	/* We have a new state for the image subset to be added to the end */
        *(++ht_ptre) = csi;
      }
      else {
        ht_chptr = ht_ptrb;
        while (*ht_chptr < csi)
          ht_chptr++;
        if (csi < *ht_chptr) {
	  /* we have a new state for the image subset to be added in the middle */
          ht_ptr = ++ht_ptre;
          while (ht_ptr > ht_chptr) {
            *ht_ptr = *(ht_ptr-1);
            ht_ptr--;
          }
          *ht_ptr = csi;
        }
      }
      tableptr+=2;
    }
  }
  im = hash_locate(&ht,ht_ptre-ht_ptrb+1);
  /* Each state in 'det' will be represented as a subset of the set of states
   * of *fsaptr. The initial state contains the initial states
   * of *fsaptr.
   * A subset is an accept-state if it contains an accept state of *fsaptr
   * The subsets will be stored as variable-length records in the hash-table,
   * always in increasing order.
   */
  if (im!=1) {
    fprintf(stderr,"Hash-initialisation problem in fsa_determinize.\n");
    exit(1);
  }
  if ((tempfile=fopen(tempfilename,"w"))==0){
    fprintf(stderr,"Error: cannot open file %s\n",tempfilename);
    exit(1);
  }
  if (dense_op)
    tmalloc(fsarow,int,ngens)
  else
    tmalloc(fsarow,int,2*ngens+1)
 
  cstate = 0;
  if (dense_op)
    len = ngens; /* The length of the fsarow output. */
  nt = 0; /* Number of transitions in det */

  while (++cstate <= ht.num_recs) {
    if (print_level>=3) {
      if ((cstate<=1000 && cstate%100==0)||(cstate<=10000 && cstate%1000==0)||
          (cstate<=100000 && cstate%5000==0) || cstate%50000==0)
       printf("    #cstate = %d;  number of states = %d.\n",cstate,ht.num_recs);
    }
    cs_ptr = hash_rec(&ht,cstate);
    cs_ptre = hash_rec(&ht,cstate) + hash_rec_len(&ht,cstate) - 1;
    if (!dense_op)
      len = 0;

    for (g1=1;g1<=ngens;g1++) {
      /* Calculate action of generator g1 on state cstate  - to get the image, we
       * simply apply it to each state in the subset of states representing cstate.
       */
      ht_ptrb = ht.current_ptr;
      ht_ptre = ht_ptrb-1;
      ptr = cs_ptr-1;
      while (++ptr <= cs_ptre) {
        tableptr=table[*ptr]; tableptre=table[*ptr+1]; 
        while (tableptr < tableptre) if (*tableptr==g1) {
          csi = *(tableptr+1);
          if (csi==0)
            continue;
          if (ht_ptrb>ht_ptre || csi> *ht_ptre) {
	    /* We have a new state for the image subset to be added to the end */
            *(++ht_ptre) = csi;
          }
          else {
            ht_chptr = ht_ptrb;
            while (*ht_chptr < csi)
              ht_chptr++;
            if (csi < *ht_chptr) {
	      /* we have a new state for the image subset to be added in the middle */
              ht_ptr = ++ht_ptre;
              while (ht_ptr > ht_chptr) {
                *ht_ptr = *(ht_ptr-1);
                ht_ptr--;
              }
              *ht_ptr = csi;
            }
          }
        }
        tableptr+=2;
      }
      /* now perform epsilon closure of image subset */
      ptr=ht_ptrb-1;
      while (++ptr<=ht_ptre) {
        tableptr=table[*ptr]; tableptre=table[*ptr+1];
        while (tableptr < tableptre) if (*tableptr==0) {
          csi = *(tableptr+1);
          if (csi==0)
            continue;
          if (csi> *ht_ptre) {
	    /* We have a new state for the image subset to be added to the end */
            *(++ht_ptre) = csi;
          }
          else {
            ht_chptr = ht_ptrb;
            while (*ht_chptr < csi)
              ht_chptr++;
            if (csi < *ht_chptr) {
	      /* we have a new state for the image subset to be added in the middle */
              ht_ptr = ++ht_ptre;
              while (ht_ptr > ht_chptr) {
                *ht_ptr = *(ht_ptr-1);
                ht_ptr--;
              }
              *ht_ptr = csi;
            }
          }
        }
      }
      im = hash_locate(&ht,ht_ptre-ht_ptrb+1);
      if (dense_op)
         fsarow[g1-1] = im;
      else if (im>0) {
         fsarow[++len] = g1;
         fsarow[++len] = im;
      }
      if (im>0)
        nt++;
    }
    if (!dense_op)
      fsarow[0] = len++;
    fwrite((void *)fsarow,sizeof(int),(size_t)len,tempfile);
  }
  fclose(tempfile);

  ns = det->states->size = ht.num_recs;
  det->table->numTransitions = nt;
  
  /* Locate the accept states. A state is an accept state if and only if
   * the subset representing it contains an accept state of *fsaptr.
   */
  tmalloc(det->is_accepting,boolean,ns+1);
  for (i=1;i<=ns;i++)
    det->is_accepting[i] = FALSE;
  ct = 0;
  for (cstate=1;cstate<=ns;cstate++) {
    cs_ptr = hash_rec(&ht,cstate);
    cs_ptre = hash_rec(&ht,cstate) + hash_rec_len(&ht,cstate) - 1;
    /* See if the set contains an accept-state */
    ptr = cs_ptr-1;
    while (++ptr <= cs_ptre) if (fsaptr->is_accepting[*ptr]) {
      det->is_accepting[cstate] = TRUE;
      ct++;
      break;
    }
  }

  det->num_accepting = ct;
  if (ct==1 || ct != ns) {
    tmalloc(det->accepting,int,ct+1);
    ct = 0;
    for (i=1;i<=ns;i++) if (det->is_accepting[i])
      det->accepting[++ct] = i;
  }
  tfree(fsaptr->is_accepting);
  tfree(det->is_accepting);
  hash_clear(&ht);
  tfree(fsarow);

  if (destroy)
    fsa_clear(fsaptr);

  /* Now read the transition table back in */
  tempfile = fopen(tempfilename,"r");
  compressed_transitions_read(det,tempfile);
  fclose(tempfile);

  unlink(tempfilename);

  return det;
}
