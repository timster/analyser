void
srec_print(
/* Print the set record *srptr. Follows corresponding GAP routine.
 * Currently, rather arbitrarily, identifiers and strings names are
 * printed in dense format, and words and lists of words in sparse format.
 */
	FILE *wfile,
	srec *srptr,
	const char *name,
	int  offset,
	const char *endstring);
void
table_print(
/* Print the table record *tableptr. */
	FILE *wfile,
	table_struc  *tableptr,
	const char *name,
	int  offset,
	const char *endstring,
	int ns, int ne);
void
fsa_print(
	FILE *wfile,
	fsa *fsaptr,
	const char *name);
void
srec_read(
/* Read the set record *srptr from rfile, assigning space as required.
 * If maxsize is larger than srptr->size, and space is allocated for
 * names or labels, then space is allocated for maxsize of these.
 * This allows for possible later expansion.
 */
	FILE *rfile,
	srec *srptr,
	int maxsize);
void
table_read(
/* Read the table_struc *tableptr from rfile, assigning space as required.
 * dr is the number of rows stored densely if storage_type=SPARSE.
 * ns and ne are the sizes of the state-set and alphabet-set.
 * maxstates is the maximum number of states that we allocate space for
 * in dense-storage mode.
 */
	FILE *rfile,
	table_struc *tableptr,
	storage_type table_storage_type,
	int dr,
	int ns, int maxstates, int ne);
void
fsa_read(
/* Read the fsa *fsaptr from rfile, assigning space as required.
 * If maxstates is greater than the actual number of states, then
 * space will be assigned for maxstates states, to allow more states
 * to be added later (this only makes sense with dense storage_type).
 * If assignment is true, an assignment to an fsa is read.
 * The name is returned in *name, which is assumed to have enough space.
 */
	FILE *rfile,
	fsa *fsaptr,
	storage_type table_storage_type,
	int dr,
	int maxstates,
	boolean assignment,
	char *name);
void
compressed_transitions_read(
	fsa *fsaptr,
        FILE *rfile);
