boolean
isdelim(
        int c);
boolean
isvalid(
        int c);
int
read_char(
        FILE * rfile);
void
check_next_char(
        FILE * rfile,
        int c);
void
read_delim(
        FILE * rfile,
        int * delim);
void
read_token(
        FILE * rfile,
	char * token,
        int * delim);
void
skip_gap_expression(
        FILE * rfile,
        int * delim);
void
read_ident(
        FILE * rfile,
        char * ident,
        int * delim,
	boolean inv);
boolean
read_int(
        FILE * rfile,
        int * integ,
        int * delim);
boolean
read_string(
        FILE * rfile,
        char * string,
        int * delim);
void
process_names(
	char **name,
	int num_names);
boolean
read_word(
        FILE * rfile,
        char * gen_word,
        char * end_word,
        int * delim,
	char **name,
	int num_names,
        boolean check);
int
read_word_list(
        FILE * rfile,
        char * wordlist,
        int space,
        int * delim,
	char **name,
	int num_names);
void
printbuffer(
	FILE *wfile);
void
add_to_buffer(
	int n,
	const char *w);
int
add_word_to_buffer(
	FILE *wfile,
	char *word,
	char **symbols);

int
add_iword_to_buffer(
	FILE *wfile,
	int *word,
	char **symbols);
int
int_len(
	int n);
boolean
is_int(
        const char *x);
int
stringlen(
/* We recode this standard function, since the solaris version returns a type
 * other than int, which causes irritating warning messages! 
 */
	  const char *c);
