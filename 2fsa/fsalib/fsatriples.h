fsa *
fsa_triples(
	    fsa *waptr, fsa *diffptr,
	storage_type op_table_type,
	boolean destroy,
	char *tempfilename,
	reduction_equation *eqnptr,
	int maxeqns,
        boolean eqnstop,
	boolean readback);
