fsa *
midfa_determinize(
	fsa *fsaptr,
	storage_type op_table_type,
	boolean destroy,
	char *tempfilename);


fsa *
midfa_determinize_short(
	fsa *fsaptr,
	storage_type op_table_type,
	boolean destroy,
	char *tempfilename);



fsa *
midfa_determinize_int(
	fsa *fsaptr,
	storage_type op_table_type,
	boolean destroy,
	char *tempfilename);


void
midfa_minimize(
	fsa	*fsaptr);

void
midfa_labeled_minimize(
	fsa	*fsaptr);

