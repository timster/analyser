#ifndef PROC_HH
#define PROC_HH

#include <string>
#include <fstream>
#include "dir.hh"
#include "dirtab.hh"
#include "prog.hh"
#include "desc.hh"
#include "oper.hh"
#include "fsalib/fsalib.hh" 
#include "conflict.hh"
#include "closure.hh"
#include "arden.hh"



class Proc {

private:

  DLList<ContSlice*> *contpartitions;
  DLList<StructSlice*> *strucpartitions;
  MapFsa *read, *write;
  ContSlice *valid;
  Desc <StructSlice> readslices;
  Desc <StructSlice> writeslices;
  Desc <StructSlice> accessslices;
  Desc <ContSlice> part;
  ConflictFsa *conflict;
  

public:
  
  Proc(DLList<ContSlice*> *cont, DLList<StructSlice*> *struc, MapFsa *r, MapFsa *w);
  void MakeValid();
  void Analyse();
  void FindConflicts(int i);
  void Davinci(string filename);
  void MakeWaitsFor();

};



#endif












