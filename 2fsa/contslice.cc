#include "oper.hh"

ContSlice::ContSlice() {

  Dir t;
  sizea = t.alphabet.ControlNum();  
}


ContSlice::ContSlice(ContSlice *c): OneFsa(c) {

  ContSlice();
}


string
ContSlice::Name(int d) {

  Dir t;
  
  if (d <= numsym) return t.alphabet.ContName(d);
  if (d == numsym) return "_";

  cerr << "Out of bounds " << d << endl; 
  exit(1);
}
