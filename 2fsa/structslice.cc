#include "oper.hh"



StructSlice::StructSlice() {

  Dir t;
  //not numsym
  sizea = t.alphabet.DirNum();
}


string
StructSlice::Name(int d) {

  Dir t;
  if (d <= numsym) return t.alphabet.DirName(d);
  if (d == numsym) return "_";

  cerr << "Out of bounds " << d << endl; 
  exit(1);
}
