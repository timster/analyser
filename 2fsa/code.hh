#ifndef CODE_HH
#define CODE_HH

#include <fstream>
#include <iostream>
#include "dir.hh"
#include "dirtab.hh"
#include "prog.hh"
#include "desc.hh"
#include "oper.hh"
#include "fsalib/fsalib.hh" 
#include "conflict.hh"
#include "closure.hh"
#include "arden.hh"
#include "proc.hh"

enum language{C,JAVA};


class Code {

  //   int javaparse();   //Parse information from .java file
  //   int cparse();   //Parse information from .c file
  int yyparse ();
  
  ofstream annotate;

  int numdir;

  language Lang(string filename);

  //Iterators 
  void DoToAllIns(void (Code::*insfunc)(Instruction*));
  void HandleBlock(Block *b, void (Code::*insfunc)(Instruction*));
  void HandleStatement(Statement *s, void (Code::*insfunc)(Instruction*));

  void PredDoFunctions();
  void PredDoBlock(Block *b);
  void PredDoStatement(Statement *s);


  //Perform operations on individual instructions
  void AddWords(Instruction *);
  void LookForCalls(Instruction *);
  void MakeAccess(Instruction *);  //Create access information for each statement
  void Print(Instruction *);
  void Prune(Instruction *);
  void MakeAlloc(Instruction *ins); 
  void CheckType(Instruction *ins);
  void InitMustlist(Instruction *ins);
  void AnnotateControlSymbols(Instruction *ins);
  void Annotate();

  
  ContSlice *valid;
  void MakeValidControlWord();
  
  int ParamOf(int func, int n);//Find the nth parameter of func

  //DirFsa *current;   //Current operation for parsing
  //ContSlice *currcontslice;
  DLList<ContSlice*> *contpartitions;
  DLList<StructSlice*> *structpartitions;
  Proc * processes;
  int insnum;   //instruction number for parsing
  int agmstate; //state for parsing agm

  //Globals used when iterating through instructions
  int tableindex; 
  int curparam;
  int **curtable;

public:

  Code(string filename);
  Prog funcs;       //The functions that are to be analysed
  Desc<DirFsa> des;        //The AG description of the structure, plus composites
  //Desc cont;        
  Desc<DirFsa> notequal;   //Disjointness +
  Desc<DirFsa> equal;      //Aliasing       FSAs derived from ASAP description
  ConflictFsa causal;      //Acceptor for causal pairs of control words
  MapFsa *read;        //Maps control word to read set
  MapFsa *write;       //Maps control word to write set
  MapFsa *alloc;       //Maps control words to allocation of memory
  ConflictFsa *conflict;    //Holds conflict information between control words
  
  DLList<Path> words;  //All compositional words required

  

  MapFsa *subs[MAXTAB]; //Array of subs 

  DLList<Must> mustlist;
  DLList<Dir>  nopred;  //A list of statements with no predicates

  //Do we use these ??
  Desc<ConflictFsa> pruned;  //Pruned source information for each read statement
  Desc<ConflictFsa> unpruned; //Unpruned version of above

  Dir d; //For access to dirnames
  NameTab functable;     //Names of functions
  NameTab typenames;     //Names of the different types (Not implemented yet)
  NameTab indvars;       //Names for induction variables

  Cont ControlAdd();  //Add control direction

  void GenWords(); //Create a list of all composed words required
                    //and generates map 
  void Analyse();          //Do full dataflow dependence 
  void Display();          //Show everything

  void MakeConflict();
  void MakePruned();  //Create pruned source information

  void MakeSubs(int v); //Converts Control -> PathName of variable v
  void MakeAllSubs(); //Create the subs for all vars
  void BuildMustlist();

  void Test(); //Run whatever the current test function is
  void OutputSubs();

};



#endif












