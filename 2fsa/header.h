#define BIT unsigned#1
#define TWO unsigned#2
#define THREE unsigned#3
#define FOUR unsigned#4
#define FIVE unsigned#5
#define SIX unsigned#6
#define SEVEN unsigned#7
#define EIGHT unsigned#8
#define UN unsigned
#define recv receive


#define MAX_PAYLOAD_SIZE unsigned#10
#define TRUE 1
#define FALSE 0

#define quart(var,x) var[4*x+3..4*x]
#define oct(var,x) var[8*x+7..8*x]
#define hex(var,x) var[16*x+15..16*x]
#define chunk32(var,x) var[32*x+31..32*x]

void
print (int n){

  putint(stdout, 10, 5, n); putchar(stdout,'\n');
}

void
printun2(UN#2 x){

  putint(stdout, 10, 0, x[1]);putint(stdout, 10, 0, x[0]);
}

void
print128(UN#128 x) {

  int i;
  for[i=0..15] {
    putint(stdout, 16, 0, oct(x,i)); putchar(stdout,' ');
  }
}

void
print136(UN#136 x) {

  int i;
  for[i=0..16] {
    putint(stdout, 16, 0, oct(x,i)); putchar(stdout,' ');
  }
}

void
print32(UN#32 x) {

  int i;
  for[i=0..3] {
    putint(stdout, 16, 0, oct(x,i)); putchar(stdout,' ');
  }
}

void
print64(UN#64 x) {

  int i;
  x = x[0..63];  //Reverse
  for[i=3..0] {
    putint(stdout, 16, 0, hex(x,i)); putchar(stdout,' ');
  }
}

void
printbin64(UN#64 x) {

  int i;
  for[i=63..0] {
    putint(stdout, 10, 0, x[i]);
  }
}

void
print96(UN#96 x) {

  int i;
  for[i=0..11] {
    putint(stdout, 16, 0, oct(x,i)); putchar(stdout,' ');
  }
}
  

