#include "path.hh"

Path::Path() {
  Sep("->");
}

Dir
Path::Head() {
  Pix a=first();
  return (*this)(a);
}


bool
Path::Siblings(Path &p) {

  if (length() != p.length()) return false;
  Pix a=first(), b=p.first();
  for (int i=0; i<length()-1; i++) {
    if ((*this)(a)!=p(b)) return false;
    (*this).next(a) ;p.next(b);
  }
  return true;
}

DirType
Path::Type() {

  Pix a = first();
  DirType current=GEN;
  bool isend=false;
  Dir dir;

  while (a!=end()) {
    dir = (*this)(a);
    current = dir.alphabet.Type(dir.Int());
    if (isend) {
      cerr << "Illegal pathname, direction follows scalar" 
	   << endl;
      exit(1);
    }
    if (current == SCALAR) {isend=true;}
    next(a);
  }
  return current;
}


//If this is a prefix of p (or equal)
bool
Path::PrefixOf(Path *p) {

  Pix a=first(), b=p->first();
  while (a!=end()) {
    if (b!=p->end()) return false;
    if ((*this)(a) != (*p)(b)) return false;
    (*this).next(a) ; p->next(b); //Next symbols...
  }
  return true;
}

bool
Path::ParentOf(Path &p) {

  if (length()+1 != p.length()) {
    //cout << (*this) << " is not the parent of " << p << endl;
    return false;
  }
  Pix a=first(),b=p.first();
  for (int i=0; i<length(); i++) {
    if ((*this)(a)!=p(b)) {
      //cout << (*this) << " is not the parent of " << p << endl;
      return false;
    }
    (*this).next(a) ;p.next(b); //Next symbols...
  }
  //cout << (*this) << " is the parent of " << p << endl;
  return true;
}


ControlWord::ControlWord() {
  Sep("");
}

