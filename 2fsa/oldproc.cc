#include "proc.hh"

Proc::Proc(DLList<ContSlice*> *cont, DLList<StructSlice*> *struc, 
	   MapFsa *r, MapFsa *w) {

  contpartitions = cont;
  strucpartitions = struc;
  MapFsa *readorwrite;

  read = r;
  write = w;
  
  read->Davinci("procread");
  write->Davinci("procwrite");

  readorwrite = new MapFsa();
  readorwrite->Or(read, write);
  readorwrite->Davinci("procreadorwrite");

  //conflict = conf->conflict;
  valid = new ContSlice();
  valid->ExistsFirst(readorwrite);
  valid->Davinci("valid-control");

<<<<<<< proc.cc
=======
 
>>>>>>> 1.12
  cout << "Found " << contpartitions->length() << " code partitions and " 
       << strucpartitions->length() << " structure partitions" << endl;

  Davinci("partitions");
  MakeValid();
  Analyse();
  MakeWaitsFor();
}


//Take the partition descriptions and take the subset of them that is
//valid as according to the valid control word FSA
void
Proc::MakeValid(){

  Pix p = contpartitions->first();
  ContSlice *curr;
  int dir = 1;

  while(p) {

    curr = (*contpartitions)(p);
    curr->And(valid);
    curr->direction = dir++;
    part.AddOp(curr);
    contpartitions->next(p);
  }
  part.Davinci("valid-partitions");
}


void
Proc::Davinci(char *filename) {
  
  Desc<ContSlice> d;

  Pix p = contpartitions->first();
  ContSlice *curr;
  int dir = 1;

  while(p) {

    curr = (*contpartitions)(p);
    curr->direction = dir++;
    d.AddOp(curr);
    contpartitions->next(p);
  }
  d.Davinci(filename);
}


void
Proc::Analyse() {

  Pix p = contpartitions->first();
  ContSlice *curr;
  StructSlice *sread, *swrite, *saccess;
  int reads, writes;
  
  reads = writes = 1;
  
  while (p) {

    curr = (*contpartitions)(p);
    cout << "Partition " << endl;
    curr->PrintSparse();
    sread = new StructSlice();
    sread->Combine(curr,read);
    sread->direction = reads++;
    readslices.AddOp(sread);
    cout << "Read" << endl;
    sread->PrintSparse();
    swrite = new StructSlice();
    curr->Davinci("curr");
    write->Davinci("thiswrite");
    swrite->Combine(curr,write);
    cout << "Write" << endl;
    swrite->direction = writes++;
    writeslices.AddOp(swrite);
    swrite->PrintSparse();
    saccess = new StructSlice();
    saccess->Or(sread, swrite);
    accessslices.AddOp(saccess);
    contpartitions->next(p);
  }
  readslices.Davinci("readslices");
  writeslices.Davinci("writeslices");
  accessslices.Davinci("accessslices");

  //FindConflicts(1);

}

//Find access conflicts between pairs of partitions
//Fairly obsolete now
void
Proc::FindConflicts(int rep) {

  Pix i,j;
  i = contpartitions->first();
  ContSlice *a, *b, *anot, *bnot;
  ConflictFsa *ato, *bto, *comb;
  int from, to;
  char filename[30];

  comb = new ConflictFsa();
  ato = new ConflictFsa();
  bto = new ConflictFsa();
  anot = new ContSlice();
  bnot = new ContSlice();
  
  from = to = 1;

  while (i){
    j = contpartitions->first();
    to = 1;
    while(j){
      if (i!=j) {
	//Find conflicts between i and j
	a = (*contpartitions)(i);
	b = (*contpartitions)(j);

	ato->MakeDouble(a);
	bto->MakeDouble(b);

	sprintf(filename, "%dclash-from-%d-to-%d",rep, from,to);
	//a->Davinci("a");
	//b->Davinci("b");
	//conflict->Davinci("conflict");
	comb->Combine(ato, conflict);
	comb->Combine(comb,bto);
	//comb->Davinci("comb");
	//comb->And(a);
	//comb->Davinci("and");

	if (!comb->Empty()) {
	  
	  comb->Davinci(filename);

	  //Attempt to remove conflicting sections from partitions
	  /*
	  //Remove conflict from partitions
	  anot->ExistsFirst(comb);
	  sprintf(filename, "%dremove-%d-to-%d",rep,from,to);
	  anot->Davinci(filename);
	  //bnot->ExistsSecond(comb);
	  a->AndNot(anot);    //Subtract clash
	  b->Or(anot);        //Add clash
	  */
	}
      }
      contpartitions->next(j);
      to++;
    }
    contpartitions->next(i);
    from++;
  }
  sprintf(filename, "%d-cut",rep);
  Davinci(filename);
}


//Create waits-for information for each pair of partitions
void
Proc::MakeWaitsFor() {

  Pix i,j;
  int to=1, from=1;
  ContSlice *a,*b;
  MapFsa *aread, *awrite, *bread, *bwrite, *combread, *combwrite;
  ConflictFsa *ato, *bto;
  Conflict *conf;
  char filename[30];
  

  aread = new MapFsa();
  awrite = new MapFsa();
  bread = new MapFsa();
  bwrite = new MapFsa();
  combread = new MapFsa();
  combwrite = new MapFsa();
  ato = new ConflictFsa();
  bto = new ConflictFsa();

  i = contpartitions->first();

  //Go through each pair of partitions
  while (i){
    j = i;
    contpartitions->next(j);
    to = from+1;
    while(j){
      if (i!=j) {
	//Find conflicts between i and j
	cout << "Doing pair of threads " << to << ":" << from << endl;

	a = (*contpartitions)(i);
	b = (*contpartitions)(j);

	//Pair is acceptor for all statements in either thread
	ConflictFsa *pair = new ConflictFsa();

	pair->MakePair(a,b);

	//Make read and write MapFsas that map from
	//only words in these partitions
	ato->MakeDouble(a);
	bto->MakeDouble(b);
	aread->Combine(ato,read);
	bread->Combine(bto,read);
	awrite->Combine(ato,write);
	bwrite->Combine(bto,write);
	combread->Or(aread,bread);
	combwrite->Or(awrite,bwrite);

	//combread->Davinci("combread");
	//combwrite->Davinci("combwrite");

	//Create conflict analyser
	conf = new Conflict(combread, combwrite);
<<<<<<< proc.cc
	sprintf(filename, "waits-from-%d-to-%d", to,from);
	conf->WaitsFor->Davinci(filename);
	sprintf(filename, "access-conf-%d-to-%d", to,from);
	conf->AccessConf->Davinci(filename);
	ConflictFsa *access = new ConflictFsa(conf->AccessConf),
	  *waits = new ConflictFsa(conf->WaitsFor); 
=======
>>>>>>> 1.12

<<<<<<< proc.cc

	//(Perhaps produce a more restricted form of the waits for information
	//that looks at dependencies that are killed only by statements in the
	//same thread)

	//Look at pair info 
	//access contains clashes with one from each thread
	access->And(pair); //This means information will appear to have been added by the 
	                   //waits-for process
	access->Davinci("access-pair");
	//waits->And(pair);  //Don't do this because we may be 
	                     //able to wait for a statement in same thread and thus remove synch 
=======
	//Make the waits for information
	ConflictFsa *access = new ConflictFsa(conf->AccessConf),
	  *waits = new ConflictFsa();
>>>>>>> 1.12
	
	access->And(pair); //Only look at the conflicts between threads
	
	if (access->Size()==0) {

	  cout << "No dependencies between partitions " << to 
	       << " and " << from << endl; 
	}
	else {
	  //Wait for last conflicting statement from last thread
	  waits->MakeWaitsFor(access);  

	  sprintf(filename, "waits-from-%d-to-%d", to,from);
	  waits->Davinci(filename);
	  sprintf(filename, "access-conf-%d-to-%d", to,from);
	  access->Davinci(filename);


	  if (waits->IsMonotonic()) {

	    cout << "Waits-for is monotonic, and can be locked with a pair of locks" << endl;
	  }

	  else {
	    cout << "Waits-for is not monotonic" << endl;
	  }

	  //Find the second set of the waits for
	  //This is the servant set
	  //Its maximum is the statement all the second threads must wait for
	  ContSlice *second;

<<<<<<< proc.cc
	if (p1->Empty()){
	  cout << "Waits for information is correct" << endl;
	}
	else{

	  cout << "Over pruning of waiting-for information has occured" << endl;
=======
	  second = new ContSlice();
	  second->ExistsSecond(waits);
	
	  sprintf(filename, "servants-%d-to-%d", to,from);
	  second->Davinci(filename);
>>>>>>> 1.12

<<<<<<< proc.cc
	  //Find which dependencies are missing
	  ConflictFsa *missingdeps = new ConflictFsa(), *any = new ConflictFsa();
	   
	  any->MakeAll();
	  missingdeps->MakeDouble(p1);
	  missingdeps->Combine(missingdeps, any);
	  missingdeps->And(access);
	  missingdeps->Davinci("missingdeps");
	  //missingdeps->PruneInfiniteMaps();
	  //missingdeps->Davinci("missing-pruned");
	}
=======
	  //New check for overpruning
	  //p1 is the set of x s.t (x,y) is in access and y is in A
	  //p1 is the set of things that must wait (according to the conflict)
	  //p2 is the set of u s.t. there exists a v s.t (u,v) is in WaitsFor
	  //p2 is the set of computed waiters
	  ContSlice *p1,*p2, *p3;
	  //ConflictFsa *acc;
	  p1 = new ContSlice();
	  p2 = new ContSlice();
	
	  p1->ExistsFirst(access);
>>>>>>> 1.12

<<<<<<< proc.cc
	if (!p2->Empty()){
	  cout << "*Under* pruning of waiting-for information has occured" << endl;
	}
=======
	  p1->Davinci("clashes-from");
>>>>>>> 1.12

<<<<<<< proc.cc
	sprintf(filename, "pair-conf-%d-to-%d", to, from);
	access->Davinci(filename);
	ContSlice *first=new ContSlice();
	first->ExistsFirst(access);
	
	ConflictFsa *assert = new ConflictFsa(conf->AccessConf);
=======
	  p2->ExistsFirst(waits);
>>>>>>> 1.12

<<<<<<< proc.cc
	assert->AndNot(access);
=======
	  sprintf(filename, "waiters-%d-to-%d", to,from);
>>>>>>> 1.12
	
<<<<<<< proc.cc
	assert->Davinci("assert");

	sprintf(filename, "pair-waiters-%d-to-%d", to, from);
	first->Davinci(filename);
=======
	  p2->Davinci(filename);
>>>>>>> 1.12

<<<<<<< proc.cc
	waits->And(pair);
	waits->Davinci("waits-pair");
=======
	  p3 = new ContSlice(p1);
>>>>>>> 1.12

<<<<<<< proc.cc
=======
	//Build check
	//p1 is now the ones that are missed
	//p2 is the extra ones (if there are any)
	  p1->AndNot(p2);
	  p2->AndNot(p3);

	  if (p1->Empty()){
	    cout << "Waits for information is correct" << endl;
	  }
	  else{

	    cout << "Over pruning of waiting-for information has occured" << endl;

	    //Find which dependencies are missing
	    ConflictFsa *missingdeps = new ConflictFsa(),
	      *any = new ConflictFsa();
	  
	    any->MakeAll();
	    missingdeps->MakeDouble(p1);
	    missingdeps->Combine(missingdeps, any);
	    missingdeps->And(access);
	    missingdeps->Davinci("missingdeps");
	    //missingdeps->PruneInfiniteMaps();
	    //missingdeps->Davinci("missing-pruned");
	  }

	  if (!p2->Empty()){
	    cout << "*Under* pruning of waiting-for information has occured" << endl;
	  }
	}
>>>>>>> 1.12
	delete conf;
      }
      contpartitions->next(j);
      to++;
    }
    contpartitions->next(i);
    from++;
  }
}


























