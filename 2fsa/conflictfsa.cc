#include "oper.hh"

ConflictFsa::ConflictFsa() {

  Dir t;
  numcont = sizea = sizeb = t.alphabet.ControlNum();
}

ConflictFsa::ConflictFsa(ConflictFsa *c): TwoFsa(c) {

  ConflictFsa();
}

void
ConflictFsa::MakeCausal(){

  mult->flags[DFA] = true;
  //Make states, initial and accepting
  mult->states->size = 2;
  mult->num_initial = 1;
  tmalloc(mult->initial, int, 2);
  mult->initial[1] = 1;
  mult->num_accepting = 1;
  tmalloc(mult->accepting, int, 2);
  mult->accepting[1] = 2;

  //Make alphabet
  MakeAlphabet();

  //Make table
  AllocateTransTable();
  //Handle double empty symbol seperately
  //Fix transitions from state 1
  for (int x = 1; x <= numcont+1 ; x++) {
    for (int y = 1; y <= numcont+1 ; y++) {
      if ((x == numcont+1) && (y==numcont+1)) continue;
      if ((x!=numcont+1) && (y!=numcont+1)) {
	if (x > y) {
	  AddTrans(1,2,x,y);
	}
	if (x < y) {
	  AddTrans(1,0,x,y);
	}
	if (x == y) {
	  AddTrans(1,1,x,y);
	}
      }
      else {
	if ((x==numcont+1) && (y!=numcont+1)){
	  AddTrans(1,0,x,y);
	}
	else if ((x!=numcont+1) && (y==numcont+1)) {
	  AddTrans(1,2,x,y);
	}
      }
    }
  }    
  //Fix transitions from state 2
  //Make all transitions back to state 2
  for (int x = 1; x <= numcont+1 ; x++) {
    for (int y = 1; y <= numcont+1 ; y++) {
      if ((x == numcont+1) && (y==numcont+1)) continue;
      AddTrans(2,2,x,y);
    }
  }
}

void
ConflictFsa::MakeBottom(){

  mult->flags[DFA] = true;
  //Make states, initial and accepting
  mult->states->size = 1;
  mult->num_initial = 1;
  tmalloc(mult->initial, int, 2);
  mult->initial[1] = 1;
  mult->num_accepting = 1;
  tmalloc(mult->accepting, int, 2);
  mult->accepting[1] = 1;

  //Make alphabet
  MakeAlphabet();

  //Make table
  AllocateTransTable();

  //Fix transitions from state 1:  (X,_) -> 1
  for (int x = 1; x <= numcont+1 ; x++) {
    if (x==numcont+1) continue;
    AddTrans(1,1,x,numcont+1);
  }
}

void
ConflictFsa::MakeWaitsFor(ConflictFsa *Accessconf) {

  ConflictFsa *causal, *comb;

  cout << "Starting Waits for..." << endl;
  cout << "Accessconf has " 
       << Accessconf->Size() << " states" << endl;
  
  if (Accessconf->Size()==0) {
    cout << "Zero sized accessconf" << endl;
    Copy(Accessconf);
    return;
  }

  causal = new ConflictFsa();
  comb = new ConflictFsa();
  causal->MakeCausal();

  Copy(Accessconf);

  comb->Combine(Accessconf, causal);
  AndNot(comb);
  //Davinci("waitsfor");

  delete causal;
  delete comb;
  
}


string
ConflictFsa::Name1(int d) {

  Dir t;
  if (d==sizea+1) d = numsym+1;
  return t.alphabet.ContName(d);
}

string
ConflictFsa::Name2(int d) {
  
  if (d==sizeb+1) d = numsym+1;
  return Name1(d);
}

void 
ConflictFsa::MakeNext(ContSlice *c){

  ConflictFsa *doub=new ConflictFsa(), *after=new ConflictFsa(),
    *andlater=new ConflictFsa(), *fdoubledash=new ConflictFsa();

  doub->MakeDouble(c);
  after->MakeCausal(); 
  after->Invert();
  andlater->Combine(after,doub);
  Combine(doub,andlater);
  fdoubledash->Combine(this,after);
  AndNot(fdoubledash);
  delete doub; delete after; delete andlater; delete fdoubledash;
}


void
ConflictFsa::BackDecomp(MapFsa *a, MapFsa *b){

  Dir t;
  MapFsa inv(b);
  
  inv.InvertTFSA();
  CombineTFSA(a,&inv);
  numcont = sizea = sizeb = t.alphabet.ControlNum();
}


void
ConflictFsa::PruneInfiniteMaps() {

  Find_ATransition();
  Tidy();
  cout << "Pruned infinite maps" << endl;
}


void
ConflictFsa::Find_ATransition() {

  int trans,a,y, numstates;

  numstates = mult->states->size;

  for (a=1; a<=numstates;a++) { 
      for (y=1; y<sizeb+1; y++) {
	trans = Trans(a,sizea+1,y);
	if (trans!=0) {
	  //cout << "Found _A transition from state " << a << " to state " << trans <<endl;
	  LookForLoops(a);
	}
      }
  }
}

void
ConflictFsa::LookForLoops(int state) {


  int numstates = mult->states->size;


  tmalloc(flag, int, numstates+1);
  for(int i=0; i<=numstates; i++) flag[i]=0;

  SubLookForLoops(state);
  tfree(flag);
}

void
ConflictFsa::SubLookForLoops(int state){

  int y, trans;

  if (flag[state]) {

    //cout << "State " << state << " is part of a loop" << endl;
    RemoveLoop(state);
    return;
  }

  flag[state]=1;

  for (y=1; y<sizeb+1; y++) {
    trans = Trans(state,sizea+1,y);
    if(trans) SubLookForLoops(trans);
  }
}


void
ConflictFsa::RemoveLoop(int state){

  int y, trans;
  AddAccepting(state);   //Make the state accepting
  for (y=1; y<sizeb+1; y++) {
    trans = Trans(state,sizea+1,y);
    if(trans) {
      AddTrans(state, 0, sizea+1, y);  //Remove transition within loop
      //cout << "Removing transition " << state << "->" << trans << ":" << Name2(y) << endl;
      RemoveLoop(trans);
    }
  }
}


// Make acceptor for a pair of statements from different threads
// Accept (x,y) if x is in a and y is in b
void
ConflictFsa::MakePair(ContSlice *a, ContSlice *b){

  ConflictFsa *B,*A, *any;
  
  any = new ConflictFsa();
  any->MakeAll();
  
  B = new ConflictFsa();
  B->MakeDouble(b);
  B->Combine(B,any);
  
  A = new ConflictFsa();
  A->MakeDouble(a);
  A->Combine(A,any);
  A->Invert();

  Combine(B,A);
  Davinci("pair");

  delete any;  delete B; delete A;
}


bool
ConflictFsa::IsMonotonic() {

  ConflictFsa *a,*b,*c,*d, *causal;
  bool is;

  a = new ConflictFsa(); b = new ConflictFsa(); c = new ConflictFsa();
  d = new ConflictFsa(); 

  ConflictFsa *oinv = new ConflictFsa(this);

  causal = new ConflictFsa();
  causal->MakeCausal();

  oinv->Invert();

  a->Combine(this,causal);
  b->Combine(a,oinv);
  c->Combine(b,causal);

  c->Davinci("monotonic1");


  d->MakeIdentity();

  d->Davinci("identity");
  c->And(d);

  c->Davinci("monotonic2");

  if (c->Empty()) is = true;
  else is = false;

  delete a;
  delete b;
  delete c;
  delete d;
  delete causal;
  delete oinv;

  return is;

}













