#ifndef DESC_HH
#define DESC_HH

#include <iostream>
#include "stdio.h"
#include <string>
#include "DLList.hh"
#include "twofsa.hh"

//Stores a list of TwoFsas and allows manipulation of them
template <class T>
class BaseDesc{
  
  DLList<T *> operations;

public:

  BaseDesc(): operations() {
    operations.Sep("\\\\\n");
  }


  void AddOp(T *o){

    operations.append(o);
  }

  void Combine(){

    Pix a=operations.first(), b=operations.first();

    while (a!=0) {
      b = a;
      operations.next(b); //Start from next position
      while (b!=0) {
	if (operations(a)->direction == operations(b)->direction) {
	  //Merge and remove 
	  cout << "Found two operations with same name" << endl;
	  operations(a)->Or(operations(b));
	  operations.del(b,1);
	}
	else {operations.next(b);}
      }
      operations.next(a);
    }
  }
  void Enum(int lev){

    Pix o = operations.first();
    while (o != NULL) { 
      operations(o)->Enum(lev);
      operations.next(o);
    }
  }

  void PrintSparse(){

    Pix o = operations.first();
    while (o != NULL) { 
      operations(o)->PrintSparse();
      operations.next(o);
    }
  }
  //void Prune();
  void DavinciLink(const char* filename){

    ofstream file;
    file.open(adddav(filename));

    Pix o = operations.first();
    file << "[" << endl;
    while (o != operations.end()) { 
      if (operations(o)->direction.Type()==LINK) operations(o)->Davinci(file);
      operations.next(o); 
      //file << "," << endl;
    }
    file << "]" << endl;;
    file.close();
  }

  void Davinci(string filename){

    ofstream file;
    file.open(adddav(filename));

    Pix o = operations.first();
    file << "[" << endl;
    while (o != operations.end()) { 
      operations(o)->Davinci(file);
      operations.next(o); 
      //file << "," << endl;
    }
    file << "]" << endl;;
    file.close();
  }

  T * NewFindOp(Path f) {

    Pix o = operations.first();
    //cout << "Looking for op " << f << endl;
    while (o != operations.end()) { 
      //cout << "Is it " << operations(o).direction << " ? " << endl;
      if (operations(o)->direction == f) {
	return operations(o); 
      }
      operations.next(o);
    }

    cerr << "Error missing operation " << f << endl;
    return NULL;
  }
  T * NewFindOp(Dir d){

    //Create temp format for comparison
    Path f;
    f.append(d);
    return NewFindOp(f);
  }
  bool HaveOp(Path f) {

    Pix o = operations.first();
    //cout << "Looking for op " << f << endl;
    while (o != operations.end()) { 
      if (operations(o)->direction == f) {
	return true; 
      }
      operations.next(o);
    }
    return false;
  }

  void AndWith(T *f){
  
    Pix d=operations.first();
    while (d!=0) {
      //cout << "Causal\n";
      //operations(d).Davinci(cout, f);
      //cout << "Before Causal\n" << operations(d);
      operations(d)->And(f);
      //cout << "After Causal\n" << operations(d);
      operations.next(d);
    }
  }
 
  void ExpandAlphabet(){
    T* curr;
    Pix d=operations.first();
    while (d!=operations.end()) {
      curr = operations(d);
      curr->ExpandAlphabet();
      operations.next(d);
    }
  }

  void MakeJoinCode(bool leaf, string filename) {

    ofstream out;
    Pix d=operations.first();


    out.open(filename);
    
    out << "//Join code: generated automatically" << endl;
    out << "// by 2FSA analyser" << endl  << endl;


    while(d != operations.end()){

      if (operations(d)->direction.Type()==LINK) {
	operations(d)->MakeJoinCode(leaf, out);
      }
      operations.next(d);
    }

    d=operations.first();

    out << "void join(Tree *root) {" << endl;

    while(d != operations.end()){

      if (operations(d)->direction.Type()==LINK) {

	out << "join_" << operations(d)->direction 
	    << "_1(root,root);" << endl; 
      }
      operations.next(d);
    }

    out << "}" << endl << endl;
    out.close();
  }

  // Ouput the C code to do general structure stuff
  void MakeStructCode(string filename) {

    ofstream out;
    out.open(filename);

    out << "//Struct code: generated automatically" << endl;
    out << "// by 2FSA analyser" << endl  << endl;

    out << " //Declare the struct" << endl;
    out << "struct tree {" << endl;

    Pix d=operations.first(); 

    while(d!=NULL){

      if (operations(d)->direction.Type()!=SCALAR) {

	out << "  struct tree *" << operations(d)->direction << ";" << endl;
      }
      else {  // Assume all scalars are ints since we lose type info
	
	out << " int " << operations(d)->direction << ";" << endl;
      }
      operations.next(d);
    }

    out << "typedef struct tree Tree;" << endl;

    // NB not doing the SUIF macro version of this function

    out << "void TreeCons(Tree *t) {" << endl;

    d=operations.first(); 

    while(d!=NULL){

      if (operations(d)->direction.Type()!=SCALAR) {

	out << "  t->" << operations(d)->direction << " = NULL;" << endl;
      }
      else {  // Assume all scalars are ints since we lose type info
	
	out << " t->" << operations(d)->direction << " = -1;" << endl;
      }
      operations.next(d);
    }

    out << "#include \"join.c\"" << endl; 


    // Do we bother doing this since it is specific to the structure??
    out << "int checksum (Tree *t) {" << endl;
    out << "int " << endl;

    // Give up at this point, but perhaps resurrect this idea later??
  }

  //Output C code that traverses the structure and
  //produces daVinci output 
  
  void MakeDavinciCode(string filename){


   string colours[10] = {"lightblue","green","yellow","red","orange",
		    "blue","pink","brown","darkgreen","grey"};
  
   //Pix d;

    ofstream out;
/*
	colours[0]="lightblue";
	colours[1]="green";
	colours[2]="yellow";
	colours[3]="red";
	colours[4]="orange";
	colours[5]="blue";
	colours[6]="pink";
	colours[7]="brown";
	colours[8]="darkgreen";
	colours[9]="grey";
*/

    out.open(filename);
    
    //Output generic bit

    out << "//Output C code that traverses the structure and" << endl;
    out << "//produces daVinci output" << endl << endl;

    out << "void davincinode(FILE *f, Tree *node);" << endl;
    out << "void outputchildedge(FILE *f, Tree *node){" << endl;
    out << "fprintf(f,\"e(\\\"\\\",[],\");" << endl;
    out << "davincinode(f, node);" << endl;
    out << "fprintf(f,\"),\");" << endl;
    out << "}" << endl;
    out << "void outputlinkedge(FILE *f, Tree *node, char * colour){" << endl;
    out << "fprintf(f,\"e(\\\"\\\",[a(\\\"EDGECOLOR\\\",\\\"%s\\\")],r(\\\"%x\\\")),\", colour, node);" << endl;
    out << "}" << endl;
    out << "void davinci(char *filename, Tree *root){" << endl;
    out << "FILE  *file;" << endl;
    out << "file = fopen(filename, \"w\");" << endl;
    out << "fprintf(file, \"[\\n\");" << endl;
    out << "davincinode(file, root);" << endl;
    out << "fprintf(file, \"]\\n\");" << endl;
    out << "fclose(file);" << endl;
    out << "}" << endl;
    
    //Output davincinode code
    
    out << "void davincinode(FILE *f, Tree *node) {" << endl;
    out << "if (node==NULL) return;" << endl;
    out << "//Output node header" << endl;
    out << "fprintf(f,\"l(\\\"%x\\\",n(\\\"%x\\\",[a(\\\"OBJECT\\\",\\\"%x\\\")],[\\n\", node,node, node);" << endl;
    
    //Output edges to child nodes
    for(Pix d=operations.first() ; d!=operations.end() ; operations.next(d)){  
      if (operations(d)->direction.Type()==GEN) {
	out << "if (node->" 
	    << operations(d)->direction
	    << ") outputchildedge(f,node->"
	    << operations(d)->direction
	    << ");" << endl;
      }
    }


    //Output edge to links
    int col = 0;
    for(Pix d=operations.first() ; d!=operations.end() ; operations.next(d)){  
      if (operations(d)->direction.Type()==LINK) {
	out << "if (node->" 
	    << operations(d)->direction
	    << ") outputlinkedge(f,node->"
	    << operations(d)->direction
	    << ", \"" << colours[col++] <<  "\");" << endl;
      }
    }
    

    //Output node footer
    out << "fprintf(f,\"]))\\n\");" << endl;
    out << "}" << endl;

    out.close();
  }


  //void MakeSources();
  //void RemoveDuplicates();
  //template <class T> friend ostream& operator << (ostream&, Desc<T>& d);
};



template <class T>
class Desc : public BaseDesc<T> {


public:

  //Creates a composite operation described by the given format 
  //And appends it to our descriptive list, for later
  void AddComp(Path f){

    T  *a, *curr;

    Pix d = f.first();

    cout << "Working on r/w word " << f << endl;
    
    if (!f.empty()) {
      
      //Get the first operation 
      curr = new T (this->NewFindOp(f(d)));
      f.next(d);
    
      //cout << "Made a start" << endl; 
      //Go through each element of f
      while(d != f.end()) {
	//cout << "Inside loop" << endl;
	//Find the appropriate operation
	a = this->NewFindOp(f(d));
	//cout << "Composing direction " << endl << curr << endl;
	//cout << "With fsa " << endl << a << endl;
	curr->Combine(curr,a);
	f.next(d);
      }
      curr->SetDir(f);
    }
    //Append our newop to existing list
    else {  //Path is empty, need to create identity fsa
      curr = new T ();
      curr->MakeIdentity();
    } 
    //curr->Davinci("current");
    this->AddOp(curr);
  }

  void AddCompList(DLList<Path> l) {
  
    Pix d=l.first();

    while (d!=l.end()) {
      //****If it is not already in the list, add it**************
      if (!this->HaveOp(l(d))) {this->AddComp(l(d));}
      l.next(d);
    }
  }

  

};

#endif




