#include "code.hh"

#define YY_NO_UNPUT         //Avoid `unput not used' warning

//Add the parser function
//Make parser local to this function
// #define yyparse Code::javaparse
// #include "javaparser.c"
// #define yyparse Code::cparse
// #include "cparser.c"

#define yyparse Code::yyparse

int lineno;
int startline;
string file;

#include "parser.c"
#include "lexer.c"

Code::Code(string filename) : funcs(), des(), causal(){

  FILE *in ;
  file = filename;
  contpartitions = new DLList<ContSlice*>;
  structpartitions = new DLList<StructSlice*>;

  in = fopen(filename.c_str(),"r"); 
  if (!in) {
    cerr << "No such file \""  << filename << "\"" << endl;
    exit(1);
  }
  yyin = in;
  //language l;
  //yydebug = 1;    //Turn on bison debugging
  agmstate = 1;
  insnum = 1;
  cout << "Parsing from file " << filename << endl;

  //Parse user code from file
  //If .c cparse, if .java javaparse
  //   l = Lang(filename);
  //   if (l==C) {yyparse();}
  //   if (l==JAVA) {
  //       cout << "Java support not yet..." << endl;
  //       //javaparse();
  //   }
  
  lineno=1;
  startline=1;
  yyparse();

  cout << "Parse done" << endl;

  if (options.Annotate) Annotate();

  cout << "Code read in" << endl;
  cout << d.alphabet << endl;
  cout << "Induction Variables: " << indvars << endl;
  cout << "Functions: " << functable << endl;
  
  mustlist.Sep("\n");
  Analyse();
}

language
Code::Lang(string filename) {

  /*
  while(*filename!='.') {filename++;}

  while(*filename) {

    if (*filename == 'c') return C;
    if (*filename == 'j') return JAVA;
    filename++;
  }
  //Guess C as default
  return C;
  */

  if (filename.find(".java")!=string::npos){
    return JAVA;
  }
  //Assume C as default
  return C;
}

Cont
Code::ControlAdd() {

  char c[2];
  //cout << "Statenum " << insnum << endl;
  c[0] = insnum + 'A' - 1; c[1]=0;
  d.alphabet.ControlAdd(c);
  insnum++;
  return (Cont)(d.alphabet.ContLookup(c));
}

void
Code::Display() {

  //cout << funcs << endl;
  //cout << des << endl;
}


void
Code::Analyse() {

  /*
  //Build equal and not-equal fsas
  equal.Combine();
  notequal.Combine();
  //Output equal and not equal
  equal.Davinci("equal");
  notequal.Davinci("notequal");
  */

  //Convert equal not-equal to control words 
  
  //cout << "Directions read in are" << endl << d.dirnames << endl;
  //cout << "Program read in is" << endl << funcs << endl;

  /*
  //Generate predicate information
  PredDoFunctions();
  BuildMustlist();
  */
  cout << "Code read in\n==========" << endl;
  DoToAllIns(&Code::Print);  //Display all the code
  cout << "==========" << endl;
  DoToAllIns(&Code::CheckType); //Do simple type check of program
  cout << "Type checked" << endl;
  
  //Keep an eye on this alphabet expansion stuff: Seems to work
  des.ExpandAlphabet();   
  cout << "Expanded Alphabet" << endl;

  //Output description
  des.DavinciLink("description");
  cout << "Description output" << endl;

  if (options.JoinCode) des.MakeJoinCode(false, "join.c");
  if (options.DavCode) des.MakeDavinciCode("dav.c");

  DirFsa *remove=new DirFsa();

  remove->MakeRemoveEps();
  cout << "RemoveEps made" << endl;
  
  //Generate all composite words
  GenWords();
  cout << "Composites generated" << endl;

  //Create the composite operations
  des.AddCompList(words);

  des.Davinci("composite");
  //cout << des << endl;
  cout << "Made composite directions" << endl; 
  MakeAllSubs();
  cout << "Made substitutions for induction variables" << endl;
  OutputSubs();
  cout << "Outputted substitution map" << endl;
  MakeValidControlWord();
  cout << "Valid Control Word made" << endl;


  MakeConflict();
  //causal.MakeCausal();
  //cout << "Made causal FSA" << endl;

  MapFsa *acc=new MapFsa(read);
  ContSlice *val = new ContSlice();

  acc->Or(write);
  val->ExistsFirst(acc);
  val->Davinci("full-valid-controlword");

  //Build conflict graphs
  //Get this section working
  /*
  Conflict *conf;

  if (options.GlobalConflict) {
    conf = new Conflict (read,write); 
  }
  else {
    processes = new Proc(contpartitions, structpartitions, read, write);
  }
  */

  //conf->BuildDepGraph();
  //conf->BuildCilk();

  cout << "Finished Analysis" << endl;
}

void
Code::MakeConflict() {

  ContSlice *allocated=new ContSlice();
  read = new MapFsa();
  write = new MapFsa();
  alloc = new MapFsa();

  read->MakeNone();
  write->MakeNone();
  alloc->MakeNone();

  //Create read, write and alloc fsas
  DoToAllIns(&Code::MakeAccess);
  DoToAllIns(&Code::MakeAlloc);

  read->Davinci("read");
  write->Davinci("write");
  alloc->Davinci("alloc");

  cout << "Made Access FSAs" << endl;

  //Works up to here

  allocated->ExistsSecond(alloc);
  allocated->Davinci("allocated");

  cout << "Made allocated" << endl;

  conflict = new ConflictFsa();
  conflict->BackDecomp(read,write);

  cout << "Made conflict" << endl;

  conflict->Davinci("conflict");
  conflict->PrintSparse();
  //conflict.Not();
  //conflict.TrimDirTran();
  //conflict.Davinci("disjoint");

  cout << "Made Conflict FSAs" << endl;
}


//Problems with empty words, eg v = ..., rather than v->data = ...
//Needed for globals
void
Code::MakeAccess(Instruction *ins) {

  MapFsa   *wtemp, *rtemp, *curr;// *global;
  DirFsa  *woper,*roper;

  //cout << "Creating new MapFsas" << endl;
  ins->def = new MapFsa();
  ins->use = new MapFsa();
 
  //Do only if it is a read/write instruction
  if (ins->type != INSRW) return;

  cout << "Handling instruction: " << ins->statdir
       << " " << ins->writevar << endl;
  //For the write section, the def fsa
  //Take the subs of the appropriate ind var and append (statdir, word);
  if (ins->writevar>0) {  //It is an induction variable
    wtemp = new MapFsa(subs[ins->writevar]);  //Copy
  }
  else { //It is a global variable
    cout << "Globals not handled properly yet" << endl;
    exit(1);
    /* global = new MapFsa();
    global->MakeGlobal(-ins->writevar);
    wtemp.Combine(&valid,&global);  //This is wrong, sort out
    wtemp.Davinci("global");
    */
  }
  wtemp->Davinci("w1temp");
  wtemp->SetFirstRW(ins->statdir.Int());
  cout << "Write direction " << *(ins->write) << endl;
  wtemp->Davinci("wtemp");
  
  if (!ins->write->empty()) {
    woper = des.NewFindOp(*(ins->write));
    woper->Davinci("woper");
    //woper->Davinci("woper");
    //wtemp.Davinci("wtemp3");
    ins->def->Combine(wtemp, woper); 
    ins->def->Davinci("insdef");
  }
  else {
    ins->def = new MapFsa(wtemp);
  }
  delete wtemp;

  //ins->def.Davinci("write-acc");

  //For the read section, go through all the elements
  Pix x = ins->elements->first();
  Path word;
  int var;
  ins->use->MakeNone(); //Initialise use
  while (x!=ins->elements->end()) {
    //Do as for the write section
    word = *((*(ins->elements))(x)->word);
    var = (*(ins->elements))(x)->ind;
    if (var>0) { //It is an induction variable
    rtemp = new MapFsa(subs[var]);
    rtemp->Davinci("rtemp");
    }
    else { //It is a global variable
      cout << "Globals not handled properly yet" << endl;
      /*
      global = new MapFsa();
      rtemp = new MapFsa();
      global->MakeGlobal(-var);
      global->Davinci("global");
      ConflictFsa *val;
      val = new ConflictFsa();
      val->MakeDouble(valid);
      rtemp->Combine(val,global); 
      */
    }
    rtemp->SetFirstRW(ins->statdir.Int());    
    if (!word.empty()){
      roper = des.NewFindOp(word);
      roper->Davinci("roper");
      curr = new MapFsa();
      curr->Combine(rtemp, roper); 
      curr->Davinci("curr");
    }
    else {
      curr = new MapFsa(rtemp);
    }
    ins->use->Davinci("insuse");
    //Or together all these fsas
    ins->use->Or(curr); //Different alphabets
    delete curr;
    ins->elements->next(x);
  }
  read->Or(ins->use);
  write->Or(ins->def);
}


//Build the Allocation fsa
// ********************* This needs updating for globals
void
Code::MakeAlloc(Instruction *ins) {

  DirFsa *woper;
  MapFsa wtemp;

  //Do only if it is a new allocation instruction
  if (ins->type != INSNEW) return;

  if (ins->writevar<0) { 
    cerr << "Should not allocate to global" << endl;
    exit(1);
  }

  //For the write section, the def fsa
  //Take the subs of the appropriate ind var and append (statdir, word);
  wtemp = subs[ins->writevar];
  wtemp.SetFirstRW(ins->statdir.Int());
  woper = new DirFsa(*(des.NewFindOp(*(ins->write))));
  ins->def->Combine(&wtemp, woper);
  alloc->Or(ins->def);
}


void
Code::MakeValidControlWord(){

  MapFsa *valcontwordmap;

  valcontwordmap = new MapFsa();
  valid = new ContSlice();
  valcontwordmap->MakeNone(); 
  for (int i=1; i<= indvars.Size();i++){
    valcontwordmap->Or(subs[i]); 
  }
  
  valcontwordmap->Davinci("valcontwordmap");
  valid->ExistsFirst(valcontwordmap);
  valid->Davinci("valid");
  delete valcontwordmap;
}



//Make FSA that maps control word to path name
//Just does the bit for all call symbols
//NB does not do Call(w) or Call(w.NonGenerator), both would require
//much more complex non-deterministic methods
//If fact for some NonGenerator FSAs the appropriate thing is not 
//represented by a regular language over two variables.
//What can be done about this ??
//1) Change everything to a more sophisticated abstract machine. 
//2) Approximate using a regular FSA.
//3) Ignore recursions that go in non-generator directions.
//We take position 3 at the moment.


//This is new, multiple parameters version
//Need to append the words for each read/write statement later
void
Code::MakeAllSubs() {

  Arden *ard;
  for (int i=1; i <= indvars.Size(); i++) {
    MakeSubs(i);
    cout << "Made subs " << i << endl;

    //Remove epsilon transitions if necessary
    subs[i]->Davinci("last-subs");
    subs[i]->RemoveAllEps();
    subs[i]->Davinci("last-subs-cleaned");
    //subs[i]->PrintSparse();

    
    // Arden approximation
    if (subs[i]->HasNonGenTrans()) {
      ard = new Arden(subs[i], &des);
      subs[i] = ard->solution;
      subs[i]->SetDir((Dir)i);
      //Use ard to return new subs map

      subs[i]->Davinci("arden");

      delete ard;
    }
  }
}



void
Code::MakeSubs(int var) {

  fsa *f;
  fsa *newfsa;

  //cout << "Making subs for " << var << endl;

  subs[var] = new MapFsa();

  tmalloc(f,fsa,1);
  tmalloc(newfsa,fsa,1);
  fsa_init(f);  
  subs[var]->MakeAlphabet(f);  //Create alphabet for f


  numdir=d.alphabet.Size();
  int alphsize = (numdir+1)*(numdir+1);
  int size=indvars.Size()+1, maxtrans = alphsize;

  //Create a state for each induction variable, plus an initial state

  f->flags[NFA]=true;     //Non deterministic
  f->states->size = size;
  f->states->type = SIMPLE;
  f->num_initial = 1;
  tmalloc(f->initial, int, 2);
  f->initial[1] = 1; 
  f->table->denserows=0;

  f->num_accepting = 1; //Make just the var state accepting
  tmalloc(f->accepting, int, 2);
  f->accepting[1] = var+1;

  //Make table
  // table_data_ptr[i] is an array of transitions from state i
  //Stored as pairs, (generator, target).
  //For epsilon transistions the generator equals 0 
  f->table->maxstates = size;
  f->table->table_type = SPARSE;

  tmalloc(curtable, int*, size+2); 
  tmalloc(curtable[0], int, 2*size*(maxtrans)); //Conservative
  
  curtable[1]=curtable[0];

  tableindex=0;   //Second index of the table
  int parameter;
  Function *first = (*(funcs.flist))(funcs.flist->first());

  //For first start state, we create epsilon transitions for all parameters
  //the 'main' function parameters. 
  Pix x=first->paramlist->first();
  
  while (x!=first->paramlist->end()) {
    parameter = (*(first->paramlist))(x);
    if (parameter!=0){ //Ignore scalar parameters
      //Add transition
      //cout << "Adding epsilon transition from state one to state " << parameter+1 << endl;
      curtable[1][tableindex++] = 0;
      curtable[1][tableindex++] = parameter+1;
    }
    first->paramlist->next(x);
  }
  curtable[2] = &curtable[1][tableindex]; //Link up the start of the next array
  tableindex = 0;    //Reset 
 
  //For each functions we take each ind variable a in the
  //paramlist and look for all, call statements of the form
  // A:  Func(a.l, b.r, ...)
  //And if c is the relevant parameter of Func(), we add the
  //transition (A,l)->c

  Pix fun=funcs.flist->first();
  Function *curfun;

  while (fun!=funcs.flist->end()) {
    curfun = (*(funcs.flist))(fun);  //Set current function
    Pix p = curfun->paramlist->first();
    while (p!=curfun->paramlist->end()) {   //Go through all parameters
      curparam = (*curfun->paramlist)(p);
      if (curparam!=0){
	//Recurse through the main block of the function
	HandleBlock(curfun->body, &Code::LookForCalls);
	//Skip to next state/parameter
	curtable[curparam+2] = &curtable[curparam+1][tableindex];
	tableindex = 0; //Reset tableindex
      }
      curfun->paramlist->next(p);
    }
    //Skip to next function
    funcs.flist->next(fun);
  }
  f->table->table_data_ptr = curtable; //Point back to new table

  //FILE *out;
  //out = fopen("nfa.out", "w");
  //fsa_print(out,f,"subs");
  //fclose(out);

  //Print nondet fsa
  //cout << "Substitution fsa " << var << endl;
  //subs[var]->PrintSparse(f);
  
  //Determinize the nfa
  newfsa = fsa_determinize(f,DENSE,FALSE,"temp");
  fsa_minimize(newfsa);
  //tfree(curtable);
  //fsa_clear(f);
  subs[var]->SetDir((Dir)var);
  subs[var]->mult = newfsa;
  subs[var]->Davinci("last-map-substitution");
}


const int debugLookForCalls = 0;


void
Code::LookForCalls(Instruction *i) {

  int paramnum = 1,target, symbol;
  int param;
  Path *word;
  Dir t;
  int epsilon = t.alphabet.Epsilon();

  if (i->type == INSCALL) {
    //Go through all parameters of the call
    Pix e = i->elements->first(); 
    while (e != i->elements->end()) {
      param = (*(i->elements))(e)->ind;
      //cout << "curparam " << curparam << "param " << param << endl;
      //Is this parameter the same as the current one?
      if (param==curparam) {
        //Find the paramnum th parameter of the function called
	target = ParamOf(i->call,paramnum);
	if (target!=0) {
	//update the transition
	word = (*(i->elements))(e)->word; 
	if (word->length() > 1){
	  cerr << "Non generator recursion" << endl;
	  exit(1);
	}
	if (word->length()==0) {  //Introduce epsilon trans, remove later
	  symbol = subs[1]->CombineSymbol(i->statdir.Int(), epsilon);
	  int statement=i->statdir.Int();
	  if (debugLookForCalls){
	    cout << "Adding transition from " << curparam+1 << " to " 
		 <<  target+1 << "with symbol (" << t.alphabet.ContName(statement) 
		 << "," << "eps" << ")" << endl;
	  }
	} 
	//Use current just to get at member function
	else {
	  symbol = subs[1]->CombineSymbol(i->statdir.Int(), word->Head().Int());
	  int statement=i->statdir.Int(), worddir = word->Head().Int();
	  if (debugLookForCalls){
	    cout << "Adding transition from " << curparam+1 << " to " 
		 <<  target+1 << "with symbol (" << t.alphabet.ContName(statement) 
		 << "," << t.alphabet.DirName(worddir) << ")" << endl;
	  }
	}
	curtable[curparam+1][tableindex++] = symbol;
	curtable[curparam+1][tableindex++] = target+1;//curparam+1
	}
      }
      //Otherwise ignore
      paramnum++;
      i->elements->next(e);
    }
  }
}


int
Code::ParamOf(int func, int num) {

  Pix f = funcs.flist->first();
  Function *fun;
  int x, param;
  
  for (x=1; x<func; x++) {
    funcs.flist->next(f);
  }
  fun = (*(funcs.flist))(f);
  Pix p = fun->paramlist->first();

  for (x=1; x<num; x++) {
    fun->paramlist->next(p);
  }
  param = (*(fun->paramlist))(p);

  //cout << "The " << num << "th param of function " 
  //     << func << " is " << param << endl;
  return param;
}

void
Code::CheckType(Instruction *ins) {
  
  DirType writedir,readdir;
  switch(ins->type) {
  case INSCALL: {
    Pix x=ins->elements->first();
    while (x!=ins->elements->end()) {
      if((*(ins->elements))(x)->word->Type() == SCALAR){
	cerr << ins->statdir << ": " 
	     << "Cannot call with scalar" << endl; 
      }
      ins->elements->next(x);
    }
    break;}
  case INSRW: {
    writedir = ins->write->Type();
    if (writedir == GEN) {
      cerr << ins->statdir << ": " 
	   << "Cannot write to generator direction" << endl;
      break;
    }
    Pix x=ins->elements->first();
    while (x!=ins->elements->end()) {
      readdir = (*(ins->elements))(x)->word->Type();
      if ((writedir == SCALAR) && (readdir != SCALAR)) {
	  cerr << ins->statdir << ": " 
	       << "Cannot write pointer to scalar" << endl;
	  break;
      }
      if ((writedir == LINK) && (readdir == SCALAR)) {
	cerr << ins->statdir << ": " 
	     << "Cannot write scalar to pointer" << endl;
	break;
      }
      ins->elements->next(x);
    }
    break;}
  case INSNEW: {
    if (ins->write->Type() != GEN) {
      cerr << ins->statdir << ": " 
	   << "Cannot allocate to non-generator direction"
	   << endl;
    }
    break;
  }
  case INSRETURN: { break;}
  case INSEMPTY: { break;}
  default: {    break;}
  }
}

void
Code::InitMustlist(Instruction *ins) {

  Must newone;
  newone.predlist = ins->predlist;
  newone.statdir = ins->statdir;
  mustlist.append(newone);
  //Add to list of statements with no predicates
  if (ins->predlist.empty()) nopred.append(ins->statdir);
}

void
Code::BuildMustlist() {

  DoToAllIns(&Code::InitMustlist); //Copy predicate information across
  Pix a=mustlist.first(), b=mustlist.first();
  

  while (a!=mustlist.end()) {
    while (b!=mustlist.end()) {
      if (Implies(mustlist(a).predlist,mustlist(b).predlist)){
	mustlist(a).executed.append(mustlist(b).statdir);
      }
      mustlist.next(b);
    }
    b = mustlist.first();
    mustlist.next(a);
  }
  cout << "Execution implications:\n" << mustlist << endl;
}


//Update for globals
void
Code::Print(Instruction *ins) {
  
  Dir d;
  switch(ins->type) {
    
  case INSCALL: {
    cout << functable.Name(ins->call) 
	 << "(";
    Pix x=ins->elements->first();
    while (x!=ins->elements->end()) {
      cout << indvars.Name((*(ins->elements))(x)->ind) << "->" 
	   << *((*(ins->elements))(x)->word) << " " ;
      ins->elements->next(x);
    }
    cout << ");";
    break;}
  case INSRW: {
    if (ins->writevar>0) {
    cout << indvars.Name(ins->writevar) << "->" << *(ins->write)  << " = ";
    }
    else {
      cout << d.alphabet.DirName(-ins->writevar) << "->" << *(ins->write)  << " = ";
    }
    Pix x=ins->elements->first();
    int current;
    while (x!=ins->elements->end()) {
      current = (*(ins->elements))(x)->ind;
      if (current>0){
	cout << indvars.Name((*(ins->elements))(x)->ind) << "->" 
	     << *((*(ins->elements))(x)->word) << " " ;
      }
      else {
	cout << d.alphabet.DirName(-current) ;
      }
      ins->elements->next(x);
    }
    cout << ";";
    break;}
  case INSRETURN: {
    cout << "return;"; 
    break;}
  case INSNEW: {
    cout << indvars.Name(ins->writevar) << "->" << *(ins->write)  << " = ";
    cout << "new Tree;";
    break;
  }
  case INSEMPTY: {
    cout << "no-op;";
    break;}
  default: {
    cout << "Unrecognised operation;";
    break;}
  }
  //cout << ins->predlist << endl;
  cout << "\t//\t" << ins->statdir << ":" << endl;
}

void
Code::AnnotateControlSymbols(Instruction *ins) {

  annotate << "[" << ins->endline << ":]";
  annotate << "\t//\t" << ins->statdir << ":" << "[:]" << endl;
}


//Just adds symbol names 
void
Code::Annotate() {

  string annfile = file + ".ann";

  //sprintf(annfile,"%s.ann", file);
  annotate.open(annfile);
  DoToAllIns(&Code::AnnotateControlSymbols);
  annotate.close();
}


void
Code::GenWords() {

  DoToAllIns(&Code::AddWords);

}


void
Code::AddWords(Instruction *ins)  {

  if ((ins->type == INSCALL) || (ins->type == INSRW)) {
    //Get words from elements
    Pix e=ins->elements->first();
    while (e!=ins->elements->end()) {
      words.append(*((*(ins->elements))(e)->word));
      ins->elements->next(e);
    }
  }

  if ((ins->type == INSRW)||(ins->type == INSNEW)) {
    //Get word for write
    words.append(*(ins->write));
  }
}


//Iterators for all instructions 
void
Code::DoToAllIns(void (Code::*insfunc)(Instruction*)){

  Pix x = funcs.flist->first();
  
  while (x!=funcs.flist->end()) {
    HandleBlock(((*funcs.flist)(x))->body, insfunc);
    funcs.flist->next(x);
  }
}


void
Code::HandleBlock(Block *b, void (Code::*insfunc)(Instruction*)) {

  if (b==NULL) return;
  if (b->next) HandleBlock(b->next, insfunc);
  HandleStatement(b->s, insfunc);
}


void 
Code::HandleStatement(Statement *s, void (Code::*insfunc)(Instruction*)) {

  if (s==NULL) return;
  if (s->type==IFSTAT) {
  HandleBlock(s->ifthen, insfunc);
  HandleBlock(s->ifelse, insfunc);
  }
  else {
    //Call insfunc
    (this->*insfunc)(s->ins);
  }
}
//End iterators

//Similar approach for handling predicates
//Really need a Do-to-all-nodes iterator, but not yet worth it.
void
Code::PredDoFunctions() {

  Pix x = funcs.flist->first();
  while(x!=funcs.flist->end()) {
    PredDoBlock(((*funcs.flist)(x))->body);
    funcs.flist->next(x);
  }
}

void
Code::PredDoBlock(Block *b) {

  if (b==NULL) return;
  if (b->next != NULL) {
    b->next->predlist = b->predlist; 
    PredDoBlock(b->next);
  }
  if (b->s != NULL) {
    b->s->predlist = b->predlist;
    PredDoStatement(b->s);
  }
}


void
Code::PredDoStatement(Statement *s) {

  if (s==NULL) return;
  if (s->type==IFSTAT) {
    Pred p = *(s->pred);
    //cout << *(s->pred) << s->predlist << endl;
    //cout << s->ifthen->predlist << endl;
    //Handle `then' section 
    s->ifthen->predlist = s->predlist;
    s->ifthen->predlist.append(p);
    //cout << s->ifthen->predlist << endl;
    PredDoBlock(s->ifthen);

    //Handle `else', if it exists
    if (s->ifelse != NULL) {
      s->ifelse->predlist = s->predlist;
      p.Not();
      //cout << s->ifelse->predlist << endl;
      s->ifelse->predlist.append(p);
      //cout << s->ifelse->predlist << endl;
      PredDoBlock(s->ifelse);
    }
  }
  else { //It has an instruction hanging off it
    s->ins->predlist = s->predlist;
    //cout << s->ins->statdir << ": " << s->predlist << endl; 
  }
}

void
Code::OutputSubs() {

  BaseDesc<MapFsa> subsoutput;

  for (int i=1; i<=indvars.Size(); i++) {
    subsoutput.AddOp(subs[i]);
  }
  subsoutput.Davinci("subs-induction-var");
}



//Needs rethinking. We need to prune for each read within a compound
//read/write instruction
//Prune is a bit dodgy anyway, since we cannot guarantee instruction
//execution

void
Code::Prune(Instruction *ins) {

  if (ins->type != INSRW) return;

  /* 
     cout << "Treating " << func.stmts[rw].statdir << ":\t" 
     << func.stmts[rw].write 
     << "\t=\t" << func.stmts[rw].readlist << endl;
     
     func.stmts[rw].use.Davinci("current-use");
     rd.Combine(func.stmts[rw].use, writeaccess);
     rd.And(causal);
     rd.SetDir(func.stmts[rw].statdir);
     unpruned.AddOp(rd);
     rd.Prune();
     pruned.AddOp(rd);
     pruned.Davinci("pruned-sources");
     unpruned.Davinci("unpruned-sources");
  */
}

