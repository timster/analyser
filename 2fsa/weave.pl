#!/usr/local/bin/perl -w

#Strip out lines with latex output, marked with ///
#Similar to DOC++ behaviour
#Itemize remaining algorithm

$curr=0;
while (<STDIN>) {
    if (/^(\s*)(\/\/\/)(.*)$/){
	#print length $1;
	$len = length $1;
	if ($len == 0) {
	    print $3 , "\n" ;
	}
	elsif ($len > $curr) {
	    print "\\begin{itemize}\n" x (($len - $curr) / 2)  
		, "\\item ", $3 , "\n";
	}
	elsif ($len < $curr) {
	    print "\\end{itemize}\n" x (($curr - $len) / 2)  
		, "\\item ", $3 , "\n";
	}
	elsif ($len == $curr) {
	    print "\\item ", $3 , "\n";
	}

	$curr = $len;
    }
}
print "\\end{itemize}\n" x (($curr) / 2);
















