#ifndef DIRTAB_HH
#define DIRTAB_HH

#define NUMDIRS MAXTAB
#define MAXTAB 60

#include <string>
#include <iostream>
#include <map>
#include "DLList.hh"
#include "defs.hh"
#include "fsalib/fsalib.hh"

using namespace std;


//Class that maintains mapping from integers <-> names 
class NameTab  {

  map<string, int> tablesi;
  map<int, string> tableis;
  
  int num;
  //string name[MAXTAB];
  int lookup(string s);

public:

  NameTab();
  int Add(string s);   //Returns 0 if s is already there
  int Lookup(string s); //Returns 0 for non existent name
  string Name(int);
  int Size();

  friend ostream& operator << (ostream&, NameTab&);
  friend class DirTab;
};

// Clash with NONE from the parser...
enum DirType{GEN, LINK, SCALAR, GLOBAL, DT_NONE};

//Stores names of directions and control symbols
class DirTab {

  int numdirs;    //1 more than actual number of directions
  int numcontrol;
  NameTab   dirnames;
  NameTab   contnames;
  DirType dirtype[NUMDIRS];
  int epsilon;

  void Add(string s, DirType d);

public:

  DirTab();
  void GenAdd(string s);
  void LinkAdd(string s);
  void ControlAdd(string s);
  void ScalarAdd(string s);
  void GlobalAdd(string s);
  void EpsilonAdd();    //Set the value of epsilon 
  int Epsilon();
  int ContLookup(string s); //Returns zero for nonexistent name
  int DirLookup(string s);
  DirType Type(int);
  string DirName(int);
  string ContName(int);
  int Size();
  int ControlNum();
  int DirNum();
  int GenNum();

  friend ostream& operator << (ostream&, DirTab&);
};

#endif















