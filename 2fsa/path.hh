#ifndef PATH_HH
#define PATH_HH

#include "dir.hh"
#include "DLList.hh"





//Need `pathnames in structures' and `controlwords'.
class Path : public DLList<Dir> {

public:

  Path();
  bool Siblings(Path &p);
  bool ParentOf(Path &p);
  bool PrefixOf(Path *p);
  DirType Type();
  Dir Head();
  //friend class Desc;
};


class ControlWord : public Path {

public:

  ControlWord();

};

#endif
