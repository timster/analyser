#include "dir.hh"

//Initialise the direction table
DirTab Dir::alphabet;

void
Dir::SetTab(DirTab d) {

  alphabet = d;
}


int operator == (const Dir&t1, const Dir& t2){

  if (t1.dir == t2.dir) return 1;
  return 0;
}

int operator != (Dir&t1, Dir& t2){

  if (t1.dir == t2.dir) return 0;
  return 1;
}


string
Dir::Name() {

  return alphabet.DirName(dir);
}

string
Cont::Name() {

  return alphabet.ContName(dir);
}

ostream&
operator << (ostream& s, Dir &t) {

  s << t.Name();
  return s;
}

