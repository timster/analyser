#include "admatrix.hh"


Admatrix::Admatrix() {

  int i,j;

  numpaths = 0;

  for (i=0; i < MAXNODE; i++) {

    namelist[i] = NULL;
    ntype[i] = CALLNODE;

    for (j=0; j < MAXNODE; j++) {
      matrix[i][j] = NODEP;
    }
  }
}


Admatrix::~Admatrix() {

  int i;

  for (i=0; i < MAXNODE; i++) {
    delete namelist[i];
  }
}


  
int
Admatrix::Lookup(Path p) {

  int current = 0;

  while (namelist[current]) {

    if (current==MAXNODE) return -1;
    if ( *namelist[current] == p) return current;
    current++;
  }

  return -1;
}

void
Admatrix::Add(Path p) {

  Path *n;

  n = new Path(p); // Create new copy

  namelist[numpaths] = n;
  

  numpaths++;
  if (numpaths > MAXNODE) {
    cerr << "Error, you have created too many nodes"
	 << endl;
    exit(1);
  }
}



void
Admatrix::AddLink(Path a, Path b, dependtype type) {

  int anum, bnum;

  anum = Lookup(a);
  bnum = Lookup(b);

  if (anum == -1) {Add(a); anum=Lookup(a);}
  if (bnum == -1) {Add(b); bnum=Lookup(b);}


  matrix[anum][bnum] = type;
  //if (type==NODEP) {cout << "No dep dependency added" << endl;}
}


void
Admatrix::SetType(Path a, nodetype type) {

  int x = Lookup(a);

  if (x == -1) return; //Ignore words we do not know about

  ntype[x] = type;
}


void
Admatrix::Davinci(string filename) {

  int i,j;
  string node, edgecolour, edgetype;
  ofstream file;

  file.open(adddav(filename));

  file << "[" << endl;

  for (i=0; i < numpaths; i++) {

    if (ntype[i]==CALLNODE) node = "ellipse";
    if (ntype[i]==LEGALLEAF) node = "circle";
    if (ntype[i]==ORPHANLEAF) node = "box";
    

    //Output node header stuff
    file << "l(\"" 
	 << *namelist[i]
	 << "\",n(\"\",[a(\"OBJECT\",\""
	 << *namelist[i]
	 << "\"),a(\"_GO\",\""
	 << node
	 << "\")"
	 << "]," << endl;
    
    file << "[" << endl;
    
  
    for (j=0; j < numpaths; j++) {
      //If link exists
      if (matrix[i][j] != NODEP) {
	edgecolour="black";
	switch (matrix[i][j]) {
	case WWDEP:
	  edgetype = "solid";
	  edgecolour = "brown";
	  break;
	case WRDEP:
          edgetype = "solid";
	  edgecolour = "pink";
	  break;
	case RWDEP:
	  edgetype = "solid";
	  edgecolour="red";
	  break;
	case SPAWN:
	  edgetype = "dotted";
	  break;
	case INDEP:
	  edgetype = "solid";
	  break;
	}
	file << "l(\"" << *namelist[i] << "->"
	     << *namelist[j]
	     << "\",e(\"\",[" 
	     << "a(\"EDGEPATTERN\",\"" << edgetype << "\"),"
	     << "a(\"EDGECOLOR\",\"" << edgecolour << "\""
	     << ")],r(\""
	     << *namelist[j]
	     << "\")))," << endl;
      }
    }
    //Output node tail stuff
    file << "]" << endl;
    file << ")),\n\n" << endl;
  }

  file << "]" << endl;
  
  file.close();

}




ostream&
operator << (ostream& str, Admatrix& a) {
  
  int i,j;

  str << "Matrix..." << endl;
  for (i=0; i < a.numpaths; i++) {
    for (j=0; j < a.numpaths; j++) {
      if (a.matrix[i][j] != NODEP) {
	str << *(a.namelist[i]) << "\t" 
	    << *(a.namelist[j]) << "\t"
	    << a.matrix[i][j] << endl;
      }
    }
  }
  str << "...End of Matrix" << endl;
  return str;
}




