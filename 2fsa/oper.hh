#ifndef OPER_HH
#define OPER_HH

#include "twofsa.hh"
#include "onefsa.hh"

class MapFsa;
class ContSlice;

//Stores controlword->controlword
class ConflictFsa : public TwoFsa {

private:

  int numcont;

protected: 

  string Name1(int d);
  string Name2(int d);
  int *flag;   //Flags used for searching for transitions

  void LookForLoops(int state);
  void SubLookForLoops(int state);
  void RemoveLoop(int state);


public:

  ConflictFsa();
  ConflictFsa(ConflictFsa*);

  void MakeCausal();
  void MakeBottom();
  void AddSecond(Dir second, fsa *f);
  void MakeNext(ContSlice *c); //Maps x accepted by c to next one accepted by c  
  //void Prune(); //Remove all 'earlier' trans, considered broken now
  void Combine(ConflictFsa *a, ConflictFsa *b){CombineTFSA(a,b);}
  //void Combine(MapFsa *a, InvMapFsa *b){CombineTFSA(a,b);};
  void Invert(){InvertTFSA();}
  void BackDecomp(MapFsa *a, MapFsa *b); 

  void PruneInfiniteMaps();
  void Find_ATransition();

  void MakePair(ContSlice *a, ContSlice *b);
  void MakeWaitsFor(ConflictFsa *Accessconf);
  bool IsMonotonic();


};


//Alphabet is control symbols
class ContSlice : public OneFsa {

public:

  ContSlice();
  ContSlice(ContSlice *);
  string Name(int d);
  void Combine(ContSlice *a, ConflictFsa *b){CombineOTFSA(a,b);}
};


//Stores pathname->pathname
class DirFsa : public TwoFsa {


private:

  int numdir;

protected:

  string Name(int d);
  string Name1(int d);
  string Name2(int d);

public:

  DirFsa();
  DirFsa(char *file);
  DirFsa(DirFsa *df);
  DirFsa(int gen);  //Create an fsa for a generator direction 
  //DirFsa(char *f1, char *f2, int axiomtype);
  ~DirFsa();
  //void Axiom1and3(fsa *f1, fsa *f2);
  //void Axiom2(fsa *f1, fsa *f2);
  void MakeRemoveEps();  //Removes one epsilon transition from a path 
  void Combine(DirFsa *a, DirFsa *b){CombineTFSA(a,b);}
  void MakeIdentity(); //Accept if w1 == w2
  //  void Combine(InvMapFsa *a, MapFsa *b){CombineTFSA(a,b);}
  void Invert() {InvertTFSA();}
  void Reverse();
  void MakeJoinCode(bool leaf, ostream &out);
  bool IsOne2One();  //Does each path link to a unique target??
};


//Stores controlword->pathname
class MapFsa : public TwoFsa {

private: 

  int numdir;
  int numcont;


protected:

  string Name1(int d);
  string Name2(int d);
  string Name(int d);

public:

  MapFsa();
  MapFsa(MapFsa*);
  ~MapFsa();

  void SetFirstRW(int rw);
  void MakeGlobal(Dir g);
  bool RemoveAllEps();  //Remove epsilon transitions from the map
  bool HasEpsTrans();
  bool HasNonGenTrans();
  //void SetRW(Dir read, Dir write);   //Append control letters to operation

  void Combine(ConflictFsa *a, MapFsa *b){CombineTFSA(a,b);}
  void Combine(MapFsa *a, DirFsa *b){CombineTFSA(a,b);}
};



//Alphabet is directions
class StructSlice : public OneFsa {


public:

  StructSlice();
  string Name(int d);
  void Combine(StructSlice *a, DirFsa *b){CombineOTFSA(a,b);}
  void Combine(ContSlice *a, MapFsa *b){CombineOTFSA(a,b);}

};

#endif


