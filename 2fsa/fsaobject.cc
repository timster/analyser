#include "fsaobject.hh"

//Set default flags
Fsa::Fsa() {

  tmalloc(mult, fsa, 1);
  fsa_init(mult);
  mult->flags[DFA]=true;
}

Fsa::Fsa(Fsa *o) {
  
  //cout << "FSA to be copied" << endl << o << endl;
  
  tmalloc(mult,fsa,1);
  fsa_init(mult);
  fsa_copy(mult, o->mult);  //Copies fsa across
}

Fsa::~Fsa() {

  //cout << "Clearing FSA " << endl;
  fsa_clear(mult);
  tfree(mult);
}



void
Fsa::SetStates(int states) {

  mult->states->size = states;
}

void
Fsa::SetupAccepting(){

  fsa_set_is_accepting(mult);
}

bool
Fsa::Accepting(int state){

  if (mult->is_accepting[state]) return true;
  return false;
}

void
Fsa::FreeAccepting() {

  tfree(mult->is_accepting);
}


void
Fsa::AddAccepting(int x) {

  SetupAccepting();
  mult->is_accepting[x]=true;
  WritebackAccepting();
  FreeAccepting();
}


void
Fsa::WritebackAccepting() {

  int numaccept=0;
  for (int i=1;i<=mult->states->size; i++) {
    if (mult->is_accepting[i]) numaccept++;
  }

  tfree(mult->accepting);
  tmalloc(mult->accepting,int,numaccept+1);
  mult->num_accepting = numaccept;

  int current = 1;

  for (int i=1;i<=mult->states->size; i++) {
    if (mult->is_accepting[i]) {
      mult->accepting[current++]=i;
    }
  }
}

void 
Fsa::SetInitial(int start){

  mult->num_initial = 1;
  //tfree(mult->initial);
  tmalloc(mult->initial, int, 2);
  mult->initial[1] = start;
}

void 
Fsa::SetAccept(DLList<int>*  l){

  Pix o = l->first();
  int num = l->length(), i=1;
  mult->num_accepting = num;
  //tfree(mult->accepting);
  tmalloc(mult->accepting, int, num+1);
  
  while (o!=l->end()) {
    
    mult->accepting[i] = (*l)(o);
    l->next(o);
    i++;
  }
}


void
Fsa::OnlyAccept(int state) {

  //tfree(mult->accepting);
  tmalloc(mult->accepting, int, 2);
  mult->accepting[1] = (int) state;
  mult->num_accepting = 1;
}


bool
Fsa::Equal(Fsa *f) {

  //if (fsa_equal(mult,f->mult)) return true;
  //return false;
  //Better way
  if (ContainedIn(f) && f->ContainedIn(this)) return true;
  return false;
}


bool
Fsa::ContainedIn(Fsa *f){

  fsa *andnot;
  andnot = fsa_and_not(mult, f->mult, DENSE,FALSE,"temp");
  fsa_minimize(andnot);
  if (Empty(andnot)) return true;
  return false;
}

bool
Fsa::Overlaps(Fsa *f){

  fsa *anda;
  anda = fsa_and(mult, f->mult, DENSE,FALSE,"temp");
  fsa_minimize(anda);
  if (Empty(anda)) return false;
  return true;
}



void
Fsa::Tidy() {

  //Set flags so that minimisation is actually carried out
  mult->flags[MINIMIZED] = FALSE;
  mult->flags[ACCESSIBLE] = FALSE;
  mult->flags[TRIM] = FALSE;

  fsa_make_accessible(mult);
  fsa_minimize(mult);
  //Make sure accepting states are set up properly
}


void
Fsa::And(Fsa *a) {

  fsa *fsaand;
  fsaand = fsa_and(mult, a->mult, DENSE, FALSE, "temp");
  fsa_clear(mult);
  fsa_init(mult);
  fsa_copy(mult, fsaand);
  fsa_clear(fsaand);
  fsa_minimize(mult);
}


void
Fsa::And(Fsa *a, Fsa *b){

  fsa_clear(mult);
  fsa_init(mult);
  mult = fsa_and(a->mult, b->mult, DENSE, FALSE, "temp");
}


void
Fsa::Not() {

  fsa * nota;
  nota = fsa_not(mult,DENSE);
  fsa_clear(mult);
  fsa_copy(mult,nota);
  fsa_clear(nota);
  tfree(nota);
}


void
Fsa::AndNot(Fsa *f) {

  fsa *fsaandnot;
  fsaandnot = fsa_and_not(mult, f->mult, DENSE, FALSE, "temp");
  fsa_clear(mult);
  fsa_init(mult);
  fsa_copy(mult, fsaandnot);
  fsa_clear(fsaandnot);
  fsa_minimize(mult);
}

void Fsa::Determinize(){

  fsa *fsadet;
  fsadet = fsa_determinize(mult,DENSE,FALSE,"temp");
  fsa_minimize(fsadet);
  fsa_clear(mult);
  fsa_init(mult);
  fsa_copy(mult, fsadet);
  fsa_clear(fsadet);
  fsa_minimize(mult);
}


void
Fsa::MakeAllAccepting(){

  mult->num_accepting = mult->states->size;

  tfree(mult->accepting);

  tmalloc(mult->accepting,int,mult->num_accepting+1);
  for(int i=1;i<=mult->num_accepting;i++) 
    mult->accepting[i]=i;
}

/*
void
Fsa::FSAAnd(fsa *f) {

  fsa *fsaand;
  fsaand = fsa_and(mult, f, DENSE, FALSE, "temp");
  fsa_clear(mult);
  fsa_init(mult);
  fsa_copy(mult, fsaand);
  fsa_clear(fsaand);
  fsa_minimize(mult);
}
*/

void
Fsa::Or(Fsa &a) {

  fsa *fsaor;
  fsaor = fsa_or(mult, a.mult, DENSE, FALSE, "temp");
  fsa_clear(mult);
  fsa_init(mult);
  fsa_copy(mult, fsaor);
  fsa_clear(fsaor);
  fsa_minimize(mult);
}

void
Fsa::Or(Fsa *a, Fsa *b) {

  mult = fsa_or(a->mult, b->mult, DENSE, FALSE, "temp");
}


void
Fsa::Or(Fsa *a) {

  fsa *fsaor;
  fsaor = fsa_or(mult, a->mult, DENSE, FALSE, "temp");
  fsa_clear(mult);
  fsa_init(mult);
  fsa_copy(mult, fsaor);
  fsa_clear(fsaor);
  fsa_minimize(mult);
}

string
Fsa::StateName(int d) {

  string s = to_string(d);
  //tmalloc(s,char,5);
  //sprintf(s,"%d",d);
  return s;
}

int
Fsa::NumSymbols() {

  if (mult->alphabet->base == NULL) return mult->alphabet->size; //For 1 var
  return mult->alphabet->base->size; //For 2
} 



int
Fsa::Size() {

  return mult->states->size;
}

bool
Fsa::Empty(fsa *f) {

  if (f==NULL) { 
    //cerr << "Mult is NULL" << endl;
    return true;}
  if (f->states->size==0) {
    //cerr << "No states" << endl;
    return true;}
  if (f->num_accepting==0) { 
    //cerr << "No accepting" << endl;
    return true;}
  return false;
}


bool
Fsa::Empty() {

  return Empty(mult);
}

void
Fsa::PrintHeader() {

  int numstates = mult->states->size, s;
  bool first = true;

  fsa_set_is_accepting(mult);
  fsa_set_is_initial(mult);

  cout << "{:" << endl;
  cout << "\tStates " << numstates << ";" << endl;
  cout << "\tStart ";
  for (s=1; s <= numstates; s++) {
    if (mult->is_initial[s]) cout << s;
  }
  cout << "; Accept ";
  for (s=1; s <= numstates; s++) { 
      if (mult->is_accepting[s]){
	if (first) {cout << s; first = false;}
	else { cout << ", " << s;}
      }
    }
  cout << ";" << endl;
}


int 
operator == (Fsa&, Fsa&) {

//   if ((o1.direction == o2.direction) && (o1.rules == o2.rules)) {
//     return 1;
//   }
  return 1;
}






