../exe.sh mesh.frog > mesh.out

diff mesh.out mesh.corr

if [ $? -eq 0 ]
then
    echo "Tests completed correctly"
    exit 0
else
    echo "Test failed: incorrect results"
    exit 1
fi


