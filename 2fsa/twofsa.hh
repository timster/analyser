#ifndef TWOFSA_HH
#define TWOFSA_HH

#include <iostream>
#include <fstream>
#include <string>
#include "DLList.hh"
#include "fsalib/fsalib.hh"
#include "fsaobject.hh"
#include "path.hh"
#include "onefsa.hh"

class OneFsa;


class TwoTrans {

public:

  int to;
  int a;
  int b;
};

typedef  DLList<TwoTrans*> TwoTransList;



// Lots of functions take/return fsa*, should change this to work
// with this object
class TwoFsa : public Fsa {
    
private:

protected:

  void AllocateTransTable(); //Allocate array for table

  //int CountStatesFor(DLList<int> l);
  //void RecCountStatesFor(DLList<int> l, int currstate);

  void Copy(TwoFsa *);

  int numsym;  //Total number of symbols in the global alphabet
  int alphsize; //Size of product alphabet
  int sizea; //Size of first alphabet
  int sizeb; //Size of second alphabet
  int padding;  //Padding symbol number
  int **data;   //Pointer to transition data

  virtual string Name1(int){return "placeholder";}
  virtual string Name2(int){return "placeholder";}
  string PairName(int a,int b);  //Create name of symbol pair
  void CombineTFSA(TwoFsa *a, TwoFsa *b); //Compose Two 2FSAs
  void InvertTFSA();  //Invert

  //Fsa functions, should change these, really
  void CreateAlphabet(fsa* f, int numsym);
  void MakeAlphabet(fsa *fsaptr); //Create alphabet
  void MakeNotEqual(); //Accepts w1 != w2
  void MakeAppendFsa(fsa *f, fsa* onevar);   //Make accept (p.RE1, p) 
  void PrintSparse(fsa *f); //Print fsa
  
  void AddTrans(int from, int to, int first, int second);
  int Trans(int from, int x, int y);


public:

  Path direction;   //Name for the operation
  
  //   //Constructors
  //   //TwoFsa(const TwoFsa &);
  TwoFsa(TwoFsa *);
  TwoFsa(string filename);
  //   TwoFsa(char *f1, char *f2, int axiomtype);//Build from axioms
  TwoFsa();
  //   TwoFsa(int gen);  //Build generating TwoFsaation automatically
  //   //TwoFsa operator = (const TwoFsa& o); 
  ~TwoFsa(); 
  //   //Create from ASAP axioms
  //   void Axiom1and3(fsa *f1, fsa *f2);
  //   void Axiom2(fsa *f1, fsa*f2);

  void AddTrans(DLList<TwoTransList*>* l); //Add a list of transitions (parser)

  int Target(int from, int first, int second);


  void MakeAlphabet();

  void ExpandAlphabet(); //Update to the current alphabet

  void AddSecond(int second);  //Accepts (x,x.second)
  void AddFirst(int first); //Accepts (x.second, x);
  void MakeIdentity(); //Accept if w1 == w2


  void MakeNone(); //Accept nothing
  void MakeAll();  //Accept everything
  //   void MakeCausal(); //Accept if w1 > w2
  //   void MakeBottom(); //Accept (w1,epsilon) for w1 control word
  void MakeLegal();  //Accept if the pair is legal w.r.t padding symbol
  void MakePrefix(Path l); //Accepts (l.?, ??)
  void MakePrefix(Path a, Path b); //Accepts (a.?, b.?)
  //   void MakeFeautrierh() ; //Accepts (u.i.??, u,j.??) 
  //   void MakeGlobal(Dir g); //Accepts (??,g) 
  void MakeDouble(OneFsa *C); //Accepts (x,x) if C accepts x
  bool HasEpsTrans();    //Does it have epsilon transitions?
  bool HasNonGenTrans();    //Has it got non generator trans?
  //   //bool RemoveAllEps();  //Attempt to remove all epsilon transitions
  void KillSelfTrans(int s);   //Remove all transitions from state s -> s

  //Deal with symbol pairs
  int MergePair(int first, int second, int symnum);//For symbols from a different alphabet
  int CombineSymbol(int first, int second); //For symbols from this alphabet
  int SplitSymbolFirst(int symbol);
  int SplitSymbolSecond(int symbol);

  void SetDir(Dir);         //Set direction
  void SetDir(Path  f);   //Set direction with format
  void MakeAppendFsa(OneFsa *c);               //Make accept (p.RE1, p)

  //   void TrimDirTran();  //Remove non-control-symbol transitions 

  //   //Display functions
  void Enum(int length);
  void PrintSparse(); //Print fsa 
  void Davinci(string filename); void Davinci(ostream &out); //Output graph

  //   bool CutEarlier(); //Attempt to remove an 'earlier' transistion

  void AcceptFirst(Path f); //Trim 2-var fsa such that only (f.w1,w2) style pairs are accepted

  //   void SetFirstRW(Dir rw); //Append RW label to FSA

  bool Accept(Path a, Path b);  //Do we accept this pair of strings??
  bool IsClosureOf(TwoFsa *o);
  
  int SizeA(){return sizea;}
  int SizeB(){return sizeb;}

  friend ostream& operator << (ostream&, TwoFsa& o);
  friend int  operator == (TwoFsa&o1, TwoFsa& o2);

  friend class Code;
  friend class OneFsa;
  friend class StructSlice;
  friend class ContSlice;
  friend class ClosureMaker;
  friend class Arden;
  friend class ConflictFsa;
};











#endif













