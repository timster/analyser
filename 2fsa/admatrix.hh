#ifndef ADMATRIX_HH
#define ADMATRIX_HH


#include <iostream>
#include <fstream>
#include <string>
#include "path.hh"
#include "misc.hh"


#define MAXNODE 500
//Different type of dependency

enum dependtype{NODEP, INDEP, WWDEP, WRDEP, RWDEP, SPAWN};
enum nodetype{LEGALLEAF, ORPHANLEAF, CALLNODE};

class Admatrix {

private:

  int matrix[MAXNODE][MAXNODE];
  Path *namelist[MAXNODE];
  nodetype ntype[MAXNODE];

  int numpaths;

  int Lookup(Path p); // -1 if path does not exist
  void Add(Path p);

public:


  Admatrix();
  ~Admatrix();
  
  void AddLink(Path a, Path b, dependtype type);
  void SetType(Path a, nodetype type);
  void Davinci(string filename); //Output in daVinci format
  friend ostream& operator << (ostream&, Admatrix&);
  
};

#endif
