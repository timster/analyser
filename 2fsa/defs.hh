#ifndef DEFS_HH
#define DEFS_HH
 

//All purpose constants
//#define NULL 0
#define PRIME 11

//#define TRUE  1
//#define FALSE 0


//enum Bool{false, true};


//Debugging information, turn on for lam
#define DEBUG



class Opts {


public:
  
  Opts() {
    JoinCode = false;
    Annotate = false;
    DavCode = false;
    WaitsFor = false;
    GlobalConflict = false;
  }
  bool JoinCode;
  bool Annotate;
  bool DavCode;
  bool WaitsFor;
  bool GlobalConflict;
};

extern Opts options;



#endif








