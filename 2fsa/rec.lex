/* Scanner for the simple imperative language with recursive constructs */
%{
  //#include "lexer.hh"
  //int lineno;

  static void skip_comment(void);
  
%}

%option nomain

DIGIT    [0-9]
ALPHA    [A-Z]|[a-z] 
     
%%
"\n" {lineno++;}
"class"       return CLASS;
"static"      return STATIC;
"public"      return PUBLIC;
"structslice"  return STRUCTSLICE;
"contslice"  return CONTSLICE;
"States"      return STATES;
"int"|"float"|"double"|"char" return SCALARVALUE;
"Start" return START;
"Accept"    return ACCEPT;
"Transitions"  return TRANSITIONS;
"new"          return NEW;
"Tree"         return TREE;
"Treelink"     return TREELINK;
"REVERSE"            return REV;
"*"            return POINTER;
"~"            return LINKER;
"@"            return STAR;
"["     return LSQRBRA;
"]"    return RSQRBRA;
"if"      return IF;
"void"   return VOID;
"()"     return COND;
"->"      return ARROW;
":"    return COLON;
"all"  return ALL;
"none" return NONE;
"+"|"-"|"*"|"/"    return OPER;
"!="         return NOTEQUAL;
">" | ">=" | "<" | "<=" | "==" "!=" return RELOP;
"&&" return AND;
"||" return OR;
"!" return NOT;
"\^" return INV;
"=" return ASSIGN;
"=="   return EQUALS;
"("   return LBRACKET;
")"   return RBRACKET;
","   return COMMA;
"{"   return LBRACE;
"}"    return RBRACE;
"NULL" return ZEROPOINTER; 
"."   return CONCAT;
";"   return TERMINATOR;
"return" return RETURN;
"else" return ELSE;
"\""  return QUOTES;
"Func" return FUNC;
"//"[^\n]*    /* eat up one-line comments */
[ \t]+          /* eat up whitespace */
"/*"					{ skip_comment(); }

{ALPHA}({ALPHA}|{DIGIT})*  {
  yylval.str = strdup(yytext);
  return IDENT;}

"_" {
  yylval.str = strdup(yytext);
  return IDENT;}

{DIGIT}+ {
  (yylval.ival) = atoi(yytext);
  return INTEGER;
}

.           printf( "Unrecognized character: %s\n", yytext );
     
%%


/*
 * We use this routine instead a lex pattern because we don't need
 * to save the matched comment in the `yytext' buffer.
 */
static void
skip_comment(void)
{
	int c1, c2;

	c1 = yyinput();
	c2 = yyinput();

	while (c2 != EOF && !(c1 == '*' && c2 == '/')) {
		if (c1 == '\n')
			++lineno;
		c1 = c2;
		c2 = yyinput();
	}
}



