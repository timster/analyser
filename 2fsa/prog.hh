#ifndef PROG_HH
#define PROG_HH

#include <fstream>
#include "DLList.hh"
#include "dirtab.hh"
#include "oper.hh"
#include "path.hh"

const bool ISCORRECT = true;  //Can we assume that the program
                            //is correct as given??

//Define all possible types of statement 
enum InsType{INSEMPTY, INSCALL, INSRW, INSRETURN, INSNEW, INSNOOP};
enum StatType{IFSTAT, INSTRUCTSTAT};
enum PredType{ISNULL, NOTNULL, UNKNOWN};


class Elem {
public:
  int  ind;
  //Allow read from a global direction
  Path *word;
};

class Pred {
public:
  PredType type;
  Elem *elem;
  void Not();
  friend int operator == (Pred& , Pred&);
  friend ostream & operator << (ostream&, Pred&);
  friend bool Implies(Pred &, Pred &);
};

class Predlist : public DLList<Pred> {

public:
  //Does a => b ?
  friend bool Implies(Predlist &a, Predlist &b); 
};

//Base class for all program nodes
class Node {
public:
  Predlist predlist;
};


//Stores information about the statements that must be 
//executed when this one is 
class Must : public Node {
public:
  Cont statdir;
  DLList<Dir> executed;

  friend ostream& operator << (ostream &s, Must &);
  friend int operator == (Must&,Must&);
};

class Instruction : public Node{
public:

  InsType type;            //Type of statement
  int call;             //Function that statement calls
  int writevar;         //Store write variable, or allocation variable
  int startline;
  int endline;
  //Allow write to a global direction
  Path *write;           //Store write word 

  //List of read vars or call params
  //And words that go with them
  DLList<Elem*>  *elements; 
  MapFsa *def, *use;        //FSAs for information accessed
  Cont statdir;          //Store name of statement
};

class Statement;

class Block : public Node {
public:
  Statement *s;
  Block *next;
};

class Statement : public Node {
public:
  StatType   type;
  Instruction *ins;
  Pred       *pred;
  Block     *ifthen, *ifelse;
};

class Function {

public:
  int          num;
  DLList<int>  *paramlist;
  Block        *body;
};



class Prog {

  int numfunc;
  DLList<Function*> *flist;

public:

  Prog();
  void  SetFuncs(DLList<Function*> *f){flist=f;};

  //void  BuildE();

  //Relates to unrolled loops
  void OnlyThisCall(MapFsa *); //Only keep dependencies within one call of function
  friend ostream& operator << (ostream&, Prog&);
  friend class Code;
  
};



#endif
