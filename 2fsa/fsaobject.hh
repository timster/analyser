#ifndef FSAOBJECT_HH
#define FSAOBJECT_HH


#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include "DLList.hh"
#include "dir.hh"
#include "fsalib/fsalib.hh"
#include "misc.hh"


//The basic finite state machine class, interfaces with kbmag stuff
class Fsa {

private:

  bool Empty(fsa *f);

  
protected:
  
  fsa  *mult;
  virtual string StateName(int d);
  
  
public:
  
  //   Fsa(const Fsa & f);
  Fsa(Fsa*);
  Fsa();
  virtual ~Fsa();
  bool Empty();
  void SetStates(int states);
  void SetInitial(int start);
  void SetAccept(DLList<int>*  l);
  void OnlyAccept(int s); //Set accepting state to s 
  
  void MakeAllAccepting();
  void And(Fsa *a);
  void And(Fsa *a, Fsa *b);
  void AndNot(Fsa *);
  void Not();
  
  void Determinize();
  //void FSAAnd(fsa *f);
  void Or(Fsa &a);
  void Or(Fsa *o);
  void Or(Fsa *o, Fsa *p);
  
  void SetupAccepting();
  bool Accepting(int a);
  void FreeAccepting();
  void WritebackAccepting();
  void AddAccepting(int x);
  
  bool Equal(Fsa *);
  bool ContainedIn(Fsa *);
  bool Overlaps(Fsa *);
  int Size();  //Number of states
  int NumSymbols(); //Returns numsym

  void PrintHeader();  //Display fsa information

  //int Target(int symbol, int state);
  void Tidy(); //Tidy up by minimizing
  
  //   friend int operator == (Fsa &f1, Fsa &f2);
  //   Fsa operator = (const Fsa& o);  
};
#endif



