// This may look like C code, but it is really -*- C++ -*-
/* 
   Copyright (C) 1988 Free Software Foundation
   written by Doug Lea (dl@rocky.oswego.edu)
   cout stuff done by Tim Lewis (tl@dcs.ed.ac.uk)
*/


#ifndef _DLList_h
#ifdef __GNUG__
//#pragma interface
#endif
#define _DLList_h 1

#undef OK

#include <iostream>
#include <string>
#include <list>

using namespace std;

// Need to check the syntax here
//template <class T> 
//class Pix : public list<T>::iterator{
//};

#define Pix auto

template <class T>
class DLList : public list<T> {

  /*
  virtual void delete_node(BaseDLNode *node) { delete (DLNode<T>*)node; }
  virtual BaseDLNode* copy_node(const void *datum)
    { return new DLNode<T>(*(const T*)datum); }
  virtual void copy_item(void *dst, void *src) { *(T*)dst = *(T*)src; }
  */
  
public:

  string                  sep;
  void                    Sep(string p) {sep = p; }

  DLList() : list<T>() { }
  
  //DLList(const DLList<T>& a) : BaseDLList() { copy(a);}

  //DLList<T>&            operator = (const DLList<T>& a)
  ///{ BaseDLList::operator=((const BaseDLList&) a); return *this; }

  //virtual ~DLList() {  }

  void prepend(const T& item) {list<T>::push_front(item);}
  void append(const T& item) {list<T>::push_back(item);}

  T& operator () (typename list<T>::iterator p) {
    return (*p);
  }

  /*
  const T&              operator () (Pix p) const {
    if (p == 0) error("null Pix");
    return ((DLNode<T>*)p)->hd;
  }
  */

  typename list<T>::iterator first() { return list<T>::begin(); }
  typename list<T>::iterator last()  { return list<T>::end();}

  typename list<T>::iterator del(typename list<T>::iterator pos)  {
    return list<T>::erase(pos);}

  //typename list<T>::iterator join(typename list<T>::iterator pos, int num)  {
  //  return list<T>::erase(pos, pos+num);}

  //void splice(typename list<T>::iterator pos, list& x, int num)  {
  //return list<T>::erase(pos, pos+num);}

  void next(typename list<T>::iterator& p) const { p++;}
    
  void prev(typename list<T>::iterator& p) const {p--;}

  int length() {return list<T>::size();}

  
  //My stream output function
  friend ostream& operator << (ostream& s, DLList<T>& l) {

    typename list<T>::iterator d = l.first(); 
    
    while (d!=l.end()) {
      s << l(d);
      l.next(d);	
      if (d!=l.end()) s << l.sep;
    }
    return s;
  }
  
  // Want to handle iterator!=0 type constructs 
  /*
  friend int operator == (Pix<T> &it, int n){
  */
  
    
};

#endif



