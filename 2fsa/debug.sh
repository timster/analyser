export FSADIR=~/phd/Analyser/2fsa

export DYLD_LIBRARY_PATH=$FSADIR/fsalib


# This is for debugging under lldb, to make sure it finds the library
install_name_tool -change libfsa.dylib $FSADIR/fsalib/libfsa.dylib exe


lldb -- $FSADIR/exe tests/mesh.frog

