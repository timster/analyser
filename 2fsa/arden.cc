#include "arden.hh"


const int debugArdenTransitions = TRUE;
const int newSolver = TRUE;


ostream& operator << (ostream &s, Term &t){

    s << "E" << t.expression << "(" << t.contword << ")" ;
    
    return s;
}


ostream& operator << (ostream &s, Arden &a){

  for (int i=0; i<a.MAXEXPR; i++) {
    if (a.exp[i]) {
    s << "E" << i << " = " << *(a.exp[i]) << endl;
    }
  }
    return s;
}

ostream& operator << (ostream &s, Expression &a){

  Pix p=a.first();
  while (p!=a.end()){
    
    s << *a(p);
    a.next(p);
    if (p!=a.end()) s << " | ";
  }
  s << "\t";
  if (a.tainted) s << "Tainted\t";
  if (a.solved) s << "Solved\t";
  if (a.inloop) s << "InLoop\t"; 
  return s;
}


Term::Term() {

  expression = -1;
  contword = -1;
}


Expression::Expression() {

  seen = false;
  tainted = false;
  solved = false;
  inloop = false;
}

void
Expression::Davinci() {

  Pix p = first();
  char fname[30];
  cout << "Printing expression" << endl;
  while(p!=end()) {
    sprintf(fname, "E%d", (*this)(p)->expression);
    (*this)(p)->op->Davinci(fname);
    this->next(p);
  }
}


Arden::Arden(MapFsa *o, Desc<DirFsa> *desc){

  int i;
  for(i=0; i<MAXEXPR; i++) {exp[i]=NULL;}

  Dir t;
  int numdir = o->SizeB(),
    numcont = o->SizeA(),
    numstates = o->mult->states->size,
    //numgen = t.dirnames.GenNum(),
    //**data = o->mult->table->table_data_ptr,
    letter;
  Term *term;
  
  varop = o;

  cout << "Building Arden expressions" << endl;
  cout << "Numstates " << numstates << endl;
  
  ContSlice *c = new ContSlice();
  c->ExistsFirst(o);
  c->Davinci("first");

  o->Davinci("ardenop");

  //Create first expression
  exp[1] = new Expression;
  int target;

  for (int s = 1; s <= numstates; s++) {
    for (int x = 1; x <= numcont+1 ; x++) {
      for (int y = 1; y <= numdir+1 ; y++) {
	letter = o->CombineSymbol(x,y);
	if ((x == numcont+1) && (y==numdir+1)) continue;
	target = o->Target(s,x,y);
	if (target!=0) {
	  //Add term  s,y to expression data 
	  term = new Term;
	  term->expression = s;
	  term->contword = x;
	  term->isgen = (t.alphabet.Type(y)==GEN);
	  if (debugArdenTransitions) {
	    cout << "Found transition " << s << ":" 
		 << "(" << x << "," << y << ")->" << target << endl;
	  } 
	  if (y==numdir+1) {
	    term->op = new DirFsa();
	    term->op->MakeIdentity();
	  }
	  else {
	    term->op = new DirFsa(desc->NewFindOp(Dir(y)));
	  }
	  if (exp[target]==NULL) {
	    //Create new expression
	    exp[target] = new Expression;
	  }
	  //Append
	  exp[target]->append(term);
	  if (t.alphabet.Type(y)!=GEN){exp[target]->tainted=true;}
	}
	else {
	  if (debugArdenTransitions) {
	    //  cout << "No transition " << s << ":" 
	    // << "(" << x << "," << y << ")->" << data[letter][s] << endl;
	  } 
	}
      } 
    }
  }
  if (debugArdenTransitions) {
    cout << *this << endl;
  }
  if (newSolver) NewSolver();
  else Solve();
  //Davinci();
}


void
Arden::Resolve() {

  int i=1;
  solution = new MapFsa();
  solution->MakeNone();
  
  //Go through each accepting state
  for (i=1; i<=varop->mult->num_accepting;i++) {
    cout << "Resolving Expression " <<  varop->mult->accepting[i] << endl;
    ResolveExpr(varop->mult->accepting[i]);
  }
  solution->Davinci("solution");
}



void
Arden::ResolveExpr(int expr) {

  if (!exp[expr]){
    cout << "Null expression" << endl;
    return;
  }
  Pix p = exp[expr]->first();
  MapFsa *trunc, *control, *second;
  ContSlice *cont;
  ConflictFsa *first;

  cout << "Resolving expression " << expr << endl;

  while(p!=exp[expr]->end()) {
    cout << "Looking at terms" << endl;

    //Build first component by taking the original varop and 
    //killing all second components beyond the current state 

    //Create the truncated operation
    //Copy the original varop
    trunc = new MapFsa(varop);
    //Set state expression to be the sole accepting state
    trunc->OnlyAccept((*exp[expr])(p)->expression);
    trunc->Davinci("1");

    //Make second component of the FSA
    control = new MapFsa(varop);
    control->SetInitial((*exp[expr])(p)->expression);
    control->KillSelfTrans((*exp[expr])(p)->expression);
    cont = new ContSlice();
    cont->ExistsFirst(control);
    cont->Davinci("control");
    first = new ConflictFsa();
    first->MakeAppendFsa(cont);
    second = new MapFsa();
    second->Combine(first, trunc);
    second->Combine(second, (*exp[expr])(p)->op);
    
    second->Davinci("approx");

    //OR with the solution 
    solution->Or(*second);
    exp[expr]->next(p);
  }
}



//Does not seem to be used yet
//Mark all states that are in loops
//Look for looped states that are non-generating and taint
//Taint all states in that loop
void
Arden::FindLoops(int i) {

  if (exp[i]->seen) {exp[i]->inloop=true; return;}
  exp[i]->seen=true;
  
  //Follow to all children
  Dir t;
  int **data = varop->mult->table->table_data_ptr,
    numdir = t.alphabet.DirNum(),
    alphsize = (numdir+1)*(numdir+1);
  
  for (int j=1; j<alphsize; j++) {
    if ((data[j][i]!=0)) {
      FindLoops(data[j][i]);
    }
  }
} 


void
Arden::DoTaint() {

  int i=1;
  //Find the first tainted expression
  cout << "Doing Tainting" << endl;
  while(exp[i]){
    if (exp[i]->tainted) {
      //Taint all `below' it
      Taint(i);
    }
    i++;
  }
}

void
Arden::Taint(int i) {

  //Read transitions from state i of varop
  Dir t;
  int **data = varop->mult->table->table_data_ptr,
    numdir = t.alphabet.DirNum(),
    alphsize = (numdir+1)*(numdir+1);
  
  for (int j=1; j<alphsize; j++) {

    if ((data[j][i]!=0)&&(!exp[data[j][i]]->seen)) {
      exp[data[j][i]]->seen = true;
      exp[data[j][i]]->tainted = true;
      Taint(data[j][i]);
    }
  }
}


void
Arden::Solve() {

  int i,j;

  DoTaint();
  TidyAll();
  cout << *this << endl;
  
  i = FirstUnsolved();
  while(i>0) {
    //Find first unsolved expression

    //If expression is reflexive, resolve that
    if (IsReflexive(i)) RemoveReflexive(i);
    //Substitute something into expression
    ExpandExpr(i);
    
    //If expression is now solved, update this
    if (CheckSolved(i))exp[i]->solved = true;
    //Tidy expression
    Tidy(i);
    j = FirstUnsolved();
    if (i==j) i = j+1;
    else i=j;
    cout << "First unsolved " << i << endl;
    cout << *this << endl;
  }
  Resolve();
}


void
Arden::NewSolver() {

  int i,j,k;

  DoTaint();
  TidyAll();
  cout << *this << endl;
  

  i = FirstUnsolved();
  while(i>0) {
    //Find first unsolved expression

    //If expression is reflexive, resolve that
    if (IsReflexive(i)) RemoveReflexive(i);
    //Substitute something into expression
    ExpandExpr(i);


    for (j=FirstUnsolved(); j<MAXEXPR; j++) {

      if (!exp[j]) continue;
      
      //If expression is reflexive, resolve that
      if (IsReflexive(j)) RemoveReflexive(j);
      
      SubsInAll(j);
      
      //If expression is now solved, update this
      if (CheckSolved(j))exp[j]->solved = true;
      //Tidy expression
      Tidy(j);
      j++;
      //cout << "First unsolved " << i << endl;
      cout << *this << endl;
    }

    
    //If expression is now solved, update this
    if (CheckSolved(i))exp[i]->solved = true;
    //Tidy expression
    Tidy(i);
    k = FirstUnsolved();
    if (k==i) k = i+1;
    i=k;
    cout << "First unsolved " << k << endl;
    cout << *this << endl;
  }

  Resolve();
}


//(Term does not have to be solved, (just not reflexive) to want to subs it.)
void
Arden::ExpandExpr(int expr) {

  //Go through all subterms
  //If term is tainted and solved,  subs it in expression
  Pix p = exp[expr]->first();
  int term;
  while (p!=exp[expr]->end()) {
    term = (*exp[expr])(p)->expression;
    
    if (exp[term]->tainted && exp[term]->solved) {
      Subs (term,expr);
      return;
    }
    exp[expr]->next(p);
  }
}


int
Arden::FirstUnsolved() {

  for(int i=1; i<MAXEXPR; i++) {
    if ((exp[i]!=NULL)&&(exp[i]->tainted)&&(!exp[i]->solved))
      return i;
  }
  return 0;  //No unsolved, we are finished
}

//Not used 
bool
Arden::RemoveAllReflexive() {

  bool work=false;
  for(int i=0; i<MAXEXPR; i++) {
    if ((exp[i]!=NULL)&&(exp[i]->tainted)&&(!exp[i]->solved)&&(IsReflexive(i)))
      RemoveReflexive(i);
    work=true;
  }
  return work;
}

bool
Arden::IsReflexive(int expr) {

  Pix p = exp[expr]->first();
  while (p!=exp[expr]->end()) {
    if ((*exp[expr])(p)->expression == expr) return true;
    exp[expr]->next(p);
  }
  return false;
}


void
Arden::RemoveReflexive(int expr) {

  //Or together all of the oper in reflexive terms
  DirFsa *ora=new DirFsa();
  DirFsa *closure;
  ora->MakeNone();
  Pix p = exp[expr]->first();

  cout << "Removing reflexive expression " << expr << endl;

  while (p!=exp[expr]->end()) {

    if ((*exp[expr])(p)->expression == expr){
      ora->Or(*(*exp[expr])(p)->op);
      p = exp[expr]->del(p);  //Remove reflexive term
    }
    else{
      exp[expr]->next(p);
    }
  }
  //Form closure 
  ora->Davinci("for-closure");
  ClosureMaker close(ora);
  closure = close.Make();
  //Combine closure onto all oper in other expressions
  p = exp[expr]->first();
  while(p!=exp[expr]->end()) {
    (*exp[expr])(p)->op->Combine((*exp[expr])(p)->op,closure);
    exp[expr]->next(p);
  }
}

bool
Arden::SubsInAll(int expr) {

  bool work = false;
  for(int i=0; i<MAXEXPR; i++) {
    if ((exp[i]!=NULL) && (exp[i]->tainted) && (!exp[i]->solved) && (i!=expr) ){
      cout << "Want to subs " << expr << " into " << i << endl; 
      if (Subs(expr, i)) work = true;
    }
  }
  //Remove expr
  //delete exp[expr];
  //exp[expr]=NULL;
  return work;
}

bool
Arden::Subs(int expr, int into) {

  //Go through all expressions in into 
  Pix p = exp[into]->first();
  Term *newt;
  DirFsa *a,*b;
  bool work = false;


  while (p!=exp[into]->end()) {

  //If equal to (expr,o), append all expressions in expr 
  //append o to their operations
    if ((*exp[into])(p)->expression == expr){
      work = true;
      Pix q = exp[expr]->first();
      while (q!=exp[expr]->end()){
	newt = new Term;
	newt->op = new DirFsa();
	newt->expression = (*exp[expr])(q)->expression;
	a = (*exp[expr])(q)->op;
	b = (*exp[into])(p)->op;
	newt->op->Combine(a,b);
	exp[into]->append(newt);
	exp[expr]->next(q);
      }
      p = exp[into]->del(p);
    }
    else {exp[into]->next(p);}
  }
  if (work) {
    cout << "Substituted " << expr << " into " << into << endl;
  }
  return work;
}


void
Arden::Tidy(int expr){

  Expression *ex = exp[expr];

  Pix p = ex->first();

  while(p!=ex->end()){
    Pix q = p;
    ex->next(q);
    while(q!=ex->end()) {
      //If both have same expression base, OR together operations
      if ((*ex)(p)->expression == (*ex)(q)->expression) {
	(*ex)(p)->op->Or(*(*ex)(q)->op);
	q = ex->del(q);
      }
      else ex->next(q);
    }
    ex->next(p);
  }
}


bool
Arden::CheckSolved(int i) {

  Pix p = exp[i]->first();
  int sub;

  while (p!=exp[i]->end()) {
    
    sub = (*exp[i])(p)->expression;
    if (exp[sub]->tainted) return false;
    exp[i]->next(p);
  }
  return true;

}

void
Arden::TidyAll() {

  for(int i=0; i<MAXEXPR; i++) {
    if ((exp[i]!=NULL) && (!exp[i]->solved) ) {
      if (CheckSolved(i)){
	exp[i]->solved = true;
      }
      if (exp[i]->tainted) Tidy(i);   //Only tidy if the expr is tainted
    }
  }
}


void
Arden::Davinci() {

  int i=1;
  //cout << "Num accepting " << varop->mult->num_accepting << endl;
  for (i=1; i<=varop->mult->num_accepting;i++) {
    exp[varop->mult->accepting[i]]->Davinci();
  }
}













