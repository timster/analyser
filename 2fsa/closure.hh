#ifndef CLOSURE_HH
#define CLOSURE_HH

#include <iostream>
#include <fstream>
#include <string>
#include "DLList.hh"
#include "fsalib/fsalib.hh"
#include "fsaobject.hh"
#include "path.hh"
#include "oper.hh"
#include "onefsa.hh"


#define MAXSTATES 1000


//NB these 2FSA define their own alphabets and
//Manipulate their own numsym, sizea, sizeb etc...
/*
class Duel: public TwoFsa {

private:

  int numstate;

public:

  Duel(DirFsa *o);
  Duel(){};
  char * Name(int d);
  char * StateName(int d);
  int CombineSymbol(int,int);
  friend class StateTrans;
};
*/

class StateTrans: public TwoFsa {

private:


  //Deal with symbol pairs, including barred symbols
  //Custom versions 
  //Uses negative values for barred versions
  int CombineSymbol(int first, int second);
  int SplitSymbolFirst(int symbol);
  int SplitSymbolSecond(int symbol);
  int numbarred;
  int numstate;

public:

  StateTrans(DirFsa *o);    //Make Duel 
  StateTrans(StateTrans *d, int input, int output);   //Make from Duel
  StateTrans(int numbarred);
  void MakeInitial(); //Make initial
  string Name(int d);
  string Name1(int d){return Name(d);}
  string Name2(int d){return Name(d);}
  string StateName(int d);
  void MakeRemoveInactive();
};

class State : public OneFsa {

private:

  int numbarred;

public:

  State(int numbarred);
  string StateName(int d);
  string Name(int d);
  int MapSym(int x);
  void MakeInitial();
  void RemoveInactive(State *s);
  void Active(int numbarred);
  bool LeafState(int state);
  bool ActiveTransTo(int state);
  void RemoveState(int state);
  bool RemoveLeaf();
  void Reverse();
  void Cull();
  bool Fail();
  void Combine(State *a, StateTrans *b){CombineOTFSA(a,b);}
};


class ClosureMaker {

private:

  StateTrans *trans[MAXTAB][MAXTAB];
  State *st[MAXSTATES];
  int statenum;
  int numbarred;
  DirFsa *oper;  //Operation we are finding the closure of
  int numdir;
  DirFsa *closure; //Approximate closure result

public:

  ClosureMaker(DirFsa *);
  DirFsa*  Make();
  bool BuildStates(int max);
  bool BuildStatesViaTrans(int max);
  bool SameTrans(int a, int b);
  void RemoveState(int a, int b);
  int StateNumber(State *);
  bool Accept(State *s);
  void Compare(State *s);
  void RemoveOverlap(State *s);
  void OutputOverlaps();
};

#endif


