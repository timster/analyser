#ifndef ONEFSA_HH
#define ONEFSA_HH

#include <iostream>
#include <fstream>
#include "DLList.hh"
#include "fsalib/fsalib.hh"
#include "fsaobject.hh"
#include "twofsa.hh"

class TwoFsa;

class OneTrans {

public:
  
  int to;
  int a;
};


typedef DLList<OneTrans*> OneTransList;


//Stores conditional information. Basically a one variable regular language
class OneFsa : public Fsa { 

private:

  void PruneGen();

protected:

  int numsym;
  int sizea;      //Actual size of alphabet
  void CombineOTFSA(OneFsa *c, TwoFsa *o);
  void MakeAlphabet(fsa *fsaptr);
  int **data;
  void AllocateTransTable();

public:

  int direction;

  //OneFsa(const OneFsa &);
  OneFsa(string filename);
  OneFsa(OneFsa *copy);
  OneFsa();
  //OneFsa operator = (const OneFsa& o);
  ~OneFsa(); 

  void AddTrans(DLList<OneTransList*>* l); //Add a list of transitions (parser)

  void AddTrans(int from, int to, int sym);


  int Target(int state, int sym);
  bool Accept(Path &p);
  void MakeAll();
  void MakeNone();
  void MakeAlphabet();
  int CombineSymbol(int first, int op, int numdir);
  void MakeAppendFsa(fsa *f, fsa* onevar); 
  void MakeNotEqual(fsa *f);
  void ExistsFirst(TwoFsa *op); //Accept w1 if Op accepts (w1,w2) for some w2
  void ExistsSecond(TwoFsa *op);//Accept w2 if Op accepts (w1,w2) for some w1
  void AppendDir(Dir d);
  void Enum(int length);
  void PrintSparse(); //Print fsa 
  void Davinci(ostream &out); //Output graph
  void Davinci(string filename);
  void PrintSparse(fsa *f); //Print fsa

  virtual string Name(int d)=0;
  
  friend ostream& operator << (ostream&, OneFsa& o);
  friend int  operator == (OneFsa&o1, OneFsa& o2);
  friend class Code;
  friend class TwoFsa;
  friend class Proc;
};



#endif
