#ifndef DIR_HH
#define DIR_HH

#include "defs.hh"
#include "dirtab.hh"

//Maximum number of directions 
#define MAXDIR 50

//Define a control symbol as well as a direction symbol
class Dir {

protected:

  int dir;

public:

  Dir(){dir=0;}
  Dir(int i) {dir=i;}
  Dir operator = (const Dir& d){dir=d.dir;return *this;} 
  Dir operator = (const int& d){dir=d;return *this;}
  int Int() {return dir;}
  void Set(int i) {dir=i;}
  void Invert(){dir = -dir;}

  static DirTab alphabet; //Global table of direction names
  void SetTab(DirTab d);

  friend ostream& operator << (ostream&, Dir& t);
  virtual string Name();
  friend int operator == (const Dir&t1, const Dir& t2);
  friend int operator != (Dir&t1, Dir& t2);
};


class Cont : public Dir {

public:

  Cont(){dir=0;}
  Cont(int i) {dir=i;}
  Cont operator = (const Cont& d){dir=d.dir;return *this;} 
  Cont operator = (const int& d){dir=d;return *this;} 
  string Name();
};




#endif







