#include "oper.hh"


MapFsa::MapFsa() {

  Dir t;
  sizea = numcont = t.alphabet.ControlNum();
  sizeb = numdir = t.alphabet.DirNum();
}

MapFsa::~MapFsa(){}

MapFsa::MapFsa(MapFsa *m): TwoFsa(m) {MapFsa();}

/*
void
MapFsa::SetFirstRW(Dir rw) {

  fsa *fsarw, *fsacomb;
  
  if (mult->states->size == 0) {
    cerr << "Empty FSA for appending control read/write symbol" << endl;
    return; //Ignore if empty
  }
  //cout << "AddFirst " << rw << endl;
  tmalloc(fsarw, fsa, 1);
  tmalloc(fsacomb, fsa, 1);
  AddFirst(rw, fsarw);
  //PrintSparse(fsarw);
  //Davinci("setfirst1");
  fsacomb = fsa_composite(fsarw,mult, DENSE, FALSE, "temp", TRUE);
  fsa_clear(mult);  //Should be able to clear here 
  fsa_init(mult);
  fsa_copy(mult,fsacomb); //Copy back
  //Davinci("setfirst2");
  fsa_clear(fsarw); fsa_clear(fsacomb);
  fsa_minimize(mult);  //Tidy up
}
*/

void
MapFsa::SetFirstRW(int rw) {

  ConflictFsa *f;

  f = new ConflictFsa();
  f->AddFirst(rw);
  f->Davinci("f");
  Combine(f, this);
  delete f;
}


void
MapFsa::MakeGlobal(Dir g) {

  int global=g.Int();
  
  mult->flags[DFA] = true;
  //Make states, initial and accepting
  mult->states->size = 2;
  mult->num_initial = 1;
  tmalloc(mult->initial, int, 2);
  mult->initial[1] = 1;
  mult->num_accepting = 1;
  tmalloc(mult->accepting, int, 2);
  mult->accepting[1] = 2;

  //Make alphabet
  MakeAlphabet();

  //Make table
  AllocateTransTable();
  //Handle double empty symbol seperately
  //Fix transitions from state 1
  for (int x = 1; x <= numcont+1 ; x++) {
    for (int y = 1; y <= numdir+1 ; y++) {
      if ((x == numcont+1) && (y==numdir+1)) continue;
      if (y==global) AddTrans(1,2,x,y);
      else AddTrans(1,0,x,y);
    }
  }    
  //Fix transitions from state 2
  //Make all transitions back to state 2
  for (int x = 1; x <= numcont+1 ; x++) {
    for (int y = 1; y <= numdir+1 ; y++) {
      if ((x == numcont+1) && (y==numdir+1)) continue;
      if (y == numdir+1) AddTrans(2,2,x,y);
      else AddTrans(2,0,x,y);
    }
  }
  //Davinci("global");
}

//What does this do ??????
//Does not appear to be used
/*
void
MapFsa::SetRW(Dir read, Dir write) {
  
  fsa *fsaw, *fsacomp, *fsar, *fsafinal;
  
  //Change name of operation
  //SetDir(read);       //Do we need to change this??

  if (mult->states->size == 0) return; //Ignore if empty

  tmalloc(fsaw, fsa, 1);
  tmalloc(fsacomp, fsa, 1);
  tmalloc(fsar, fsa, 1);
  tmalloc(fsafinal, fsa, 1);

  //Make fsarw accept only strings terminated by write in the second var
  AddSecond(write, fsaw);

  //Compose it with the current fsa
  fsacomp = fsa_composite(mult,fsaw, DENSE, FALSE, "temp", TRUE);

  //Create read appended fsar
  AddSecond(read, fsar);

  //Invert fsacomp and compose with rsar
  fsa_swap_coords(fsacomp);
  fsafinal = fsa_composite(fsacomp,fsar, DENSE, FALSE, "temp", TRUE);
  
  //Swap back
  fsa_swap_coords(fsafinal);

  //Copy it to this TwoFsaation
  fsa_clear(mult);         //Should be able to clear here, timremove
  fsa_init(mult);
  fsa_copy(mult, fsafinal);

  //Clean up
  fsa_clear(fsaw);
  fsa_clear(fsacomp);
  fsa_clear(fsar);
  fsa_clear(fsafinal);

  //Minimise to tidy up
  fsa_minimize(mult);
}
*/


//Change
string
MapFsa::Name1(int d) {

  Dir t;
  if (d==sizea+1) d = numsym+1;
  return t.alphabet.ContName(d);
}

//Change
string
MapFsa::Name2(int d) {

  Dir t;
  if (d==sizeb+1) d = numsym+1;
  return t.alphabet.DirName(d);
}


bool
MapFsa::RemoveAllEps() {

  DirFsa *epsrem=new DirFsa();

  int attempts = 5;
   
  epsrem->MakeRemoveEps();
  epsrem->Davinci("eps-rem");
  //epsrem->PrintSparse();

  for (int i=0; i < attempts; i++) {
    
    if (HasEpsTrans()) {
      cout << "About to combine" << endl;
       Combine(this, epsrem);
       Davinci("remove-attempt");
    }
  }
  delete epsrem;
  if (HasEpsTrans()) return false;
  return true;
}

//For some symbol has nonzero (x,eps)
bool
MapFsa::HasEpsTrans() {

  Dir t;
  int epsilon,
    states = mult->states->size;

  epsilon=t.alphabet.Epsilon();
  data = mult->table->table_data_ptr;
  
  //Fix transitions from each generator state
  for (int i=1; i<=states; i++) {
    for (int x = 1; x <= numcont+1 ; x++) {
      if  (Target(i,x,epsilon)) return true;
    }
  }
  return false;
}

bool
MapFsa::HasNonGenTrans() {

  Dir t;
  int states = mult->states->size;
  data = mult->table->table_data_ptr;
  
  //Fix transitions from each generator state
  for (int i=1; i<=states; i++) {
    for (int x = 1; x <= numcont+1 ; x++) {
      for (int y = 1; y < numdir+1 ; y++) {
	if ((x == numcont+1) && (y==numdir+1)) continue;
	if  ( Target(i,x,y) && (t.alphabet.Type(y)!=GEN) && (y!=padding)){
	  cout << "Non generator " << y << endl;
	  return true;
	}
      }
    }
  }
  return false;
}


