#ifndef ARDEN_HH
#define ARDEN_HH


#include <iostream>
#include <fstream>
#include "DLList.hh"
#include "path.hh"
#include "desc.hh"
#include "oper.hh"
#include "closure.hh"


class Term {

public:

  int expression;
  int contword;
  bool isgen;
  DirFsa * op;

  Term();
  
  friend ostream& operator << (ostream &s, Term &);

};

//Expression is Term | Term | Term | Term
class Expression : public DLList<Term *> {

public:

  Expression();
  void Davinci();
  bool seen;
  bool tainted;   //If the expression is part of the approximating bit
  bool solved;   //If expression is in terms of only untainted expressions
  bool inloop;   //If expression is part of a loop

  friend ostream& operator << (ostream &s, Expression &);

};


class Arden {

private:

  static const int MAXEXPR=20;
  MapFsa *varop;
  Expression *exp[MAXEXPR];    //Array of expressions

  void Solve();            //Old solution
  void NewSolver();      //New solution
  void Resolve();
  void ResolveExpr(int i);
  bool RemoveAllReflexive();
  void RemoveReflexive(int expr);
  bool SubsInAll(int expr);
  bool Subs(int expr, int into);
  bool IsReflexive(int expr);
  void DoTaint();
  void Taint(int exp);
  void FindLoops(int);
  void Tidy(int expr);
  void TidyAll();
  bool CheckSolved(int i);
  int FirstUnsolved();
  void ExpandExpr(int expr);

public:

  Arden(MapFsa *o, Desc<DirFsa> *desc);  //Build from the induction variable 2FSA 
  void Davinci();
  MapFsa *solution;

  friend ostream& operator << (ostream &s, Arden &);
};

#endif

