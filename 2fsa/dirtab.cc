#include "dirtab.hh"

NameTab::NameTab() {

  // Note we start numbering at 1
  num = 1;
}

int
NameTab::Size() {

  return num-1;
}

int
NameTab::Add(string s) {
  
  int index = lookup(s);
  
  if (index == 0){ // Element was not there, so add it

    //cout << "Adding " << s << " to NameTab" << endl;
    tablesi[s] = num;
    tableis[num] = s;
    return num++;
  }
  else { 

    return 0; //Check for (and ignore) repeated names
  }

}

int
NameTab::lookup(string s) {

  map<string, int>::iterator it = tablesi.find(s);

  if (it == tablesi.end()){ // Element was not there

    return 0;
  }
  else { //Return the index

    return tablesi[s]; 
  }
}

int
NameTab::Lookup(string s) {

  int found=lookup(s);
  if (lookup(s) == 0) {
    cerr << "Could not find " << s << " in NameTab" << endl;
    return 0;  //Do not quit on error
    //exit(1);
  }
  return found;
}


string
NameTab::Name(int d) {

  if ((d < num) && (d > 0)) {

    return tableis[d];
    //return name[d];
  }
  if (d == num) {
    cerr << "You are asking me for the padding symbol of smaller alphabet: "
	 << d << endl;
    return "SmallerPad" ;
  }
  cerr << "Name out of bounds for " << d << endl;
  exit(1);
  //return "Huge";
}


ostream&
operator << (ostream& str, NameTab &d) {

  
  for (int i = 1; i < d.num ; i++) {
    //str << d.name[i] << "   ";
    str << d.tableis[i] << "   ";
  }
  return str;
}


DirTab::DirTab() {

  numdirs = 1;
  numcontrol = 0;
  for(int i =0; i < NUMDIRS; i++) dirtype[i] = DT_NONE;
 //cout << "Direction table initialised" << endl;
}

int
DirTab::Size() {

  int c, d;
  c = contnames.Size();
  d = dirnames.Size();
  if (c > d) return c;
  return d;
}

int 
DirTab::ControlNum() {
  
  return contnames.Size();
}

int 
DirTab::GenNum() {
  
  int gen=0;
  //cout << numdirs << endl;
  //cout << "Generators:" ;
  for (int i=1; i<numdirs; i++) {
    if (dirtype[i]==GEN) {
      gen++;
    //cout << DirName(i) << " ";
    }
    //cout << dirtype[i] << endl;
  }
  //cout << endl;
  return gen-1; //Remove eps generator
}

int 
DirTab::DirNum() {
  
  return dirnames.Size();
}


void
DirTab::Add(string s, DirType d) {

  //Check for (and ignore) repeated names
  int index;
  if (dirnames.lookup(s) > 0) return;
  index = dirnames.Add(s);
  if (index>0) dirtype[index] = d;
  numdirs++;
}

void
DirTab::EpsilonAdd() {

  epsilon = numdirs;
  GenAdd("eps");
}

int
DirTab::Epsilon() {

  return epsilon;
}

void
DirTab::GenAdd(string s) {Add(s, GEN);}

void
DirTab::LinkAdd(string s) {Add(s,LINK);}


void
DirTab::ScalarAdd(string s) {Add(s,SCALAR);}

void
DirTab::GlobalAdd(string s) {Add(s,GLOBAL);}

void
DirTab::ControlAdd(string s) {

  contnames.Add(s);
}

DirType
DirTab::Type(int d) { 
  
  if (dirnames.Size() >= d) return dirtype[d];
  return DT_NONE;
}


int 
DirTab::ContLookup(string s) {

  if (s == "_") {      //Needed ??????
    return Size()+1;
  }
  return contnames.Lookup(s);
}

int 
DirTab::DirLookup(string s) {

  if (s == "_") {      //Needed ??????
    return Size()+1;
  }
  return dirnames.Lookup(s);
}

string
DirTab::DirName(int d) {

  if (d==Size()+1) return "_";
  return dirnames.Name(d);
}

string
DirTab::ContName(int d) {

  if (d==Size()+1) return "_";
  return contnames.Name(d);
}


ostream&
operator << (ostream& str, DirTab &d) {

  str << "Directions: ";
  str << d.dirnames << endl;
  str << "Control Symbols: ";
  str << d.contnames << endl; 

  return str;
}


