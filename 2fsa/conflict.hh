#ifndef CONFLICT_HH
#define CONFLICT_HH

#include "path.hh"
#include "oper.hh"
#include "admatrix.hh"

#define RECDEPTH 6
#define NUMPROC 8


//Stores information on the nodes accessed from function calls
//Extend to visualisation of all conflict stuff 
class Conflict {

private:

  DLList<ControlWord> allwords;
  DLList<ControlWord> controlwords;
  DLList<ContSlice> accessed;
  DLList<ControlWord> scheduled;




  Admatrix dep;

  void Recurse(int depth, ControlWord p, int state);
  bool Executable(DLList<ControlWord>::iterator p);
  bool Schedulable(DLList<ControlWord>::iterator p);
  void RemoveCalls();


public:

  ContSlice   *functions;
  ConflictFsa   *RafterW,*WafterW, *WafterR, *RafterR,  *causal, *conflict, *AccessConf,
    *WaitsFor, 
    *BareAccessConf,  //The original accessconf information before we messed around pruning it  

  *AC2; //AccessConf squared, to handle deadlocked instructions

  MapFsa *read, *write;


  //Create from function def and conflict description
  Conflict(MapFsa *read, MapFsa *write); 
  ~Conflict(){};
  
  void BuildCilk();  //Create conflict graph for cilk style parallelism
  void BuildDepGraph();  //Create dependency call graph
  void BuildSchedule(); 


  void Davinci(const char *filename);  //Output graph


  friend ostream & operator << (ostream&, Conflict&);
};

#endif

