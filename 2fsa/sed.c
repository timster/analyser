
/*  A Bison parser, made from rec.y
    by GNU Bison version 1.28  */

#define YYBISON 1  /* Identify Bison output.  */

#define	STRUCT	257
#define	CLASS	258
#define	STATIC	259
#define	PUBLIC	260
#define	TREELINK	261
#define	IF	262
#define	ELSE	263
#define	P	264
#define	Q	265
#define	NOTEQUAL	266
#define	ZEROPOINTER	267
#define	TREE	268
#define	POINTER	269
#define	LINKER	270
#define	SCALARVALUE	271
#define	IDENT	272
#define	FUNC	273
#define	INTEGER	274
#define	STATES	275
#define	START	276
#define	ACCEPT	277
#define	TRANSITIONS	278
#define	ALL	279
#define	NONE	280
#define	VOID	281
#define	NEW	282
#define	LBRACKET	283
#define	RBRACKET	284
#define	LSQRBRA	285
#define	RSQRBRA	286
#define	TERMINATOR	287
#define	ASSIGN	288
#define	EQUALS	289
#define	OPER	290
#define	LBRACE	291
#define	RBRACE	292
#define	ARROW	293
#define	COMMA	294
#define	COLON	295
#define	COND	296
#define	QUOTES	297
#define	RETURN	298
#define	STRUCTSLICE	299
#define	CONTSLICE	300
#define	CONCAT	301
#define	OR	302
#define	AND	303
#define	NOT	304
#define	INV	305
#define	REV	306
#define	STAR	307

#line 5 "rec.y"

  
#include <string.h>
#include "dirtab.hh"
#include "stdlib.h"


#define YYDEBUG 1
  
  int yyerror (char *s) ;
  int yylex(); 

#line 20 "rec.y"
typedef union {
  int ival;
  char  *str;
  Path *f;
  Dir *t;
  DirFsa *o;
  //Cond *c;
  Pred *pred;
  Desc<DirFsa> *d;
  DLList<int> *intlist;
  Elem *e;
  Instruction *ins;
  Statement *st;
  Block *block;
  Function *func;
  DLList<Function*>  *flist;
  DLList<Elem*>   *express;
  TwoTrans * twotrans;
  TwoTransList *twotranslist;
  DLList<TwoTransList*> *dirtrans;
  ContSlice *cont;
  StructSlice *struc;
  OneTrans *onetrans;
  OneTransList *onetranslist;
  DLList<OneTransList*> *onetranslistlist;
  int i;
} YYSTYPE;
#ifndef YYDEBUG
#define YYDEBUG 1
#endif

#include <stdio.h>

#ifndef __cplusplus
#ifndef __STDC__
#define const
#endif
#endif



#define	YYFINAL		238
#define	YYFLAG		-32768
#define	YYNTBASE	54

#define YYTRANSLATE(x) ((unsigned)(x) <= 307 ? yytranslate[x] : 100)

static const char yytranslate[] = {     0,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     1,     3,     4,     5,     6,
     7,     8,     9,    10,    11,    12,    13,    14,    15,    16,
    17,    18,    19,    20,    21,    22,    23,    24,    25,    26,
    27,    28,    29,    30,    31,    32,    33,    34,    35,    36,
    37,    38,    39,    40,    41,    42,    43,    44,    45,    46,
    47,    48,    49,    50,    51,    52,    53
};

#if YYDEBUG != 0
static const short yyprhs[] = {     0,
     0,     7,    14,    16,    19,    24,    28,    33,    37,    41,
    43,    46,    53,    56,    58,    60,    64,    65,    68,    72,
    77,    79,    81,    84,    90,    95,   101,   107,   113,   119,
   123,   128,   132,   136,   140,   144,   146,   151,   154,   156,
   160,   163,   164,   173,   177,   181,   185,   189,   192,   195,
   198,   203,   205,   209,   213,   217,   219,   223,   227,   229,
   231,   234,   242,   253,   257,   260,   263,   265,   269,   271,
   275,   279,   284,   290,   293,   299,   307,   309,   313,   325,
   333,   335,   336,   339,   341,   345,   346,   349,   352,   360,
   368,   373,   376,   378,   382,   385,   386
};

static const short yyrhs[] = {    55,
    62,    64,    58,    82,    92,     0,     4,    14,    37,    56,
    38,    33,     0,    57,     0,    56,    57,     0,    14,    15,
    18,    33,     0,    17,    18,    33,     0,    14,    16,    18,
    33,     0,    14,    18,    33,     0,     7,    18,    33,     0,
    59,     0,    58,    59,     0,    27,    18,    29,    61,    30,
    33,     0,    14,    15,     0,    17,     0,    60,     0,    61,
    40,    60,     0,     0,    62,    63,     0,    17,    18,    33,
     0,     5,    17,    18,    33,     0,    65,     0,    66,     0,
    66,    65,     0,    37,    18,    41,    67,    38,     0,    37,
    18,    41,    38,     0,    37,    18,    41,    68,    38,     0,
    37,    18,    41,    78,    38,     0,    37,    18,    41,    25,
    38,     0,    37,    18,    41,    26,    38,     0,    43,    18,
    43,     0,    69,    70,    71,    73,     0,    21,    20,    33,
     0,    22,    20,    33,     0,    23,    72,    33,     0,    72,
    40,    20,     0,    20,     0,    24,    31,    74,    32,     0,
    74,    75,     0,    75,     0,    31,    76,    32,     0,    76,
    77,     0,     0,    29,    18,    40,    18,    30,    39,    20,
    33,     0,    29,    78,    30,     0,    78,    47,    78,     0,
    78,    48,    78,     0,    78,    49,    78,     0,    78,    51,
     0,    78,    52,     0,    78,    53,     0,    50,    29,    78,
    30,     0,    18,     0,    43,    18,    43,     0,    86,    35,
    13,     0,    86,    12,    13,     0,    81,     0,    80,    39,
    81,     0,    80,    47,    81,     0,    18,     0,    83,     0,
    82,    83,     0,    18,    29,    85,    30,    37,    90,    38,
     0,     6,     5,    27,    18,    29,    85,    30,    37,    90,
    38,     0,    14,    15,    18,     0,    14,    18,     0,    17,
    18,     0,    84,     0,    85,    40,    84,     0,    18,     0,
    18,    39,    80,     0,    18,    47,    80,     0,    86,    34,
    91,    33,     0,    18,    29,    88,    30,    33,     0,    44,
    33,     0,    86,    34,    28,    14,    33,     0,    86,    34,
    28,    14,    29,    30,    33,     0,    86,     0,    91,    40,
    86,     0,     8,    29,    79,    30,    37,    90,    38,     9,
    37,    90,    38,     0,     8,    29,    79,    30,    37,    90,
    38,     0,    87,     0,     0,    90,    89,     0,    86,     0,
    91,    36,    86,     0,     0,    92,    93,     0,    92,    94,
     0,    37,    46,    69,    70,    71,    95,    38,     0,    37,
    45,    69,    70,    71,    95,    38,     0,    24,    31,    96,
    32,     0,    96,    97,     0,    97,     0,    31,    98,    32,
     0,    98,    99,     0,     0,    18,    39,    20,    33,     0
};

#endif

#if YYDEBUG != 0
static const short yyrline[] = { 0,
    95,   105,   123,   124,   127,   128,   129,   130,   131,   138,
   140,   144,   149,   151,   155,   156,   159,   163,   168,   171,
   193,   252,   260,   268,   276,   287,   296,   304,   315,   328,
   338,   354,   360,   367,   375,   379,   385,   392,   396,   403,
   408,   412,   415,   438,   441,   449,   456,   463,   469,   476,
   483,   489,   500,   505,   511,   519,   525,   529,   536,   542,
   546,   552,   560,   574,   578,   583,   589,   593,   600,   613,
   626,   642,   653,   666,   674,   685,   700,   704,   710,   719,
   728,   735,   740,   747,   752,   762,   763,   764,   768,   784,
   800,   807,   811,   818,   823,   827,   830
};
#endif


#if YYDEBUG != 0 || defined (YYERROR_VERBOSE)

static const char * const yytname[] = {   "$","error","$undefined.","STRUCT",
"CLASS","STATIC","PUBLIC","TREELINK","IF","ELSE","P","Q","NOTEQUAL","ZEROPOINTER",
"TREE","POINTER","LINKER","SCALARVALUE","IDENT","FUNC","INTEGER","STATES","START",
"ACCEPT","TRANSITIONS","ALL","NONE","VOID","NEW","LBRACKET","RBRACKET","LSQRBRA",
"RSQRBRA","TERMINATOR","ASSIGN","EQUALS","OPER","LBRACE","RBRACE","ARROW","COMMA",
"COLON","COND","QUOTES","RETURN","STRUCTSLICE","CONTSLICE","CONCAT","OR","AND",
"NOT","INV","REV","STAR","program","declarestruct","decfieldlist","decfield",
"funcdeclist","funcdec","decparam","decparamlist","globallist","global","description",
"operationlist","operation","filename","dirfsa","states","startstates","acceptstates",
"numlist","dirfsatrans","dirfsatranslist","dirfsastatetrans","dirfsamovelist",
"dirfsamove","opexp","cond","termlist","term","recfunclist","recfunc","param",
"paramlist","element","instruction","callparams","statement","block","exp","proclist",
"contslice","structslice","onefsatrans","onefsatranslist","onefsastatetrans",
"onefsamovelist","onefsamove", NULL
};
#endif

static const short yyr1[] = {     0,
    54,    55,    56,    56,    57,    57,    57,    57,    57,    58,
    58,    59,    60,    60,    61,    61,    62,    62,    63,    63,
    64,    65,    65,    66,    66,    66,    66,    66,    66,    67,
    68,    69,    70,    71,    72,    72,    73,    74,    74,    75,
    76,    76,    77,    78,    78,    78,    78,    78,    78,    78,
    78,    78,    79,    79,    79,    80,    80,    80,    81,    82,
    82,    83,    83,    84,    84,    84,    85,    85,    86,    86,
    86,    87,    87,    87,    87,    87,    88,    88,    89,    89,
    89,    90,    90,    91,    91,    92,    92,    92,    93,    94,
    95,    96,    96,    97,    98,    98,    99
};

static const short yyr2[] = {     0,
     6,     6,     1,     2,     4,     3,     4,     3,     3,     1,
     2,     6,     2,     1,     1,     3,     0,     2,     3,     4,
     1,     1,     2,     5,     4,     5,     5,     5,     5,     3,
     4,     3,     3,     3,     3,     1,     4,     2,     1,     3,
     2,     0,     8,     3,     3,     3,     3,     2,     2,     2,
     4,     1,     3,     3,     3,     1,     3,     3,     1,     1,
     2,     7,    10,     3,     2,     2,     1,     3,     1,     3,
     3,     4,     5,     2,     5,     7,     1,     3,    11,     7,
     1,     0,     2,     1,     3,     0,     2,     2,     7,     7,
     4,     2,     1,     3,     2,     0,     4
};

static const short yydefact[] = {     0,
     0,    17,     0,     0,     0,     0,     0,     0,    18,     0,
    21,    22,     0,     0,     0,     0,     3,     0,     0,     0,
     0,     0,    10,    23,     0,     0,     0,     0,     0,     0,
     4,     0,    19,     0,     0,     0,     0,    11,    86,    60,
     9,     0,     0,     8,     6,     2,    20,    52,     0,     0,
     0,     0,    25,     0,     0,     0,     0,     0,     0,     0,
     0,     0,    61,     1,     5,     7,     0,    28,    29,     0,
     0,     0,    24,    26,     0,     0,    27,     0,     0,     0,
    48,    49,    50,     0,    14,    15,     0,     0,     0,     0,
    67,     0,     0,    87,    88,    32,    44,    30,     0,     0,
     0,     0,    45,    46,    47,    13,     0,     0,     0,     0,
    65,    66,     0,     0,     0,     0,    51,    33,    36,     0,
     0,    31,    12,    16,     0,    64,    82,    68,     0,     0,
    34,     0,     0,     0,     0,     0,     0,    35,    42,     0,
    39,     0,     0,    69,    62,     0,     0,    81,    83,     0,
     0,     0,    37,    38,    82,     0,     0,     0,     0,    74,
     0,     0,     0,     0,     0,    40,    41,     0,    69,     0,
     0,     0,    84,     0,     0,    59,    70,    56,    71,     0,
    84,     0,     0,    90,    89,     0,    63,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,    72,    96,     0,
    93,     0,    53,    82,    55,    54,    73,    85,    78,    57,
    58,     0,    75,     0,    91,    92,     0,     0,     0,     0,
    94,    95,     0,    80,    76,     0,     0,     0,     0,     0,
    82,    97,    43,     0,    79,     0,     0,     0
};

static const short yydefgoto[] = {   236,
     2,    16,    17,    22,    23,    86,    87,     4,     9,    10,
    11,    12,    56,    57,    58,    76,   102,   120,   122,   140,
   141,   152,   167,    59,   171,   177,   178,    39,    40,    91,
    92,   147,   148,   174,   149,   135,   175,    64,    94,    95,
   163,   200,   201,   214,   222
};

static const short yypact[] = {    43,
    45,-32768,   -23,     8,    53,    88,    93,   109,-32768,   110,
-32768,    97,   128,   100,   129,    10,-32768,   130,   116,   111,
   132,    46,-32768,-32768,   118,   135,   136,   122,   123,   124,
-32768,   125,-32768,    25,   131,   154,   133,-32768,    16,-32768,
-32768,   134,   137,-32768,-32768,-32768,-32768,-32768,   141,   126,
   127,     3,-32768,   145,   139,   138,   140,   144,    55,   103,
   142,   105,-32768,   143,-32768,-32768,   146,-32768,-32768,    39,
   147,     3,-32768,-32768,   151,   149,-32768,     3,     3,     3,
-32768,-32768,-32768,   158,-32768,-32768,    31,   156,   106,   157,
-32768,    32,    87,-32768,-32768,-32768,-32768,-32768,    48,   148,
   162,   153,    61,    78,    78,-32768,   150,   103,   155,   167,
-32768,-32768,   152,   105,   165,   165,-32768,-32768,-32768,   -21,
   160,-32768,-32768,-32768,   105,-32768,-32768,-32768,   144,   144,
-32768,   168,   161,    49,    -7,   149,   149,-32768,-32768,   104,
-32768,   159,   164,   -19,-32768,   166,   163,-32768,-32768,   170,
   170,    94,-32768,-32768,-32768,   -14,   169,   177,   177,-32768,
    37,   171,   172,   173,   180,-32768,-32768,    -3,    19,   182,
   174,    21,   175,   176,   -10,-32768,    35,-32768,    35,   187,
-32768,    92,   178,-32768,-32768,   179,-32768,   181,   183,   190,
   194,   184,   169,   169,   177,   177,    65,-32768,-32768,   107,
-32768,   195,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,
-32768,   185,-32768,    -9,-32768,-32768,   186,    -2,   188,   189,
-32768,-32768,   191,   199,-32768,   192,   198,   196,   193,   201,
-32768,-32768,-32768,     0,-32768,   214,   222,-32768
};

static const short yypgoto[] = {-32768,
-32768,-32768,   207,-32768,   203,   119,-32768,-32768,-32768,-32768,
   217,-32768,-32768,-32768,   -35,    11,     6,-32768,-32768,-32768,
    91,-32768,-32768,     5,-32768,    73,   -51,-32768,   197,   121,
   112,  -154,-32768,-32768,-32768,  -155,    77,-32768,-32768,-32768,
    89,-32768,    41,-32768,-32768
};


#define	YYLAST		241


static const short yytable[] = {   168,
   143,   172,   173,   169,   143,   143,   181,   143,   220,   157,
   144,   131,     6,     5,   144,   144,    13,   144,   132,   158,
    48,    36,   221,    14,     7,   193,    15,   159,   170,   194,
   145,    52,   190,    37,   187,   224,   146,   235,   208,   209,
   146,   146,    48,   146,     8,    49,     1,    30,   218,    50,
    51,    36,    55,    52,   169,   191,    70,   158,     3,    13,
   107,   113,    53,    37,   180,   159,    14,    54,    97,    15,
   108,   114,    21,   195,    55,   234,    99,   117,   142,   129,
   130,   196,   103,   104,   105,    78,    79,    80,   114,    81,
    82,    83,    77,   212,    78,    79,    80,   213,    81,    82,
    83,    78,    79,    80,    18,    81,    82,    83,    79,    80,
    19,    81,    82,    83,    26,    27,    84,    28,    89,    85,
   110,    90,   165,   111,   198,   166,    20,   193,    81,    82,
    83,   115,   116,     8,   139,   153,    21,   199,   215,   136,
   137,   150,   151,   210,   211,    25,    29,    32,    33,    35,
    41,    34,    42,    43,    44,    45,    46,    47,    61,    60,
    67,    62,    71,    68,    69,    75,    65,    72,    88,    66,
   100,   101,   106,   109,   112,    73,   121,    74,    96,    93,
   118,   119,   123,   125,   126,    49,   169,   138,   127,    98,
   133,   139,   156,   162,   176,   155,   161,   186,   160,   188,
   197,   183,   205,   189,   -77,   192,   206,   228,   199,   184,
   185,   229,   217,   237,   219,   223,   207,   230,   202,   204,
   225,   238,    31,   203,    38,   232,   124,   226,    24,   227,
   154,   179,   231,   233,   128,    63,   134,   182,     0,   164,
   216
};

static const short yycheck[] = {   155,
     8,   156,   157,    18,     8,     8,   161,     8,    18,    29,
    18,    33,     5,    37,    18,    18,     7,    18,    40,    39,
    18,     6,    32,    14,    17,    36,    17,    47,    43,    40,
    38,    29,    12,    18,    38,    38,    44,    38,   193,   194,
    44,    44,    18,    44,    37,    21,     4,    38,   204,    25,
    26,     6,    50,    29,    18,    35,    52,    39,    14,     7,
    30,    30,    38,    18,    28,    47,    14,    43,    30,    17,
    40,    40,    27,    39,    50,   231,    72,    30,    30,   115,
   116,    47,    78,    79,    80,    47,    48,    49,    40,    51,
    52,    53,    38,    29,    47,    48,    49,    33,    51,    52,
    53,    47,    48,    49,    17,    51,    52,    53,    48,    49,
    18,    51,    52,    53,    15,    16,    14,    18,    14,    17,
    15,    17,    29,    18,    33,    32,    18,    36,    51,    52,
    53,    45,    46,    37,    31,    32,    27,    31,    32,   129,
   130,   136,   137,   195,   196,    18,    18,    18,    33,    18,
    33,    41,    18,    18,    33,    33,    33,    33,     5,    29,
    20,    29,    18,    38,    38,    22,    33,    29,    27,    33,
    20,    23,    15,    18,    18,    38,    24,    38,    33,    37,
    33,    20,    33,    29,    18,    21,    18,    20,    37,    43,
    31,    31,    29,    24,    18,    37,    34,    18,    33,    18,
    14,    31,    13,    30,    30,    30,    13,     9,    31,    38,
    38,    20,    18,     0,    30,    30,    33,    20,    40,    37,
    33,     0,    16,    43,    22,    33,   108,    39,    12,    39,
   140,   159,    37,    33,   114,    39,   125,   161,    -1,   151,
   200
};
/* -*-C-*-  Note some compilers choke on comments on `#line' lines.  */
#line 3 "/usr/lib/bison.simple"
/* This file comes from bison-1.28.  */

/* Skeleton output parser for bison,
   Copyright (C) 1984, 1989, 1990 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* This is the parser code that is written into each bison parser
  when the %semantic_parser declaration is not specified in the grammar.
  It was written by Richard Stallman by simplifying the hairy parser
  used when %semantic_parser is specified.  */

#ifndef YYSTACK_USE_ALLOCA
#ifdef alloca
#define YYSTACK_USE_ALLOCA
#else /* alloca not defined */
#ifdef __GNUC__
#define YYSTACK_USE_ALLOCA
#define alloca __builtin_alloca
#else /* not GNU C.  */
#if (!defined (__STDC__) && defined (sparc)) || defined (__sparc__) || defined (__sparc) || defined (__sgi) || (defined (__sun) && defined (__i386))
#define YYSTACK_USE_ALLOCA
#include <alloca.h>
#else /* not sparc */
/* We think this test detects Watcom and Microsoft C.  */
/* This used to test MSDOS, but that is a bad idea
   since that symbol is in the user namespace.  */
#if (defined (_MSDOS) || defined (_MSDOS_)) && !defined (__TURBOC__)
#if 0 /* No need for malloc.h, which pollutes the namespace;
	 instead, just don't use alloca.  */
#include <malloc.h>
#endif
#else /* not MSDOS, or __TURBOC__ */
#if defined(_AIX)
/* I don't know what this was needed for, but it pollutes the namespace.
   So I turned it off.   rms, 2 May 1997.  */
/* #include <malloc.h>  */
 #pragma alloca
#define YYSTACK_USE_ALLOCA
#else /* not MSDOS, or __TURBOC__, or _AIX */
#if 0
#ifdef __hpux /* haible@ilog.fr says this works for HPUX 9.05 and up,
		 and on HPUX 10.  Eventually we can turn this on.  */
#define YYSTACK_USE_ALLOCA
#define alloca __builtin_alloca
#endif /* __hpux */
#endif
#endif /* not _AIX */
#endif /* not MSDOS, or __TURBOC__ */
#endif /* not sparc */
#endif /* not GNU C */
#endif /* alloca not defined */
#endif /* YYSTACK_USE_ALLOCA not defined */

#ifdef YYSTACK_USE_ALLOCA
#define YYSTACK_ALLOC alloca
#else
#define YYSTACK_ALLOC malloc
#endif

/* Note: there must be only one dollar sign in this file.
   It is replaced by the list of actions, each action
   as one case of the switch.  */

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		-2
#define YYEOF		0
#define YYACCEPT	goto yyacceptlab
#define YYABORT 	goto yyabortlab
#define YYERROR		goto yyerrlab1
/* Like YYERROR except do call yyerror.
   This remains here temporarily to ease the
   transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */
#define YYFAIL		goto yyerrlab
#define YYRECOVERING()  (!!yyerrstatus)
#define YYBACKUP(token, value) \
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    { yychar = (token), yylval = (value);			\
      yychar1 = YYTRANSLATE (yychar);				\
      YYPOPSTACK;						\
      goto yybackup;						\
    }								\
  else								\
    { yyerror ("syntax error: cannot back up"); YYERROR; }	\
while (0)

#define YYTERROR	1
#define YYERRCODE	256

#ifndef YYPURE
#define YYLEX		yylex()
#endif

#ifdef YYPURE
#ifdef YYLSP_NEEDED
#ifdef YYLEX_PARAM
#define YYLEX		yylex(&yylval, &yylloc, YYLEX_PARAM)
#else
#define YYLEX		yylex(&yylval, &yylloc)
#endif
#else /* not YYLSP_NEEDED */
#ifdef YYLEX_PARAM
#define YYLEX		yylex(&yylval, YYLEX_PARAM)
#else
#define YYLEX		yylex(&yylval)
#endif
#endif /* not YYLSP_NEEDED */
#endif

/* If nonreentrant, generate the variables here */

#ifndef YYPURE

int	yychar;			/*  the lookahead symbol		*/
YYSTYPE	yylval;			/*  the semantic value of the		*/
				/*  lookahead symbol			*/

#ifdef YYLSP_NEEDED
YYLTYPE yylloc;			/*  location data for the lookahead	*/
				/*  symbol				*/
#endif

int yynerrs;			/*  number of parse errors so far       */
#endif  /* not YYPURE */

#if YYDEBUG != 0
int yydebug;			/*  nonzero means print parse trace	*/
/* Since this is uninitialized, it does not stop multiple parsers
   from coexisting.  */
#endif

/*  YYINITDEPTH indicates the initial size of the parser's stacks	*/

#ifndef	YYINITDEPTH
#define YYINITDEPTH 200
#endif

/*  YYMAXDEPTH is the maximum size the stacks can grow to
    (effective only if the built-in stack extension method is used).  */

#if YYMAXDEPTH == 0
#undef YYMAXDEPTH
#endif

#ifndef YYMAXDEPTH
#define YYMAXDEPTH 10000
#endif

/* Define __yy_memcpy.  Note that the size argument
   should be passed with type unsigned int, because that is what the non-GCC
   definitions require.  With GCC, __builtin_memcpy takes an arg
   of type size_t, but it can handle unsigned int.  */

#if __GNUC__ > 1		/* GNU C and GNU C++ define this.  */
#define __yy_memcpy(TO,FROM,COUNT)	__builtin_memcpy(TO,FROM,COUNT)
#else				/* not GNU C or C++ */
#ifndef __cplusplus

/* This is the most reliable way to avoid incompatibilities
   in available built-in functions on various systems.  */
static void
__yy_memcpy (to, from, count)
     char *to;
     char *from;
     unsigned int count;
{
  register char *f = from;
  register char *t = to;
  register int i = count;

  while (i-- > 0)
    *t++ = *f++;
}

#else /* __cplusplus */

/* This is the most reliable way to avoid incompatibilities
   in available built-in functions on various systems.  */
static void
__yy_memcpy (char *to, char *from, unsigned int count)
{
  register char *t = to;
  register char *f = from;
  register int i = count;

  while (i-- > 0)
    *t++ = *f++;
}

#endif
#endif

#line 217 "/usr/lib/bison.simple"

/* The user can define YYPARSE_PARAM as the name of an argument to be passed
   into yyparse.  The argument should have type void *.
   It should actually point to an object.
   Grammar actions can access the variable by casting it
   to the proper pointer type.  */

#ifdef YYPARSE_PARAM
#ifdef __cplusplus
#define YYPARSE_PARAM_ARG void *YYPARSE_PARAM
#define YYPARSE_PARAM_DECL
#else /* not __cplusplus */
#define YYPARSE_PARAM_ARG YYPARSE_PARAM
#define YYPARSE_PARAM_DECL void *YYPARSE_PARAM;
#endif /* not __cplusplus */
#else /* not YYPARSE_PARAM */
#define YYPARSE_PARAM_ARG
#define YYPARSE_PARAM_DECL
#endif /* not YYPARSE_PARAM */

/* Prevent warning if -Wstrict-prototypes.  */
#ifdef __GNUC__
#ifdef YYPARSE_PARAM
int yyparse (void *);
#else
int yyparse (void);
#endif
#endif

int
yyparse(YYPARSE_PARAM_ARG)
     YYPARSE_PARAM_DECL
{
  register int yystate;
  register int yyn;
  register short *yyssp;
  register YYSTYPE *yyvsp;
  int yyerrstatus;	/*  number of tokens to shift before error messages enabled */
  int yychar1 = 0;		/*  lookahead token as an internal (translated) token number */

  short	yyssa[YYINITDEPTH];	/*  the state stack			*/
  YYSTYPE yyvsa[YYINITDEPTH];	/*  the semantic value stack		*/

  short *yyss = yyssa;		/*  refer to the stacks thru separate pointers */
  YYSTYPE *yyvs = yyvsa;	/*  to allow yyoverflow to reallocate them elsewhere */

#ifdef YYLSP_NEEDED
  YYLTYPE yylsa[YYINITDEPTH];	/*  the location stack			*/
  YYLTYPE *yyls = yylsa;
  YYLTYPE *yylsp;

#define YYPOPSTACK   (yyvsp--, yyssp--, yylsp--)
#else
#define YYPOPSTACK   (yyvsp--, yyssp--)
#endif

  int yystacksize = YYINITDEPTH;
  int yyfree_stacks = 0;

#ifdef YYPURE
  int yychar;
  YYSTYPE yylval;
  int yynerrs;
#ifdef YYLSP_NEEDED
  YYLTYPE yylloc;
#endif
#endif

  YYSTYPE yyval;		/*  the variable used to return		*/
				/*  semantic values from the action	*/
				/*  routines				*/

  int yylen;

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Starting parse\n");
#endif

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss - 1;
  yyvsp = yyvs;
#ifdef YYLSP_NEEDED
  yylsp = yyls;
#endif

/* Push a new state, which is found in  yystate  .  */
/* In all cases, when you get here, the value and location stacks
   have just been pushed. so pushing a state here evens the stacks.  */
yynewstate:

  *++yyssp = yystate;

  if (yyssp >= yyss + yystacksize - 1)
    {
      /* Give user a chance to reallocate the stack */
      /* Use copies of these so that the &'s don't force the real ones into memory. */
      YYSTYPE *yyvs1 = yyvs;
      short *yyss1 = yyss;
#ifdef YYLSP_NEEDED
      YYLTYPE *yyls1 = yyls;
#endif

      /* Get the current used size of the three stacks, in elements.  */
      int size = yyssp - yyss + 1;

#ifdef yyoverflow
      /* Each stack pointer address is followed by the size of
	 the data in use in that stack, in bytes.  */
#ifdef YYLSP_NEEDED
      /* This used to be a conditional around just the two extra args,
	 but that might be undefined if yyoverflow is a macro.  */
      yyoverflow("parser stack overflow",
		 &yyss1, size * sizeof (*yyssp),
		 &yyvs1, size * sizeof (*yyvsp),
		 &yyls1, size * sizeof (*yylsp),
		 &yystacksize);
#else
      yyoverflow("parser stack overflow",
		 &yyss1, size * sizeof (*yyssp),
		 &yyvs1, size * sizeof (*yyvsp),
		 &yystacksize);
#endif

      yyss = yyss1; yyvs = yyvs1;
#ifdef YYLSP_NEEDED
      yyls = yyls1;
#endif
#else /* no yyoverflow */
      /* Extend the stack our own way.  */
      if (yystacksize >= YYMAXDEPTH)
	{
	  yyerror("parser stack overflow");
	  if (yyfree_stacks)
	    {
	      free (yyss);
	      free (yyvs);
#ifdef YYLSP_NEEDED
	      free (yyls);
#endif
	    }
	  return 2;
	}
      yystacksize *= 2;
      if (yystacksize > YYMAXDEPTH)
	yystacksize = YYMAXDEPTH;
#ifndef YYSTACK_USE_ALLOCA
      yyfree_stacks = 1;
#endif
      yyss = (short *) YYSTACK_ALLOC (yystacksize * sizeof (*yyssp));
      __yy_memcpy ((char *)yyss, (char *)yyss1,
		   size * (unsigned int) sizeof (*yyssp));
      yyvs = (YYSTYPE *) YYSTACK_ALLOC (yystacksize * sizeof (*yyvsp));
      __yy_memcpy ((char *)yyvs, (char *)yyvs1,
		   size * (unsigned int) sizeof (*yyvsp));
#ifdef YYLSP_NEEDED
      yyls = (YYLTYPE *) YYSTACK_ALLOC (yystacksize * sizeof (*yylsp));
      __yy_memcpy ((char *)yyls, (char *)yyls1,
		   size * (unsigned int) sizeof (*yylsp));
#endif
#endif /* no yyoverflow */

      yyssp = yyss + size - 1;
      yyvsp = yyvs + size - 1;
#ifdef YYLSP_NEEDED
      yylsp = yyls + size - 1;
#endif

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Stack size increased to %d\n", yystacksize);
#endif

      if (yyssp >= yyss + yystacksize - 1)
	YYABORT;
    }

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Entering state %d\n", yystate);
#endif

  goto yybackup;
 yybackup:

/* Do appropriate processing given the current state.  */
/* Read a lookahead token if we need one and don't already have one.  */
/* yyresume: */

  /* First try to decide what to do without reference to lookahead token.  */

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* yychar is either YYEMPTY or YYEOF
     or a valid token in external form.  */

  if (yychar == YYEMPTY)
    {
#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Reading a token: ");
#endif
      yychar = YYLEX;
    }

  /* Convert token to internal form (in yychar1) for indexing tables with */

  if (yychar <= 0)		/* This means end of input. */
    {
      yychar1 = 0;
      yychar = YYEOF;		/* Don't call YYLEX any more */

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Now at end of input.\n");
#endif
    }
  else
    {
      yychar1 = YYTRANSLATE(yychar);

#if YYDEBUG != 0
      if (yydebug)
	{
	  fprintf (stderr, "Next token is %d (%s", yychar, yytname[yychar1]);
	  /* Give the individual parser a way to print the precise meaning
	     of a token, for further debugging info.  */
#ifdef YYPRINT
	  YYPRINT (stderr, yychar, yylval);
#endif
	  fprintf (stderr, ")\n");
	}
#endif
    }

  yyn += yychar1;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != yychar1)
    goto yydefault;

  yyn = yytable[yyn];

  /* yyn is what to do for this token type in this state.
     Negative => reduce, -yyn is rule number.
     Positive => shift, yyn is new state.
       New state is final state => don't bother to shift,
       just return success.
     0, or most negative number => error.  */

  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrlab;

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Shift the lookahead token.  */

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Shifting token %d (%s), ", yychar, yytname[yychar1]);
#endif

  /* Discard the token being shifted unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  *++yyvsp = yylval;
#ifdef YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  /* count tokens shifted since error; after three, turn off error status.  */
  if (yyerrstatus) yyerrstatus--;

  yystate = yyn;
  goto yynewstate;

/* Do the default action for the current state.  */
yydefault:

  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;

/* Do a reduction.  yyn is the number of a rule to reduce with.  */
yyreduce:
  yylen = yyr2[yyn];
  if (yylen > 0)
    yyval = yyvsp[1-yylen]; /* implement default value of the action */

#if YYDEBUG != 0
  if (yydebug)
    {
      int i;

      fprintf (stderr, "Reducing via rule %d (line %d), ",
	       yyn, yyrline[yyn]);

      /* Print the symbols being reduced, and their result.  */
      for (i = yyprhs[yyn]; yyrhs[i] > 0; i++)
	fprintf (stderr, "%s ", yytname[yyrhs[i]]);
      fprintf (stderr, " -> %s\n", yytname[yyr1[yyn]]);
    }
#endif


  switch (yyn) {

case 1:
#line 96 "rec.y"
{
funcs.SetFuncs(yyvsp[-1].flist);;
    break;}
case 2:
#line 105 "rec.y"
{
  Dir t;
  int i,dirnum;
  DirFsa *gen;
  cout << "Found structure declaration" << endl;
  //Add generator directions to list
  dirnum = t.alphabet.DirNum();
  for(i=1;i<=dirnum;i++) {
    if ((t.alphabet.Type(i)==GEN) ||(t.alphabet.Type(i)==SCALAR)){
      gen = new DirFsa(i);
      gen->SetDir((Dir)i);
      des.AddOp(gen);
    }
  }
  des.Davinci("gens");
;
    break;}
case 5:
#line 127 "rec.y"
{  d.alphabet.GenAdd(yyvsp[-1].str);;
    break;}
case 6:
#line 128 "rec.y"
{  d.alphabet.ScalarAdd(yyvsp[-1].str);;
    break;}
case 7:
#line 129 "rec.y"
{  d.alphabet.LinkAdd(yyvsp[-1].str);;
    break;}
case 8:
#line 130 "rec.y"
{  d.alphabet.GenAdd(yyvsp[-1].str);;
    break;}
case 9:
#line 131 "rec.y"
{  d.alphabet.LinkAdd(yyvsp[-1].str);;
    break;}
case 12:
#line 144 "rec.y"
{
  functable.Add(yyvsp[-4].str);
;
    break;}
case 13:
#line 149 "rec.y"
{
;
    break;}
case 14:
#line 151 "rec.y"
{
;
    break;}
case 15:
#line 155 "rec.y"
{;
    break;}
case 16:
#line 156 "rec.y"
{;
    break;}
case 17:
#line 159 "rec.y"
{
  //Now that all directions and globals have been added, sort out the epsilon direction
  d.alphabet.EpsilonAdd();
;
    break;}
case 18:
#line 163 "rec.y"
{;
    break;}
case 19:
#line 168 "rec.y"
{
  d.alphabet.GlobalAdd(yyvsp[-1].str); //Add a global direction 
;
    break;}
case 20:
#line 172 "rec.y"
{
  d.alphabet.GlobalAdd(yyvsp[-1].str); //Add a global direction 
;
    break;}
case 21:
#line 194 "rec.y"
{

  //cout << "Found description" << endl << des << endl;
;
    break;}
case 22:
#line 253 "rec.y"
{
  //cout << "Found last in operationlist" << endl;
  //$$ = new Desc();
  //$$->AddOp(*$1);
  //delete $1;
  
;
    break;}
case 23:
#line 260 "rec.y"
{
  //cout << "Found element in operationlist" << endl;
  //$$ = $2;
  //$$->AddOp(*$1);
  //delete $1;    //Should be able to delete here, but it doesn't work
;
    break;}
case 24:
#line 269 "rec.y"
{

  cout << "Found GAP format description for " << yyvsp[-3].str << endl;
  yyvsp[-1].o->SetDir((Dir)(d.alphabet.DirLookup(yyvsp[-3].str)));
  des.AddOp(yyvsp[-1].o);
  //delete $4;
;
    break;}
case 25:
#line 277 "rec.y"
{
  //For a generating direction
  cout << "Found (obsolete) generating description for " << yyvsp[-2].str << endl;
  /*
  DirFsa *gen;
  gen = new DirFsa(d.alphabet.DirLookup($2));
  gen->SetDir((Dir)(d.alphabet.DirLookup($2)));
  des.AddOp(gen);
  */
;
    break;}
case 26:
#line 288 "rec.y"
{
  //AGM format FSA
  cout << "Found AGM format description for " << yyvsp[-3].str << endl;

  yyvsp[-1].o->SetDir((Dir)(d.alphabet.DirLookup(yyvsp[-3].str)));
  //$4->Davinci("current.daVinci");
  des.AddOp(yyvsp[-1].o);
;
    break;}
case 27:
#line 297 "rec.y"
{
  //Operation expression
  cout << "Found operation expression for " << yyvsp[-3].str << endl;
  yyvsp[-1].o->SetDir((Dir)(d.alphabet.DirLookup(yyvsp[-3].str)));
  des.AddOp(yyvsp[-1].o);
  //delete $4;
;
    break;}
case 28:
#line 305 "rec.y"
{
  //Operation expression
  cout << "Found operation expression for " << yyvsp[-3].str << endl;
  DirFsa *op;
  op = new DirFsa();
  op->MakeAll();
  op->SetDir((Dir)(d.alphabet.DirLookup(yyvsp[-3].str)));
  des.AddOp(op);
  //delete op;
;
    break;}
case 29:
#line 316 "rec.y"
{
  //Operation expression
  cout << "Found operation expression for " << yyvsp[-3].str << endl;
  DirFsa *op;
  op = new DirFsa();
  op->MakeNone();
  op->SetDir((Dir)(d.alphabet.DirLookup(yyvsp[-3].str)));
  des.AddOp(op);
  //delete op;
;
    break;}
case 30:
#line 328 "rec.y"
{
  yyval.o = new DirFsa(yyvsp[-1].str);
  //cout << *$$ << endl;
  //cout << "Found FSA filename" << endl;
;
    break;}
case 31:
#line 338 "rec.y"
{

  yyval.o = new DirFsa();
  yyval.o->SetStates(yyvsp[-3].i);
  yyval.o->SetInitial(yyvsp[-2].i);
  yyval.o->SetAccept(yyvsp[-1].intlist);
  yyval.o->MakeAlphabet();
  yyval.o->AllocateTransTable();
  yyval.o->mult->flags[DFA] = true;
  yyval.o->AddTrans(yyvsp[0].dirtrans);
  //cout << "Found number of states" << endl;

;
    break;}
case 32:
#line 354 "rec.y"
{
  yyval.i = yyvsp[-1].ival;
;
    break;}
case 33:
#line 360 "rec.y"
{

  yyval.i = yyvsp[-1].ival;
  //cout << "Found initial state" << endl;
;
    break;}
case 34:
#line 367 "rec.y"
{

  agmstate = 1; //Reset statenum
  yyval.intlist = yyvsp[-1].intlist;
  //cout << "Found accept states" << endl;
;
    break;}
case 35:
#line 375 "rec.y"
{
  yyval.intlist=yyvsp[-2].intlist;
  yyval.intlist->append(yyvsp[0].ival);
;
    break;}
case 36:
#line 379 "rec.y"
{
  yyval.intlist = new DLList<int>;
  yyval.intlist->append(yyvsp[0].ival);
;
    break;}
case 37:
#line 385 "rec.y"
{

  yyval.dirtrans = yyvsp[-1].dirtrans;
  //cout << "Found transitions" << endl;
;
    break;}
case 38:
#line 392 "rec.y"
{
  yyval.dirtrans = yyvsp[-1].dirtrans;
  yyval.dirtrans->append(yyvsp[0].twotranslist);
;
    break;}
case 39:
#line 396 "rec.y"
{
  yyval.dirtrans = new DLList<TwoTransList*>;
  yyval.dirtrans->append(yyvsp[0].twotranslist);
;
    break;}
case 40:
#line 403 "rec.y"
{
  yyval.twotranslist = yyvsp[-1].twotranslist;
;
    break;}
case 41:
#line 408 "rec.y"
{
  yyvsp[-1].twotranslist->append(yyvsp[0].twotrans);
  yyval.twotranslist = yyvsp[-1].twotranslist;
;
    break;}
case 42:
#line 412 "rec.y"
{yyval.twotranslist = new TwoTransList;;
    break;}
case 43:
#line 415 "rec.y"
{

  //Add transition to transition list
  int a,b;
  yyval.twotrans = new TwoTrans();

  a = d.alphabet.DirLookup(yyvsp[-6].str);
  b = d.alphabet.DirLookup(yyvsp[-4].str);

  yyval.twotrans->to = yyvsp[-1].ival;
  yyval.twotrans->a = a;
  yyval.twotrans->b = b;

  //cout << "Transition State:" << statenum << ":" << x << "," << y 
  //   << " -> " << tostate << endl;
  //cout << "Letter " << letter << endl;
;
    break;}
case 44:
#line 438 "rec.y"
{
  yyval.o = yyvsp[-1].o;
;
    break;}
case 45:
#line 442 "rec.y"
{
  
  yyval.o= new DirFsa();
  yyval.o->Combine(yyvsp[-2].o,yyvsp[0].o);
  delete yyvsp[-2].o;
  delete yyvsp[0].o;
;
    break;}
case 46:
#line 450 "rec.y"
{
  yyval.o = new DirFsa(yyvsp[-2].o);
  yyval.o->Or(*yyvsp[0].o);
  delete yyvsp[-2].o;
  delete yyvsp[0].o;
;
    break;}
case 47:
#line 457 "rec.y"
{
  yyval.o = new DirFsa(yyvsp[-2].o);
  yyval.o->And(yyvsp[0].o);
  delete yyvsp[-2].o;
  delete yyvsp[0].o;
;
    break;}
case 48:
#line 464 "rec.y"
{
  yyval.o = new DirFsa(yyvsp[-1].o);
  yyval.o->Invert();
  delete yyvsp[-1].o;
;
    break;}
case 49:
#line 470 "rec.y"
{
  yyval.o = new DirFsa(yyvsp[-1].o);
  yyval.o->Reverse();
  delete yyvsp[-1].o;
  //$$->Davinci("rev.daVinci");
;
    break;}
case 50:
#line 477 "rec.y"
{

  ClosureMaker close(yyvsp[-1].o);
  yyval.o = close.Make();
  delete yyvsp[-1].o;
;
    break;}
case 51:
#line 484 "rec.y"
{
  yyval.o = new DirFsa(yyvsp[-1].o);
  yyval.o->Not();
  delete yyvsp[-1].o;
;
    break;}
case 52:
#line 490 "rec.y"
{
  Path f;
  f.append( (Dir)( d.alphabet.DirLookup(yyvsp[0].str) ) );
  yyval.o = new DirFsa(des.NewFindOp(f));
;
    break;}
case 53:
#line 500 "rec.y"
{

  yyval.pred = new Pred();
  yyval.pred->type = UNKNOWN;
;
    break;}
case 54:
#line 506 "rec.y"
{  //Allow w==NULL style conditionals
  yyval.pred = new Pred();
  yyval.pred->type = ISNULL;
  yyval.pred->elem = yyvsp[-2].e;
;
    break;}
case 55:
#line 512 "rec.y"
{  //Allow w!=NULL style conditionals
  yyval.pred = new Pred();
  yyval.pred->type = NOTNULL;
  yyval.pred->elem = yyvsp[-2].e;
;
    break;}
case 56:
#line 520 "rec.y"
{
  Path *n = new Path();
  n->append(*yyvsp[0].t);
  delete yyvsp[0].t;
  yyval.f = n;;
    break;}
case 57:
#line 525 "rec.y"
{
  yyvsp[-2].f->append(*yyvsp[0].t);
  delete yyvsp[0].t;
  yyval.f = yyvsp[-2].f;;
    break;}
case 58:
#line 529 "rec.y"
{
  yyvsp[-2].f->append(*yyvsp[0].t);
  delete yyvsp[0].t;
  yyval.f = yyvsp[-2].f;;
    break;}
case 59:
#line 536 "rec.y"
{
  yyval.t = new Dir(d.alphabet.DirLookup(yyvsp[0].str));
;
    break;}
case 60:
#line 542 "rec.y"
{
  yyval.flist = new DLList<Function*>;
  yyval.flist->append(yyvsp[0].func);
;
    break;}
case 61:
#line 546 "rec.y"
{
  yyvsp[-1].flist->append(yyvsp[0].func);
  yyval.flist=yyvsp[-1].flist;
;
    break;}
case 62:
#line 553 "rec.y"
{
  cout << "Parsed function " << yyvsp[-6].str << "()" << endl;
  yyval.func = new Function;
  yyval.func->num = functable.Lookup(yyvsp[-6].str);
  yyval.func->paramlist = yyvsp[-4].intlist;
  yyval.func->body = yyvsp[-1].block;
;
    break;}
case 63:
#line 562 "rec.y"
{
  cout << "Parsed function " << yyvsp[-6].str << "()" << endl;
  yyval.func = new Function;
  //If not already in table
  if (functable.Lookup(yyvsp[-6].str)==0) functable.Add(yyvsp[-6].str);
  yyval.func->num = functable.Lookup(yyvsp[-6].str);
  yyval.func->paramlist = yyvsp[-4].intlist;
  yyval.func->body = yyvsp[-1].block;
;
    break;}
case 64:
#line 574 "rec.y"
{
  yyval.ival = indvars.Add(yyvsp[0].str);           //Add induction variable to table
  //  cout << "Added variable " << $3 << endl;
;
    break;}
case 65:
#line 579 "rec.y"
{
  yyval.ival = indvars.Add(yyvsp[0].str);           //Add induction variable to table
  //  cout << "Added variable " << $3 << endl;
;
    break;}
case 66:
#line 584 "rec.y"
{
  yyval.ival = 0;   //Value to indicate parameter is ignored
;
    break;}
case 67:
#line 589 "rec.y"
{
  yyval.intlist = new DLList<int>;
  yyval.intlist->append(yyvsp[0].ival);
;
    break;}
case 68:
#line 593 "rec.y"
{
  yyvsp[-2].intlist->append(yyvsp[0].ival);
  yyval.intlist=yyvsp[-2].intlist;
;
    break;}
case 69:
#line 601 "rec.y"
{//Create empty path
  yyval.e=new Elem;
  int v = indvars.Lookup(yyvsp[0].str);
  if (v>0) {
  yyval.e->ind = v;
  }
  else {  //It is a global direction
    v = d.alphabet.DirLookup(yyvsp[0].str);
    yyval.e->ind = -v;
  }
  yyval.e->word = new Path;
;
    break;}
case 70:
#line 613 "rec.y"
{
  yyval.e=new Elem;
  int v = indvars.Lookup(yyvsp[-2].str);
  //Path *curr=$3;
  if (v>0) {
  yyval.e->ind = v;
  }
  else {  //It is a global direction
    v = d.alphabet.DirLookup(yyvsp[-2].str);
    yyval.e->ind = -v;
  }
  yyval.e->word=yyvsp[0].f;
;
    break;}
case 71:
#line 626 "rec.y"
{
  yyval.e=new Elem;
  int v = indvars.Lookup(yyvsp[-2].str);
  //Path *curr=$3;
  if (v>0) {
  yyval.e->ind = v;
  }
  else {  //It is a global direction
    v = d.alphabet.DirLookup(yyvsp[-2].str);
    yyval.e->ind = -v;
  }
  yyval.e->word=yyvsp[0].f;
;
    break;}
case 72:
#line 642 "rec.y"
{//Read/Write
  yyval.ins = new Instruction;
  yyval.ins->type = INSRW;
  yyval.ins->writevar = yyvsp[-3].e->ind;
  yyval.ins->write = yyvsp[-3].e->word;
  yyval.ins->elements = yyvsp[-1].express;
  yyval.ins->statdir = ControlAdd();
  yyval.ins->endline = lineno;
  yyval.ins->startline = startline;
  startline = lineno+1;
;
    break;}
case 73:
#line 653 "rec.y"
{//Call function
  yyval.ins = new Instruction;
  yyval.ins->type = INSCALL;
  //if function is not in table add it
  if (functable.Lookup(yyvsp[-4].str)==0) functable.Add(yyvsp[-4].str);
  yyval.ins->call = functable.Lookup(yyvsp[-4].str);
  yyval.ins->elements = yyvsp[-2].express;
  yyval.ins->statdir = ControlAdd();
  yyval.ins->endline = lineno;
  yyval.ins->startline = startline;
  startline = lineno+1;
;
    break;}
case 74:
#line 666 "rec.y"
{   //Return from function
  yyval.ins = new Instruction;
  yyval.ins->type = INSRETURN;
  yyval.ins->statdir = ControlAdd();
  yyval.ins->endline = lineno;
  yyval.ins->startline = startline;
  startline = lineno+1;
;
    break;}
case 75:
#line 675 "rec.y"
{  //Dynamic memory allocation
  yyval.ins = new Instruction;
  yyval.ins->type = INSNEW;
  yyval.ins->writevar = yyvsp[-4].e->ind;
  yyval.ins->write = yyvsp[-4].e->word;
  yyval.ins->statdir = ControlAdd();
  yyval.ins->endline = lineno;
  yyval.ins->startline = startline;
  startline = lineno+1;
;
    break;}
case 76:
#line 686 "rec.y"
{  
  yyval.ins = new Instruction;
  yyval.ins->type = INSNEW;
  yyval.ins->writevar = yyvsp[-6].e->ind;
  yyval.ins->write = yyvsp[-6].e->word;
  yyval.ins->statdir = ControlAdd();
  yyval.ins->endline = lineno;
  yyval.ins->startline = startline;
  startline = lineno+1;
;
    break;}
case 77:
#line 700 "rec.y"
{
  yyval.express = new DLList<Elem*>;
  yyval.express->append(yyvsp[0].e);
;
    break;}
case 78:
#line 704 "rec.y"
{
  yyval.express=yyvsp[-2].express;
  yyval.express->append(yyvsp[0].e);
;
    break;}
case 79:
#line 711 "rec.y"
{

  yyval.st = new Statement;
  yyval.st->type = IFSTAT;
  yyval.st->pred = yyvsp[-8].pred;
  yyval.st->ifthen = yyvsp[-5].block;
  yyval.st->ifelse = yyvsp[-1].block;
;
    break;}
case 80:
#line 720 "rec.y"
{

  yyval.st = new Statement;
  yyval.st->type = IFSTAT;
  yyval.st->pred = yyvsp[-4].pred;
  yyval.st->ifthen = yyvsp[-1].block;
  yyval.st->ifelse = NULL;
;
    break;}
case 81:
#line 728 "rec.y"
{
  yyval.st = new Statement;
  yyval.st->type = INSTRUCTSTAT;
  yyval.st->ins = yyvsp[0].ins;
;
    break;}
case 82:
#line 735 "rec.y"
{
  yyval.block = new Block;
  yyval.block->s = NULL;
  yyval.block->next = NULL;
;
    break;}
case 83:
#line 740 "rec.y"
{

  yyval.block = new Block;
  yyval.block->s = yyvsp[0].st;
  yyval.block->next = yyvsp[-1].block;
;
    break;}
case 84:
#line 747 "rec.y"
{

  yyval.express = new DLList<Elem*>;
  yyval.express->append(yyvsp[0].e);
;
    break;}
case 85:
#line 752 "rec.y"
{
  
  yyval.express = yyvsp[-2].express;
  yyval.express->append(yyvsp[0].e);
;
    break;}
case 86:
#line 762 "rec.y"
{;
    break;}
case 89:
#line 768 "rec.y"
{

  yyval.cont = new ContSlice();
  yyval.cont->SetStates(yyvsp[-4].i);
  yyval.cont->SetInitial(yyvsp[-3].i);
  yyval.cont->SetAccept(yyvsp[-2].intlist);
  yyval.cont->MakeAlphabet();
  yyval.cont->AllocateTransTable();
  yyval.cont->mult->flags[DFA] = true;
  yyval.cont->AddTrans(yyvsp[-1].onetranslistlist);

  cout << "Found control slice partition" << endl;
  contpartitions->append(yyval.cont);
;
    break;}
case 90:
#line 784 "rec.y"
{

  yyval.struc = new StructSlice();
  yyval.struc->SetStates(yyvsp[-4].i);
  yyval.struc->SetInitial(yyvsp[-3].i);
  yyval.struc->SetAccept(yyvsp[-2].intlist);
  yyval.struc->MakeAlphabet();
  yyval.struc->AllocateTransTable();
  yyval.struc->mult->flags[DFA] = true;
  yyval.struc->AddTrans(yyvsp[-1].onetranslistlist);

  cout << "Found structure slice partition" << endl;
  structpartitions->append(yyval.struc);
;
    break;}
case 91:
#line 800 "rec.y"
{

  yyval.onetranslistlist = yyvsp[-1].onetranslistlist;
  //cout << "Found transitions" << endl;
;
    break;}
case 92:
#line 807 "rec.y"
{
  yyval.onetranslistlist = yyvsp[-1].onetranslistlist;
  yyval.onetranslistlist->append(yyvsp[0].onetranslist);
;
    break;}
case 93:
#line 811 "rec.y"
{
  yyval.onetranslistlist = new DLList<OneTransList*>;
  yyval.onetranslistlist->append(yyvsp[0].onetranslist);
;
    break;}
case 94:
#line 818 "rec.y"
{
  yyval.onetranslist = yyvsp[-1].onetranslist;
;
    break;}
case 95:
#line 823 "rec.y"
{
  yyvsp[-1].onetranslist->append(yyvsp[0].onetrans);
  yyval.onetranslist = yyvsp[-1].onetranslist;
;
    break;}
case 96:
#line 827 "rec.y"
{yyval.onetranslist = new OneTransList;;
    break;}
case 97:
#line 830 "rec.y"
{

  //Add transition to transition list
  int a;
  yyval.onetrans = new OneTrans();

  a = d.alphabet.ContLookup(yyvsp[-3].str);

  if (a==0) {  //May be a direction
    a = d.alphabet.DirLookup(yyvsp[-3].str);
  }

  yyval.onetrans->to = yyvsp[-1].ival;
  yyval.onetrans->a = a;

  //cout << "Transition State:" << statenum << ":" << x << "," << y 
  //   << " -> " << tostate << endl;
  //cout << "Letter " << letter << endl;
;
    break;}
}
   /* the action file gets copied in in place of this dollarsign */
#line 543 "/usr/lib/bison.simple"

  yyvsp -= yylen;
  yyssp -= yylen;
#ifdef YYLSP_NEEDED
  yylsp -= yylen;
#endif

#if YYDEBUG != 0
  if (yydebug)
    {
      short *ssp1 = yyss - 1;
      fprintf (stderr, "state stack now");
      while (ssp1 != yyssp)
	fprintf (stderr, " %d", *++ssp1);
      fprintf (stderr, "\n");
    }
#endif

  *++yyvsp = yyval;

#ifdef YYLSP_NEEDED
  yylsp++;
  if (yylen == 0)
    {
      yylsp->first_line = yylloc.first_line;
      yylsp->first_column = yylloc.first_column;
      yylsp->last_line = (yylsp-1)->last_line;
      yylsp->last_column = (yylsp-1)->last_column;
      yylsp->text = 0;
    }
  else
    {
      yylsp->last_line = (yylsp+yylen-1)->last_line;
      yylsp->last_column = (yylsp+yylen-1)->last_column;
    }
#endif

  /* Now "shift" the result of the reduction.
     Determine what state that goes to,
     based on the state we popped back to
     and the rule number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTBASE] + *yyssp;
  if (yystate >= 0 && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTBASE];

  goto yynewstate;

yyerrlab:   /* here on detecting error */

  if (! yyerrstatus)
    /* If not already recovering from an error, report this error.  */
    {
      ++yynerrs;

#ifdef YYERROR_VERBOSE
      yyn = yypact[yystate];

      if (yyn > YYFLAG && yyn < YYLAST)
	{
	  int size = 0;
	  char *msg;
	  int x, count;

	  count = 0;
	  /* Start X at -yyn if nec to avoid negative indexes in yycheck.  */
	  for (x = (yyn < 0 ? -yyn : 0);
	       x < (sizeof(yytname) / sizeof(char *)); x++)
	    if (yycheck[x + yyn] == x)
	      size += strlen(yytname[x]) + 15, count++;
	  msg = (char *) malloc(size + 15);
	  if (msg != 0)
	    {
	      strcpy(msg, "parse error");

	      if (count < 5)
		{
		  count = 0;
		  for (x = (yyn < 0 ? -yyn : 0);
		       x < (sizeof(yytname) / sizeof(char *)); x++)
		    if (yycheck[x + yyn] == x)
		      {
			strcat(msg, count == 0 ? ", expecting `" : " or `");
			strcat(msg, yytname[x]);
			strcat(msg, "'");
			count++;
		      }
		}
	      yyerror(msg);
	      free(msg);
	    }
	  else
	    yyerror ("parse error; also virtual memory exceeded");
	}
      else
#endif /* YYERROR_VERBOSE */
	yyerror("parse error");
    }

  goto yyerrlab1;
yyerrlab1:   /* here on error raised explicitly by an action */

  if (yyerrstatus == 3)
    {
      /* if just tried and failed to reuse lookahead token after an error, discard it.  */

      /* return failure if at end of input */
      if (yychar == YYEOF)
	YYABORT;

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Discarding token %d (%s).\n", yychar, yytname[yychar1]);
#endif

      yychar = YYEMPTY;
    }

  /* Else will try to reuse lookahead token
     after shifting the error token.  */

  yyerrstatus = 3;		/* Each real token shifted decrements this */

  goto yyerrhandle;

yyerrdefault:  /* current state does not do anything special for the error token. */

#if 0
  /* This is wrong; only states that explicitly want error tokens
     should shift them.  */
  yyn = yydefact[yystate];  /* If its default is to accept any token, ok.  Otherwise pop it.*/
  if (yyn) goto yydefault;
#endif

yyerrpop:   /* pop the current state because it cannot handle the error token */

  if (yyssp == yyss) YYABORT;
  yyvsp--;
  yystate = *--yyssp;
#ifdef YYLSP_NEEDED
  yylsp--;
#endif

#if YYDEBUG != 0
  if (yydebug)
    {
      short *ssp1 = yyss - 1;
      fprintf (stderr, "Error: state stack now");
      while (ssp1 != yyssp)
	fprintf (stderr, " %d", *++ssp1);
      fprintf (stderr, "\n");
    }
#endif

yyerrhandle:

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yyerrdefault;

  yyn += YYTERROR;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != YYTERROR)
    goto yyerrdefault;

  yyn = yytable[yyn];
  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrpop;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrpop;

  if (yyn == YYFINAL)
    YYACCEPT;

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Shifting error token, ");
#endif

  *++yyvsp = yylval;
#ifdef YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  yystate = yyn;
  goto yynewstate;

 yyacceptlab:
  /* YYACCEPT comes here.  */
  if (yyfree_stacks)
    {
      free (yyss);
      free (yyvs);
#ifdef YYLSP_NEEDED
      free (yyls);
#endif
    }
  return 0;

 yyabortlab:
  /* YYABORT comes here.  */
  if (yyfree_stacks)
    {
      free (yyss);
      free (yyvs);
#ifdef YYLSP_NEEDED
      free (yyls);
#endif
    }
  return 1;
}
#line 851 "rec.y"


int
yyerror (char *s) {
  
  fprintf(stderr, "%s:%d: %s\n", file, lineno, s);
  return 0;
}
    
















