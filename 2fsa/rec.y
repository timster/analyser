// Prototype parser for FSA based descriptions
// and sets of mutually recursive functions
// With one or more parameters

%{
  
#include <string.h>
#include "dirtab.hh"
#include "stdlib.h"


#define YYDEBUG 1
  
  int yyerror (char *s) ;
  int yylex(); 
%}


/* Set up the type of the parsing information */
%union {
  int ival;
  char  *str;
  Path *f;
  Dir *t;
  DirFsa *o;
  //Cond *c;
  Pred *pred;
  Desc<DirFsa> *d;
  DLList<int> *intlist;
  Elem *e;
  Instruction *ins;
  Statement *st;
  Block *block;
  Function *func;
  DLList<Function*>  *flist;
  DLList<Elem*>   *express;
  TwoTrans * twotrans;
  TwoTransList *twotranslist;
  DLList<TwoTransList*> *dirtrans;
  ContSlice *cont;
  StructSlice *struc;
  OneTrans *onetrans;
  OneTransList *onetranslist;
  DLList<OneTransList*> *onetranslistlist;
  int i;
} 


/* Define tokens */ 
%token STRUCT CLASS STATIC PUBLIC TREELINK
%token IF ELSE P Q NOTEQUAL ZEROPOINTER TREE POINTER LINKER SCALARVALUE
%token <str> IDENT FUNC 
%token <ival> INTEGER 
%token STATES START ACCEPT TRANSITIONS ALL NONE VOID NEW
%token LBRACKET RBRACKET LSQRBRA RSQRBRA 
%token TERMINATOR ASSIGN EQUALS OPER LBRACE RBRACE
%token ARROW COMMA COLON 
%token COND QUOTES RETURN STRUCTSLICE CONTSLICE

%left CONCAT
%left OR AND 
%left NOT 
%right INV REV STAR

//%type <str>  onevarfsa 
%type <t> term
%type <f> termlist  
//%type <o> axiom1 axiom2 axiom3
%type <o> dirfsa filename opexp
%type <i> states startstates 
%type <dirtrans> dirfsatrans dirfsatranslist
%type <twotranslist> dirfsastatetrans dirfsamovelist
%type <twotrans> dirfsamove
%type <cont> contslice
%type <struc> structslice
%type <onetranslistlist> onefsatrans onefsatranslist
%type <onetranslist> onefsastatetrans onefsamovelist
%type <onetrans> onefsamove

%type <pred> cond 
%type <d> description
//%type <d> notaxiomlist equalaxiomlist
%type <intlist> acceptstates numlist paramlist
%type <ival> param
%type <e> element
%type <flist> recfunclist
%type <func> recfunc
%type <express>  exp callparams;
%type <block> block;
%type <ins> instruction;
%type <st> statement;
%%


program: 
declarestruct globallist description funcdeclist  recfunclist proclist {
funcs.SetFuncs($5);}
;


//- ********************************************************
//-    Section to parse the descriptive part of the code
//- ********************************************************

declarestruct: CLASS TREE LBRACE decfieldlist RBRACE TERMINATOR {
  Dir t;
  int i,dirnum;
  DirFsa *gen;
  cout << "Found structure declaration" << endl;
  //Add generator directions to list
  dirnum = t.alphabet.DirNum();
  
  d.alphabet.EpsilonAdd();

  for(i=1;i<=dirnum;i++) {
    if ((t.alphabet.Type(i)==GEN) ||(t.alphabet.Type(i)==SCALAR)){
      gen = new DirFsa(i);
      gen->SetDir((Dir)i);
      des.AddOp(gen);
    }
  }
  des.Davinci("gens");
}
;

decfieldlist: decfield 
| decfieldlist decfield
;

decfield: TREE POINTER IDENT TERMINATOR {  d.alphabet.GenAdd($3);}
| SCALARVALUE IDENT TERMINATOR {  d.alphabet.ScalarAdd($2);}
| TREE LINKER IDENT TERMINATOR {  d.alphabet.LinkAdd($3);}
| TREE IDENT TERMINATOR {  d.alphabet.GenAdd($2);}
| TREELINK IDENT TERMINATOR {  d.alphabet.LinkAdd($2);}
;



// ****NB functions must be declared and defined in the same order******
/* Add function names to list */
funcdeclist:
funcdec
| funcdeclist funcdec 
;

//New More C style function declarations, with multiple parameters
funcdec: VOID IDENT LBRACKET decparamlist  RBRACKET TERMINATOR {
  functable.Add($2);
}
;

decparam: TREE POINTER {
}
| SCALARVALUE {
}
;

decparamlist: decparam {}
| decparamlist COMMA decparam {}
;

globallist:{
  //Now that all directions and globals have been added, sort out the epsilon direction
  
  // Moved this earlier, globals will not work now.
  //d.alphabet.EpsilonAdd();
}
| globallist global {}
;



global: SCALARVALUE IDENT TERMINATOR {
  d.alphabet.GlobalAdd($2); //Add a global direction 
}
|
STATIC SCALARVALUE IDENT TERMINATOR {
  d.alphabet.GlobalAdd($3); //Add a global direction 
}
;


//-- ***************  Handle ASAP axioms  ***********************
//-- ************************************************************

/*
description:
notaxiomlist  equalaxiomlist operationlist {
  //des = *$3;
  equal = *$2;
  notequal = *$1;
  delete $1;  delete $2;  //delete $3;
  //cout << "Found description" << endl << des << endl;
}
;
*/

description:
operationlist {

  //cout << "Found description" << endl << des << endl;
}
;

/*
notaxiomlist: {
  $$=new Desc<DirFsa>();
}
| axiom1 notaxiomlist {
  $2->AddOp($1);
  //delete($1);
  $$=$2
}
| axiom2 notaxiomlist {
  $2->AddOp($1);
  //delete($1);
  $$=$2
}
;

equalaxiomlist: {
  $$=new Desc<DirFsa>();
}
| axiom3 equalaxiomlist{
  $2->AddOp($1);
  //delete($1);
  $$=$2
}
;


axiom1: P onevarfsa NOTEQUAL P onevarfsa TERMINATOR {
  $$ = new DirFsa($2, $5, 1);
}
;

axiom2: P onevarfsa NOTEQUAL Q onevarfsa TERMINATOR {
  $$ = new DirFsa($2, $5, 2);
}
;


axiom3: P onevarfsa EQUALS P onevarfsa TERMINATOR {
  $$ = new DirFsa($2, $5, 3);
}
;
*/


/*
onevarfsa: QUOTES IDENT QUOTES {
  $$ = $2;
}
;
*/

operationlist:
operation {
  //cout << "Found last in operationlist" << endl;
  //$$ = new Desc();
  //$$->AddOp(*$1);
  //delete $1;
  
}
| operation operationlist {
  //cout << "Found element in operationlist" << endl;
  //$$ = $2;
  //$$->AddOp(*$1);
  //delete $1;    //Should be able to delete here, but it doesn't work
}
;

operation:
LBRACE  IDENT COLON filename RBRACE {

  cout << "Found GAP format description for " << $2 << endl;
  $4->SetDir((Dir)(d.alphabet.DirLookup($2)));
  des.AddOp($4);
  //delete $4;
}
| 
LBRACE  IDENT COLON RBRACE {
  //For a generating direction
  cout << "Found (obsolete) generating description for " << $2 << endl;
  /*
  DirFsa *gen;
  gen = new DirFsa(d.alphabet.DirLookup($2));
  gen->SetDir((Dir)(d.alphabet.DirLookup($2)));
  des.AddOp(gen);
  */
}
|
LBRACE IDENT COLON dirfsa RBRACE {
  //AGM format FSA
  cout << "Found AGM format description for " << $2 << endl;

  $4->SetDir((Dir)(d.alphabet.DirLookup($2)));
  //$4->Davinci("current.daVinci");
  des.AddOp($4);
}
|
LBRACE IDENT COLON opexp RBRACE {
  //Operation expression
  cout << "Found operation expression for " << $2 << endl;
  $4->SetDir((Dir)(d.alphabet.DirLookup($2)));
  des.AddOp($4);
  //delete $4;
}
|
LBRACE IDENT COLON ALL RBRACE {
  //Operation expression
  cout << "Found operation expression for " << $2 << endl;
  DirFsa *op;
  op = new DirFsa();
  op->MakeAll();
  op->SetDir((Dir)(d.alphabet.DirLookup($2)));
  des.AddOp(op);
  //delete op;
}
|
LBRACE IDENT COLON NONE RBRACE {
  //Operation expression
  cout << "Found operation expression for " << $2 << endl;
  DirFsa *op;
  op = new DirFsa();
  op->MakeNone();
  op->SetDir((Dir)(d.alphabet.DirLookup($2)));
  des.AddOp(op);
  //delete op;
}
;

filename: QUOTES IDENT QUOTES {
  $$ = new DirFsa($2);
  //cout << *$$ << endl;
  //cout << "Found FSA filename" << endl;
}
;

//--************ Handle AGM style FSA descriptions ***********
//--**********************************************************

dirfsa: states startstates acceptstates dirfsatrans {

  $$ = new DirFsa();
  $$->SetStates($1);
  $$->SetInitial($2);
  $$->SetAccept($3);
  $$->MakeAlphabet();
  $$->AllocateTransTable();
  $$->mult->flags[DFA] = true;
  $$->AddTrans($4);
  //cout << "Found number of states" << endl;

}
;


states: STATES INTEGER TERMINATOR {
  $$ = $2;
}
;


startstates: START INTEGER TERMINATOR {

  $$ = $2;
  //cout << "Found initial state" << endl;
}
;

acceptstates: ACCEPT numlist  TERMINATOR {

  agmstate = 1; //Reset statenum
  $$ = $2;
  //cout << "Found accept states" << endl;
}
;

numlist: numlist COMMA INTEGER {
  $$=$1;
  $$->append($3);
}
| INTEGER {
  $$ = new DLList<int>;
  $$->append($1);
}
;

dirfsatrans: TRANSITIONS LSQRBRA dirfsatranslist RSQRBRA {

  $$ = $3;
  //cout << "Found transitions" << endl;
}
;

dirfsatranslist: dirfsatranslist dirfsastatetrans {
  $$ = $1;
  $$->append($2);
}
| dirfsastatetrans {
  $$ = new DLList<TwoTransList*>;
  $$->append($1);
}
;


dirfsastatetrans: LSQRBRA dirfsamovelist RSQRBRA{
  $$ = $2;
}
;

dirfsamovelist: dirfsamovelist dirfsamove {
  $1->append($2);
  $$ = $1;
}
| {$$ = new TwoTransList;}
;

dirfsamove: LBRACKET IDENT COMMA IDENT RBRACKET ARROW INTEGER TERMINATOR {

  //Add transition to transition list
  int a,b;
  $$ = new TwoTrans();

  a = d.alphabet.DirLookup($2);
  b = d.alphabet.DirLookup($4);

  $$->to = $7;
  $$->a = a;
  $$->b = b;

  //cout << "Transition State:" << statenum << ":" << x << "," << y 
  //   << " -> " << tostate << endl;
  //cout << "Letter " << letter << endl;
}
;


//--** Handle operation expression style FSA descriptions ***********
//--**********************************************************

opexp: LBRACKET opexp RBRACKET {
  $$ = $2;
}
|
opexp CONCAT opexp {
  
  $$= new DirFsa();

  //cout << "First alphabet size " << $1->SizeA() << "x" << $1->SizeB() << endl;  
  //cout << "Second alphabet size " << $3->SizeA() << "x" << $3->SizeB() << endl;  
  
  $$->Combine($1,$3);
  delete $1;
  delete $3;
}
|
opexp OR opexp {
  $$ = new DirFsa($1);
  $$->Or(*$3);
  delete $1;
  delete $3;
}
|
opexp AND opexp{
  $$ = new DirFsa($1);
  $$->And($3);
  delete $1;
  delete $3;
}
|
opexp INV {
  $$ = new DirFsa($1);
  $$->Invert();
  delete $1;
}
|
opexp REV {
  $$ = new DirFsa($1);
  $$->Reverse();
  delete $1;
  //$$->Davinci("rev.daVinci");
}
|
opexp STAR {

  ClosureMaker close($1);
  $$ = close.Make();
  delete $1;
}
|
NOT LBRACKET opexp RBRACKET {
  $$ = new DirFsa($3);
  $$->Not();
  delete $3;
}
|
IDENT {
  Path f;
  f.append( (Dir)( d.alphabet.DirLookup($1) ) );
  $$ = new DirFsa(des.NewFindOp(f));
}
;

// ******************************************************************
//            Section to parse the recursive functions
// ******************************************************************
cond: QUOTES IDENT QUOTES {

  $$ = new Pred();
  $$->type = UNKNOWN;
}
|
element EQUALS ZEROPOINTER {  //Allow w==NULL style conditionals
  $$ = new Pred();
  $$->type = ISNULL;
  $$->elem = $1;
}
|
element NOTEQUAL ZEROPOINTER {  //Allow w!=NULL style conditionals
  $$ = new Pred();
  $$->type = NOTNULL;
  $$->elem = $1;
}
;

termlist:
term {
  Path *n = new Path();
  n->append(*$1);
  //delete $1;
  $$ = n;}
| termlist ARROW term {
  $1->append(*$3);
  //delete $3;
  $$ = $1;}
| termlist CONCAT term {
  $1->append(*$3);
  //delete $3;
  $$ = $1;}
;


term: IDENT {
  $$ = new Dir(d.alphabet.DirLookup($1));
}
;


recfunclist: recfunc {
  $$ = new DLList<Function*>;
  $$->append($1);
}
| recfunclist recfunc {
  $1->append($2);
  $$=$1;
}
;

recfunc: IDENT LBRACKET paramlist RBRACKET 
LBRACE block RBRACE{
  cout << "Parsed function " << $1 << "()" << endl;
  $$ = new Function;
  $$->num = functable.Lookup($1);
  $$->paramlist = $3;
  $$->body = $6;
}
|
PUBLIC STATIC VOID IDENT LBRACKET paramlist RBRACKET 
LBRACE block RBRACE{
  cout << "Parsed function " << $4 << "()" << endl;
  $$ = new Function;
  //If not already in table
  if (functable.Lookup($4)==0) functable.Add($4);
  $$->num = functable.Lookup($4);
  $$->paramlist = $6;
  $$->body = $9;
}
;


param: TREE POINTER IDENT{
  $$ = indvars.Add($3);           //Add induction variable to table
  //  cout << "Added variable " << $3 << endl;
}
|
TREE IDENT{
  $$ = indvars.Add($2);           //Add induction variable to table
  //  cout << "Added variable " << $3 << endl;
}
|
SCALARVALUE IDENT {
  $$ = 0;   //Value to indicate parameter is ignored
}
;

paramlist: param {
  $$ = new DLList<int>;
  $$->append($1);
}
| paramlist COMMA param {
  $1->append($3);
  $$=$1;
}
;

//Allow the IDENT to be a global direction, use negative number
element:
IDENT {//Create empty path
  $$=new Elem;
  int v = indvars.Lookup($1);
  if (v>0) {
  $$->ind = v;
  }
  else {  //It is a global direction
    v = d.alphabet.DirLookup($1);
    $$->ind = -v;
  }
  $$->word = new Path;
}
|IDENT ARROW termlist {
  $$=new Elem;
  int v = indvars.Lookup($1);
  //Path *curr=$3;
  if (v>0) {
  $$->ind = v;
  }
  else {  //It is a global direction
    v = d.alphabet.DirLookup($1);
    $$->ind = -v;
  }
  $$->word=$3;
}
|IDENT CONCAT termlist {
  $$=new Elem;
  int v = indvars.Lookup($1);
  //Path *curr=$3;
  if (v>0) {
  $$->ind = v;
  }
  else {  //It is a global direction
    v = d.alphabet.DirLookup($1);
    $$->ind = -v;
  }
  $$->word=$3;
}
;


instruction: element ASSIGN exp TERMINATOR {//Read/Write
  $$ = new Instruction;
  $$->type = INSRW;
  $$->writevar = $1->ind;
  $$->write = $1->word;
  $$->elements = $3;
  $$->statdir = ControlAdd();
  $$->endline = lineno;
  $$->startline = startline;
  startline = lineno+1;
} 
| IDENT LBRACKET callparams RBRACKET TERMINATOR {//Call function
  $$ = new Instruction;
  $$->type = INSCALL;
  //if function is not in table add it
  if (functable.Lookup($1)==0) functable.Add($1);
  $$->call = functable.Lookup($1);
  $$->elements = $3;
  $$->statdir = ControlAdd();
  $$->endline = lineno;
  $$->startline = startline;
  startline = lineno+1;
}

| RETURN TERMINATOR {   //Return from function
  $$ = new Instruction;
  $$->type = INSRETURN;
  $$->statdir = ControlAdd();
  $$->endline = lineno;
  $$->startline = startline;
  startline = lineno+1;
}
|
element ASSIGN NEW TREE TERMINATOR {  //Dynamic memory allocation
  $$ = new Instruction;
  $$->type = INSNEW;
  $$->writevar = $1->ind;
  $$->write = $1->word;
  $$->statdir = ControlAdd();
  $$->endline = lineno;
  $$->startline = startline;
  startline = lineno+1;
}
|
element ASSIGN NEW TREE LBRACKET RBRACKET TERMINATOR {  
  $$ = new Instruction;
  $$->type = INSNEW;
  $$->writevar = $1->ind;
  $$->write = $1->word;
  $$->statdir = ControlAdd();
  $$->endline = lineno;
  $$->startline = startline;
  startline = lineno+1;
}

;


callparams: element {
  $$ = new DLList<Elem*>;
  $$->append($1);
}
| exp COMMA element {
  $$=$1;
  $$->append($3);
}
;

statement: IF LBRACKET cond RBRACKET LBRACE block RBRACE 
ELSE LBRACE block RBRACE{

  $$ = new Statement;
  $$->type = IFSTAT;
  $$->pred = $3;
  $$->ifthen = $6;
  $$->ifelse = $10;
}
| 
IF LBRACKET cond RBRACKET LBRACE block RBRACE {

  $$ = new Statement;
  $$->type = IFSTAT;
  $$->pred = $3;
  $$->ifthen = $6;
  $$->ifelse = NULL;
}
|instruction {
  $$ = new Statement;
  $$->type = INSTRUCTSTAT;
  $$->ins = $1;
}
;

block: {
  $$ = new Block;
  $$->s = NULL;
  $$->next = NULL;
}
| block statement {

  $$ = new Block;
  $$->s = $2;
  $$->next = $1;
}

exp: element {

  $$ = new DLList<Elem*>;
  $$->append($1);
}
| exp OPER element {
  
  $$ = $1;
  $$->append($3);
}
;

//--************ Handle AGM style Process Partition descriptions ***********
//--************************************************************************

proclist: {}
| proclist contslice
| proclist structslice
;


contslice: LBRACE CONTSLICE  states startstates acceptstates onefsatrans RBRACE{

  $$ = new ContSlice();
  $$->SetStates($3);
  $$->SetInitial($4);
  $$->SetAccept($5);
  $$->MakeAlphabet();
  $$->AllocateTransTable();
  $$->mult->flags[DFA] = true;
  $$->AddTrans($6);

  cout << "Found control slice partition" << endl;
  contpartitions->append($$);
}
;

structslice: LBRACE STRUCTSLICE  states startstates acceptstates onefsatrans RBRACE{

  $$ = new StructSlice();
  $$->SetStates($3);
  $$->SetInitial($4);
  $$->SetAccept($5);
  $$->MakeAlphabet();
  $$->AllocateTransTable();
  $$->mult->flags[DFA] = true;
  $$->AddTrans($6);

  cout << "Found structure slice partition" << endl;
  structpartitions->append($$);
}
;

onefsatrans: TRANSITIONS LSQRBRA onefsatranslist RSQRBRA {

  $$ = $3;
  //cout << "Found transitions" << endl;
}
;

onefsatranslist: onefsatranslist onefsastatetrans {
  $$ = $1;
  $$->append($2);
}
| onefsastatetrans {
  $$ = new DLList<OneTransList*>;
  $$->append($1);
}
;


onefsastatetrans: LSQRBRA onefsamovelist RSQRBRA{
  $$ = $2;
}
;

onefsamovelist: onefsamovelist onefsamove {
  $1->append($2);
  $$ = $1;
}
| {$$ = new OneTransList;}
;

onefsamove: IDENT ARROW INTEGER TERMINATOR {

  //Add transition to transition list
  int a;
  $$ = new OneTrans();

  a = d.alphabet.ContLookup($1);

  if (a==0) {  //May be a direction
    a = d.alphabet.DirLookup($1);
  }

  $$->to = $3;
  $$->a = a;

  //cout << "Transition State:" << statenum << ":" << x << "," << y 
  //   << " -> " << tostate << endl;
  //cout << "Letter " << letter << endl;
}
;

%%

int
yyerror (char *s) {
  
  fprintf(stderr, "%s:%d: %s\n", file.c_str(), lineno, s);
  return 0;
}
    
















