#include "onefsa.hh"

OneFsa::OneFsa() {

  Dir t;
  sizea = numsym = t.alphabet.Size();
  //tmalloc(mult, fsa, 1);
  //fsa_init(mult);
}


OneFsa::OneFsa(OneFsa *o){

  direction = o->direction;
  numsym = o->numsym;
  sizea = o->sizea;
  fsa_copy(mult, o->mult);  //Copies fsa across
}

OneFsa::~OneFsa() {

  //cout << "Clearing " << mult << endl;
  ////fsa_clear(mult);
  //tfree(mult);
}

OneFsa::OneFsa(string filename){

  FILE *fp;
  char fsaname[100];

  if ((fp = fopen(filename.c_str(),"r")) == 0) {
    cout << "Cannot open file " << filename << endl;
      exit(1);
  }
  cout << "Parsing fsa from file " << filename << endl;
  tmalloc(mult, fsa, 1);
  fsa_init(mult);
  fsa_read(fp,mult,DENSE,0,0,true,fsaname);
}


void
OneFsa::AddTrans(DLList<OneTransList*>* l){

  auto currlist = l->first();
  OneTransList *tl;
  OneTrans *t;
  int state=1;
  
  while(currlist!=l->end()) {
    tl = (*l)(currlist);
    auto curr = tl->first();
    while (curr != tl->end()){
      t = (*tl)(curr);
      AddTrans(state, t->to, t->a);
      tl->next(curr);
    }
    l->next(currlist);
    state++;
  }
}

void
OneFsa::AddTrans(int from, int to, int a) {

  data[a][from]=to;
}


void
OneFsa::MakeAlphabet() {

  MakeAlphabet(mult);
}

int
OneFsa::Target(int state, int symbol){

  return mult->table->table_data_ptr[symbol][state];
}


void
OneFsa::AllocateTransTable() {

  fsa_table_init(mult->table, mult->states->size, numsym);
  //Zero table
  for (int i=1;i<=mult->states->size;i++){
    for(int j=1;j<=numsym;j++){ 
      mult->table->table_data_ptr[j][i]=0;
    }
  }
  data = mult->table->table_data_ptr;
}

void
OneFsa::MakeAlphabet(fsa *f) {

  f->alphabet->type = SIMPLE;
  f->alphabet->size = numsym;
  f->alphabet->type = IDENTIFIERS;
  //tfree(f->alphabet->names);
  tmalloc(f->alphabet->names, char*, numsym+1);
  for (int i = 1 ; i <= numsym ; i++) {
    tmalloc(f->alphabet->names[i],
	    char, 2);
    //cout << "Identifier " << t.dirnames.Name(i) << endl;
    strcpy (f->alphabet->names[i],
	    "?");
    //cout << "Copied " << f->alphabet->base->names[i] << endl;
  }
}


void
OneFsa::ExistsFirst(TwoFsa *op) {

  mult = fsa_exists(op->mult, DENSE,FALSE, "temp");
  numsym = op->numsym;
  sizea = op->sizea;
  Tidy();
}

void
OneFsa::ExistsSecond(TwoFsa *op) {

  fsa_swap_coords(op->mult);
  mult = fsa_exists(op->mult, DENSE,FALSE, "temp");
  Tidy();
  fsa_swap_coords(op->mult);   //Swap back
}

void
OneFsa::CombineOTFSA(OneFsa *c, TwoFsa *o) {

  TwoFsa *doub = new TwoFsa(),*comb = new TwoFsa();

  doub->MakeDouble(c);
  comb->CombineTFSA(doub,o);
  comb->InvertTFSA();
  this->ExistsFirst(comb);
  delete doub; delete comb;
}

//Not actually used ******************
void
OneFsa::AppendDir(Dir d) {

  int oldstates, newstates, extrastates;
  int dir=d.Int(), accept, newaccept, ambig;
  int alphsize;

  table_struc *newtable;
  tmalloc(newtable, table_struc, 1);

  oldstates = mult->states->size;
  alphsize = mult->alphabet->size; 
  extrastates = mult->num_accepting;

  newstates = oldstates+extrastates;

  mult->states->size = newstates;

  //Paranoia??
  mult->flags[MINIMIZED] = false;
  mult->flags[ACCESSIBLE] = false;
  mult->flags[TRIM] = false;
  mult->flags[RWS] = false;
  mult->flags[DFA] = true;
  mult->flags[BFS] = false;
  mult->flags[MIDFA] = false;
  mult->flags[NFA] = false;

  int **oldtabledata,  **newtabledata;
  
  fsa_table_init(newtable, newstates, numsym);
  oldtabledata = mult->table->table_data_ptr;
  newtabledata = newtable->table_data_ptr;

  //Copy all old information across
  for (int s = 1 ; s <=oldstates ; s++) {
    for (int l = 1 ; l <=alphsize ; l++) {
      newtabledata[l][s] = oldtabledata[l][s];
    }
    //cout << endl;
  }

  //For each accepting state
  for (int acc = 1 ; acc <= extrastates ; acc++){
    accept=mult->accepting[acc];
    newaccept=acc+oldstates;
    
    //Does the accepting state have a dir transition?
    if (newtabledata[dir][accept] != 0) {
      ambig = newtabledata[dir][accept];
      newtabledata[dir][accept]=newaccept;
      //Copy all transitions from ambig
      for (int k=1; k<=alphsize; k++) {
	newtabledata[k][newaccept] = newtabledata[k][ambig];
      }
    }
    //No dir transition
    else {
      newtabledata[dir][accept] = newaccept;
      //Set all newaccept transitions to zero
      for (int k=1; k<=alphsize; k++) {
	newtabledata[k][newaccept] = 0;
      }
    }
  }

  //Add new accept states 
  for (int j = 1 ; j <= extrastates; j++) {
    mult->accepting[j] = oldstates+j;
  }
  //Remove old table information
  //tfree(mult->table);
  //Copy new table across
  mult->table = newtable;
  //Minimise fsa to remove any isolated states
  fsa_minimize(mult);
}



void
OneFsa::Davinci(string filename) {

  //cout << "Start of " << filename << endl;
  ofstream file;
  file.open(adddav(filename));
  file << "[" << endl;
  Davinci(file);
  file << "]" << endl;
  file.close();
}

//Output graph
void OneFsa::Davinci(ostream &out){

  fsa* f = mult;
  int numstates = f->states->size;
  bool found;
  int **data=f->table->table_data_ptr;

  fsa_set_is_accepting(f);
  fsa_set_is_initial(f);

  //Two type of node, state and transition
  //State nodes are of type 
  //start - Rhombus 
  //accept - double border
  //normal - Circle

  //Transitions are 'edges' that link two nodes
  //Box nodes with state name and list of  transition labels

  //Go through each state
  for (int i=1; i<=numstates; i++) {
    
    //Determine its type and create an appropriate node
    //Output node header stuff
    out << "l(\"" 
	<< direction << ":\\n"
	<< i
	<< "\",n(\"\",[a(\"OBJECT\",\"";

    if (f->is_initial[i]) {out << direction << ":\\n";}

    out << i
	<< "\"),a(\"_GO\",\"" ;

    if (f->is_initial[i]) out << "rhombus";
    else out << "circle";
    out  << "\")";  
    if (f->is_accepting[i]) {
      out << ",a(\"BORDER\",\"double\")"  << endl;
    }
    out << "],[" << endl;


    //Now go through all possible transition states
    for (int j=1; j<=numstates; j++) {
      found = false;
      //Look for transitions that take us there
      for (int a=1; a<=numsym; a++) {
	if (data[a][i] == j) found = true; 
      }
      if (found) {
	//Create edge
	out << "l(\""
	    << direction << ":"
	    << i << "->" << j << "-1-\",";
	out << "e(\"\",[a(\"EDGEPATTERN\",\"solid\"),a(\"_DIR\",\"none\")],";
	//Create new transition node
	out << "l(\"";
	//Create node name, 
	//-a list of all letters that take us there
	out << direction << ":" << i << "->" << j << "\\n";
	for (int x = 1; x <= numsym; x++) {
	  if (data[x][i] ==j) {
	    out << "(" << Name(x) << ")\\n"; 
	  }
	}
	out << "\"," << endl;
	out  << "n(\"\",[a(\"OBJECT\",\"";
	//Nodename again, Printed version
	//-a list of all letters that take us there
	//out << i << "->" << j << "\\n";
	for (int x = 1; x <= numsym ; x++) {
	  if (data[x][i] ==j) {
	    out << "(" << Name(x) << ")\\n"; 
	  }
	}
	out << "\"),a(\"_GO\",\"box\")],[" << endl;

	//Create edge to end node
	out << "l(\"" << direction << ":" << i << "->" << j << "-2-"
	<< "\",e(\"\",[" 
	<< "a(\"EDGEPATTERN\",\"" << "solid" << "\""
	    << ")],r(\""
	    << direction << ":\\n"
	    << j 
	    << "\")))," << endl;
	out << "]))";
	out << ")),";
      } //End if found
    }
    out << endl << "])),\n" << endl;
  }
}


void
OneFsa::PrintSparse() {

  int x,s, numstates, transition;

  numstates = mult->states->size;

  PrintHeader();

  cout << "\tTransitions[" << endl;
  

  for (s=1; s <= numstates; s++) {
    cout << "\t[";
    for (x=1; x <= sizea; x++) {
	transition = mult->table->table_data_ptr[x][s];
	if (transition != 0) {
	  cout << Name(x) << "->" << transition << ";";
	}
    }
    cout << "\t]" << endl;
  }
  cout << "}" << endl;
}


bool
OneFsa::Accept(Path &p) {

  int currstate, symbol;

  if (mult->states->size==0) return false; //No states

  currstate = mult->initial[1]; //Assume one initial state

  list<Dir>::iterator z = p.first();

  while (z!=p.end()) {
    
    symbol = p(z).Int();
    currstate = mult->table->table_data_ptr[symbol][currstate];
    if (currstate==0) return false;
    p.next(z);
  }

    //If current state is accepting return true
  for (int i=1; i<=mult->num_accepting;i++) {
    if (mult->accepting[i] == currstate) return true; 
  }

  return false;
}


void
OneFsa::MakeAll() {

  //Dir t;
  fsa *f = mult;

  fsa_init(f);

  f->flags[DFA] = true;
  //Make states, initial and accepting
  f->states->size = 1;
  f->num_initial = 1;
  tmalloc(f->initial, int, 2);
  f->initial[1] = 1;
  f->num_accepting = 1;
  tmalloc(f->accepting, int, 2);
  f->accepting[1] = 1;

  //Make alphabet
  MakeAlphabet();

  //Make table
  fsa_table_init(f->table, 1, numsym);
  int **data = f->table->table_data_ptr;

  //Fix transitions from state 1
  for (int x = 1; x <= numsym ; x++) {
	data[x][1] = 1;
  }
}


void
OneFsa::MakeNone() {

  //Dir t;
  fsa *f = mult;
  fsa_init(f);

  f->flags[DFA] = true;
  //Make states, initial and accepting
  f->states->size = 1;
  f->num_initial = 1;
  tmalloc(f->initial, int, 2);
  f->initial[1] = 1;
  f->num_accepting = 1;
  tmalloc(f->accepting, int, 2);
  f->accepting[1] = 1;

  //Make alphabet
  MakeAlphabet(f);

  //Make table
  //int letter;
  fsa_table_init(f->table, 1, numsym);
  int **data = f->table->table_data_ptr;

  //Fix transitions from state 1
  for (int x = 1; x <= numsym ; x++) {
	data[x][1] = 0;
  }
}


ostream& 
operator << (ostream& s, OneFsa& o) {

   s  <<  "  States: " << o.mult->states->size
    << endl;

  return s;
}




